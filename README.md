# Loop Transformer #

**Development has been moved to [gitlab](https://gitlab.com/innotkauker/rose-transformations).**

This repo still occasionally updated.

Version 0.1.1

A piece of code that performs parallelization-oriented transformations
on C programs.

A [web-interface](http://213.251.199.174:8000) is available.

### What transformations are supported? ###

    #pragma x orders...
    statement;

Order syntax:

* For arbitrary statements:
    * `inline[D](f[, ...])`;
      Inline function calls. `[D]` is optional and allows to specify number
      of iterations (default is 10). `(f[, ...])` is also optional and allows
      to limit inlining to only specified functions. If no function list is
      given, libc functions are still skipped.
    * `id(ID)`:
      Assign a nonnegative integer identifier to a statement.
    * `partition`:
      Setup a border for loop partitioning.
* For basic blocks and for-loops:
    * `swap(ID:ID[, ...])`:
      Swap statements with specified IDs pairwise.
      All types of statements are supported.
      Dependency check is performed.
      It can be disabled for a pair of statements by replacing
      `:` with `&` in the corresponding parameter.
    * `merge(ID, ID[, ...])`:
      Merge for-loop nests with provided IDs.
      The bodies are merged if loop heads are equal.
      No dependency checks are performed currently.
    * `map(VAR:VAR[, ...])`:
      For each pair, replace references to elements of the first array with
      references to elements of the second one.
      A mapping must be created in the target block.
      Regular (non-array) variables are also supported.
      Mapped arrays are removed if they are no longer used.
    * `propagate(VAR[, ...])`:
      Where possible, replaces read-access to provided variables
      with expressions that compute their values.
      Removes declarations if the corresponding variables get fully propagated
      and are no longer used in the program.
    * `local(VAR[, ...])`:
      When propagating, consider only the values set in the current block.
    * `bringout(VAR[, ...])`:
      Gets definitions of all invariant variables of the current loop or
      all nested loops if a block is being handled.
      Parentheses are optional. They are supported only when the pragma is
      attached to a loop. If present, only specified variables are considered.
* For basic blocks only:
    * `nontemp`:
      Turns off automatic removal of the block.
      This parameter is also set when an `id` is specified.
* Working with for-loop nests:
    * `index(VAR:I[, ...])`:
      Shift iteration spaces of loops. For each pair, add I to bounds of loop
      with index VAR and change access to VAR in its body accordingly.
    * `unroll(VAR:I[, ...]`:
      Unroll some iterations of loops in the nest.
      If paranthesized parameters are missing, perform full unrolling.  If it
      is impossible to determine the number of iterations, an error is issued.
      If parameters are present, only the specified first and/or last
      (depending on the sign of I) iterations of loops with corresponding
      indices are unrolled. Here, if total iteration number is unknown,
      correctness is not checked.
    * `permut(VAR[, VAR])`:
      Permutate iterations of the loop nest according to the order of variables
      in the supplied list. The variables must be same the nest iteration
      space is using.
    * `split(VAR, VAR[, VAR])`:
      If parens are missing, splits the body of the nest according to
      `partition` pragmas found in it.
      If parameters are present, creates several copies of the loop
      where modifications of all variables but one
      of the provided and those it depends on removed.

See the `tests/` directory for usage examples.

### How do I get set up? ###

Consider using the [web-interface](http://213.251.199.174:8000) for testing purpses.
Instructions on how to get a local installation are provided below.

1. Install [ROSE](http://rosecompiler.org) using the instructions on the site.
   You need the repository at <https://github.com/rose-compiler/rose>.
   You may use `--without-java --without-fortran --disable-tests-directory
   --disable-tutorial-directory --disable-binary-analysis --disable-ltdl-install --disable-opencl
   --disable-cuda --disable-php --without-haskell` configure keys to somewhat speedup
   the build.
   Apply the [patch](https://bitbucket.org/innot/rose-transformations/src/master/inliner-fixes.patch) before building.
   GCC versions up to 4.8 are currently supported.

1. Build the SPRAY library in projects/CodeThorn directory.
   If you have built ROSE in `$(ROOT)rose/buildrose`, then `cd` into
   `$(ROOT)rose/buildrose/projects/CodeThorn/src` and do
   `make spray.lo && make install-pkglibLTLIBRARIES`.

1. `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(ROSE_INSTALL_DIR)/lib`, where
    $(ROSE_INSTALL_DIR) is where you have told ROSE to install itself.

1. Clone and setup LoopTransformer. In file
    `$(ROOT)rose-transformations/LoopTransformer/Makefile.local` set
    the variable `ROSE_DIR` to `$(ROSE_INSTALL_DIR)`, and
    `SPRAY_INCLUDE_DIR` to `$(ROOT)rose/projects/CodeThorn/src`.
    Switch to GCC 4.9 or higher as the translator relies on some C++14
    features that were implemented only in 4.9.

1. `make test` to build everything and check the installation.
    `translator` is the executable file.

### Details ##

* [Test results](https://bitbucket.org/innot/rose-transformations/src/master/LoopTransformer/tests/results.md)
* Based on [ROSE Framework](http://rosecompiler.org)