/*
 *
 *  Created on: 24 Feb, 2011
 *      Author: didem
 */

/*
 *  Modified on 12 Dec, 2014
 *      Based on code from Mint project
 */

#pragma once

#include "sage3basic.h"

class MintConstantFolding {
public:
	static void remove_unsigned_casts(SgNode *e);
	MintConstantFolding() {}
	virtual ~MintConstantFolding() {}
	static void constantFoldingOptimization(SgNode* node,
						bool remove_casts = false,
						bool internalTestingAgainstFrontend = false);
	static void constantFoldingOnArrayIndexExpressions(SgNode* node);
};

