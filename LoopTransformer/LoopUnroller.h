#pragma once

#include <string>
#include <map>
#include "sage3basic.h"

/// This unrolls loops. Irrepressibly.
///
void unroll_some_iterations(SgForStatement* kernel,
			std::multimap<std::string, int> iterations);

