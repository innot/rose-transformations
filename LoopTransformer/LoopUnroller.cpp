#include "sage3basic.h"
#include <string>
#include <iostream>
#include <sstream>
#include "util.h"
#include "LoopUnroller.h"
#include "MintConstantFolding.h"
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace TfUtils;

namespace {
/// Constructs an expression containing the difference between *ub* and *lb*.
///
/// *Ub* and *lb* must share reference to one variable.
/// Returns nullptr on errors.
///
/// \todo Remove possible memory leaks. If at all possible.
SgExpression* difference_between_bounds(SgExpression* lb, SgExpression* ub)
{
	auto lb_vars = querySubTree<SgVarRefExp>(lb);
	auto ub_vars = querySubTree<SgVarRefExp>(ub);
	assert(lb_vars.size() == 1);
	assert(ub_vars.size() == 1);

	auto lb_name = get_var_initialized_name(lb_vars[0]);
	auto ub_name = get_var_initialized_name(ub_vars[0]);
	if (lb_name != ub_name) // different variables are referenced
		return nullptr;

	auto lb_const_list = querySubTree<SgIntVal>(lb);
	auto ub_const_list = querySubTree<SgIntVal>(ub);
	if (lb_const_list.size() > 1 || ub_const_list.size() > 1 )
		// constant folding failed
		return nullptr;

	vector<SgBinaryOp*> lb_op_list;
	query_subtree<SgBinaryOp>(lb, lb_op_list);
	vector<SgBinaryOp*> ub_op_list;
	query_subtree<SgBinaryOp>(ub, ub_op_list);

	SgBinaryOp *lb_binary_op = nullptr;
	SgBinaryOp *ub_binary_op = nullptr;
	if (lb_op_list.size())
		lb_binary_op = lb_op_list[0];
	if (ub_op_list.size())
		ub_binary_op = ub_op_list[0];

	SgExpression *const_val_lb;
	if (lb_const_list.size() == 1)
		const_val_lb = lb_const_list[0];
	else
		const_val_lb = buildIntVal(0);

	SgExpression *const_val_ub;
	if (ub_const_list.size() == 1)
		const_val_ub = ub_const_list[0];
	else
		const_val_ub = buildIntVal(0);

	bool minus_ub = false;
	if (ub_binary_op)
		switch (ub_binary_op->variantT()) {
		case V_SgSubtractOp:
			minus_ub = true;
		case V_SgAddOp:
			break;
		default:
			return nullptr;
		}

	if (lb_binary_op)
		switch (lb_binary_op->variantT()) {
		case V_SgSubtractOp:
			return buildAddOp((minus_ub ? buildMinusOp(const_val_ub) : const_val_ub),
							const_val_lb);
		case V_SgAddOp:
			return buildSubtractOp((minus_ub ? buildMinusOp(const_val_ub) : const_val_ub),
							const_val_lb);
		default:
			return nullptr;
		}

	return buildSubtractOp(buildAddOp(const_val_ub, buildIntVal(1)),
						const_val_lb);
}

/// Gets int-value corresponding to *e*
///
/// Works as follows:
/// * constructs a variable declaration initialized with e
/// * inserts it before scope
/// * folds constants in it
/// * if possible, gets the constant received during folding
/// * removes the declaration
int calculate_expression_value(SgExpression *e, SgScopeStatement *scope)
{
	int result;
	auto tmp_var_decl = buildVariableDeclaration("__tmp_name", buildIntType(),
					buildAssignInitializer(e), scope);
	insertStatement(scope, tmp_var_decl, false);
	auto rhs_const = getFirstInitializedName(tmp_var_decl)->get_initializer();
	MintConstantFolding::constantFoldingOptimization(rhs_const, true);

	SgIntVal* rhs_val = isSgIntVal(rhs_const);
	if (rhs_val) {
		result = rhs_val->get_value();
	} else {
		stringstream ss(rhs_const->unparseToString().c_str());
		if ((ss >> result).fail())
			return -1;
	}
	removeStatement(tmp_var_decl);
	deepDelete(tmp_var_decl);
	return result;
}

/// Calculates loop's unrolling factor if possible.
///
/// Loop lower and upper bounds should be expressions
/// based on the same variable +/- a constant.
/// For example, if `lb = i-2` and `ub = i+3` then the function should return 6.
/// Returns -1 if the condition on bounds is not satisfied.
int find_unrolling_factor(SgForStatement* loop)
{
	SgInitializedName* ivar = nullptr;
	SgExpression *lb = nullptr;
	SgExpression *ub = nullptr;
	SgExpression *step = nullptr;
	SgStatement *orig_body = nullptr;
	SgExpression *raw_range_exp = nullptr;

	MintConstantFolding::constantFoldingOptimization(loop, true);
	if (!isCanonicalForLoop(loop, &ivar, &lb, &ub, &step, &orig_body))
		throw Exception("Unable to unroll loop as it is not canonical.", loop);
	assert(ivar && lb && ub && step && isSgBasicBlock(orig_body));

	int lb_vars_n = (querySubTree<SgVarRefExp>(lb)).size();
	int ub_vars_n = (querySubTree<SgVarRefExp>(ub)).size();

	if ((lb_vars_n == 1) && (ub_vars_n == 1))
		raw_range_exp = difference_between_bounds(lb, ub);
	else if ((lb_vars_n == 0) && (ub_vars_n == 0))
		raw_range_exp = buildSubtractOp(deepCopy(ub), deepCopy(lb));
	else
		return -1; // unable to calculate factor

	if (!raw_range_exp)
		return -1;
	raw_range_exp = buildAddOp(buildDivideOp(raw_range_exp, deepCopy(step)), buildIntVal(1));
	MintConstantFolding::remove_unsigned_casts(raw_range_exp);
	raw_range_exp->set_need_paren(true);
	return calculate_expression_value(raw_range_exp, loop);
}

/// Removes variable declatations from *loop*'s body. Currently not used
///
/// * moves local declarations to the outer scope if there are no
///   variables with such name there
/// * replaces initializers with assignments
/// * replaces further references to declared variable with references to
///   the variable declared in the outer scope
///
/// \todo Also handle initialization-less assignments and array initializers
void modify_declarations(SgForStatement *loop)
{
	auto body = loop->get_loop_body();
	vector<SgVariableDeclaration*> declarations;
	query_subtree<SgVariableDeclaration>(body, declarations);
	for (auto decl : declarations) {
		auto var = decl->get_variables()[0];
		SgScopeStatement *enc_scope = getEnclosingNode<SgScopeStatement>(loop);
		SgVariableSymbol *other_declaration = lookupVariableSymbolInParentScopes(var->get_name(), enc_scope);
		SgInitializedName *outer_name;
		if (!other_declaration) {
			auto new_decl = buildVariableDeclaration(var->get_name().getString(),
								var->get_type(), nullptr, enc_scope);
			insertStatementBefore(loop, new_decl);
			outer_name = new_decl->get_variables()[0];
		} else {
			outer_name = other_declaration->get_declaration();
		}
		auto gen_init = var->get_initializer();
		auto assign_init = isSgAssignInitializer(gen_init);
		if (!assign_init)
			throw Exception("A non-supported initializer to variable.", gen_init);
		auto new_assignment = buildAssignStatement(buildVarRefExp(outer_name), assign_init->get_operand_i());
		replaceStatement(decl, new_assignment);

		vector<SgVarRefExp*> var_refs;
		query_subtree<SgVarRefExp>(body, var_refs);
		var_refs.erase(remove_if(var_refs.begin(), var_refs.end(),
				[var] (const SgVarRefExp *v) {
					return var != v->get_symbol()->get_declaration();
				}), var_refs.end());
		for (auto v : var_refs)
			replaceExpression(v, buildVarRefExp(outer_name));
	}
}

/// Creates and inserts an iteration of *loop* with supplied parameters
///
/// Replaces all references to index in loop's body with lb + i*step.
/// Copies that body before or after the loop.
///
/// \todo is *step* necessary at all?
void insert_iteration(SgForStatement *loop, SgExpression *i, SgExpression *step, bool before)
{
	SgInitializedName* ivar;
	SgExpression* lb;
	SgStatement* body;
	isCanonicalForLoop(loop, &ivar, &lb, nullptr, nullptr, &body);
	assert(body && lb && ivar);
	body = deepCopy(body);
	vector<SgVarRefExp*> refs;
	query_subtree<SgVarRefExp>(body, refs);
	refs.erase(remove_if(refs.begin(), refs.end(),
				[ivar] (SgVarRefExp *ref) {
					return ivar != get_var_initialized_name(ref);
				}), refs.end());
	for (auto refexp : refs) {
		auto new_exp = buildAddOp(deepCopy(lb),
					buildMultiplyOp(deepCopy(step), i));
		MintConstantFolding::constantFoldingOptimization(new_exp);
		replaceExpression(refexp, new_exp);
	}
	insertStatementList(loop, isSgBasicBlock(body)->get_statements(), before);
}

/// Unrolls the supplied loop.
///
/// Takes first iterations[0] and last iterations[1] iterations of the loop
/// and puts them before or after the loop, accordingly.
/// Removes the loop if there are no iterations left.
void unroll_iterations(SgForStatement *loop,
				std::size_t iterations[2], int loop_factor)
{
	if (loop_factor == -1) {
		l << LOG_WARN << "Could not find loop's unrolling factor; ";
		l << "unable to detect wrong parameters for unrolling - ";
		l << "use with caution." << endl;
	} else if ((int)(iterations[0] + iterations[1]) > loop_factor) {
		throw Exception("You have requested to unroll more iterations than the loop has",
				loop);
	}
	//if (!factor_is_acceptable(iterations[0] + iterations[1]))
	//        throw Exception("requested to unroll more iterations than possible with current unroller configuration",
	//                        loop);

	l << loop << " Unrolling " << iterations[0] << " first and ";
	l << iterations[1] << " last iterations of a loop with ";
	if (loop_factor == -1)
		l << "some";
	else
		l << loop_factor;
	l << " iterations." << endl;

	SgInitializedName* ivar = nullptr;
	SgExpression *lb = nullptr, *ub = nullptr, *step = nullptr;
	SgStatement* orig_body = nullptr;

	if (!isCanonicalForLoop(loop, &ivar, &lb, &ub, &step, &orig_body))
		throw string("Unable to unroll non-canonical loop.");

	assert(ivar && lb && ub && step);
	assert(isSgBasicBlock(orig_body));

	auto step_bin_op = isSgBinaryOp(step->get_parent());
	assert(step_bin_op);

	if (isSgMinusAssignOp(step_bin_op))
		step = buildMinusOp(deepCopy(step), SgUnaryOp::prefix);
	else if (!isSgPlusAssignOp(step_bin_op))
		throw Exception("Illegal increment in a canonical loop.", step_bin_op);

	for (unsigned i = 0; i < iterations[0]; i++)
		insert_iteration(loop, buildIntVal(i), step, true);
	for (unsigned i = iterations[1]; i > 0; i--)
		insert_iteration(loop, buildSubtractOp(
					buildSubtractOp(deepCopy(ub),
							deepCopy(lb)),
					buildIntVal(i - 1)),
				step, false);

	if ((int)(iterations[0] + iterations[1]) == loop_factor) {
		removeStatement(loop);
	} else {
		if (iterations[0])
			replaceExpression(lb, buildAddOp(deepCopy(lb), buildIntVal(iterations[0])));
		if (iterations[1])
			replaceExpression(ub, buildSubtractOp(deepCopy(ub), buildIntVal(iterations[1])));
	}
}

/// This class performs preparatory work for the function above.
class LoopUnroller {
	multimap<string, int> iterations;
public:
	LoopUnroller(multimap<string, int>&& iterations)
	: iterations(iterations)
	{}

	void operator()(SgForStatement* loop)
	{
		SgInitializedName* ivar = nullptr;
		SgExpression* lb = nullptr;
		SgExpression* ub = nullptr;
		SgExpression* step = nullptr;
		SgStatement* orig_body = nullptr;

		modifiedForLoopNormalization(loop);
		int factor = find_unrolling_factor(loop);
		if (!isCanonicalForLoop(loop, &ivar, &lb, &ub,
							&step, &orig_body))
			throw Exception("Unable to unroll: target loop is not canonical.",
					loop);

		std::size_t local_iterations[2] = {0, 0};
		if (iterations.empty()) {
			if (factor != -1)
				local_iterations[0] = factor;
			else
				throw Exception("Unable to fully unroll loop as we could not find its number of iterations.",
						loop);
		} else {
			auto iters = iterations.equal_range(ivar->get_name());
			if (iters.first == iters.second)
				return;
			for (auto i = iters.first; i != iters.second; i++)
				if (i->second > 0)
					local_iterations[0] = i->second;
				else
					local_iterations[1] = -(i->second);
		}
		unroll_iterations(loop, local_iterations, factor);
	}
};

} // namespace

/// Main unrolling function
///
/// Unrolls the nest of loops supplied in *kernel*
/// with parameters supplied as a multimap of <index, i>.
/// Checks if there are variable declarations in the loop
/// and fails if there are. Tries to find unrolling factor.
///
/// Unrolls some iterations according to *iterations*. If it is empty,
/// unrolls everything.
void unroll_some_iterations(SgForStatement* kernel,
					multimap<string, int> iterations)
{
	assert(kernel);
	auto all_loops = get_all_nested_loops(kernel);

	/// \todo switch *modify_declarations* back when ready to handle consequences
	/// \todo which consequences?
	// modify_declarations(loop);
	auto body = get_nested_loops_main_block(kernel);
	auto decl_stmts = querySubTree<SgVariableDeclaration>(body);
	for (auto d : decl_stmts)
		if (d->get_parent() == body)
			throw Exception("Unable to unroll loop with a body containing variable declarations.",
					kernel);

	boost::for_each(all_loops | boost::adaptors::reversed,
			LoopUnroller(std::move(iterations)));
}

