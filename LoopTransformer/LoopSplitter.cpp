#define BOOST_RESULT_OF_USE_DECLTYPE
#include "LoopSplitter.h"
#include "util.h"
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/numeric.hpp>

using namespace std;
using namespace SageInterface;
using namespace TfUtils;
using namespace LoopTransformations;

namespace LoopTransformations {
namespace {

/// Does the same as get_referenced_vars(),
/// but does not include the top modified variable.
///
/// Examples:
/// * `v = x + y` -> {x, y}
/// * `int v = y` -> {y}
/// * `v = x++ + y` -> {x, y}
///
/// \todo merge with the following function
CNS get_sec_refs(const SgExpression *expr)
{
	using namespace boost::adaptors;
	auto refs = querySubTree<SgVarRefExp>(expr);
	SgInitializedName *lhs_var = nullptr;
	do { // exclude lhs var
		auto assignment = isSgAssignOp(expr);
		if (!assignment)
			break;
		auto lhs = assignment->get_lhs_operand();
		if (isSgVarRefExp(lhs) || isSgPntrArrRefExp(lhs))
			lhs_var = get_var_initialized_name(lhs);
	} while (false);
	auto ref_filter = [&lhs_var] (const SgVarRefExp *ref) {
		if (!lhs_var)
			return true;
		auto rn = get_var_initialized_name(ref);
		if (lhs_var == rn) {
			lhs_var = nullptr;
			return false;
		}
		return lhs_var != rn;
	};
	CNS result;
	boost::copy(refs
			| filtered(ref_filter)
			| transformed(get_var_initialized_name),
		inserter(result, result.end()));
	return result;
}

/// A wrapper for get_sec_refs(SgExpression*)
/// that also handles variable declarations.
CNS get_sec_refs(const SgStatement* stmt)
{
	CNS result;
	auto expr_stmt = isSgExprStatement(stmt);
	auto decl = isSgVariableDeclaration(stmt);
	if (expr_stmt) {
		boost::copy(get_sec_refs(expr_stmt->get_expression()),
				inserter(result, result.end()));
	} else if (decl) {
		auto v = decl->get_variables()[0];
		auto gen_init = v->get_initializer();
		if (auto assign_init = isSgAssignInitializer(gen_init)) {
			get_referenced_vars(assign_init->get_operand(), result);
		} else if (auto ag_init = isSgAggregateInitializer(gen_init)) {
			auto initializers =
				ag_init->get_initializers()->get_expressions();
			for (auto init : initializers)
				get_referenced_vars(init, result);
		}
	} else {
		assert(0);
	}
	return result;
}

// For a given variable name, traverse statements in the reversed order,
// find the first one that modifies the var, push its referenced vars
// to the result.
class DepGen {
	const CNSV &modified;
	const CNSV &referenced;
	depv result;
public:
	DepGen(const CNSV &m, const CNSV &r): modified(m), referenced(r) {}
	depv get_result() { return std::move(result); }
	void operator() (const SgInitializedName* s)
	{
		using namespace boost::adaptors;
		CNS t;
		t.insert(s);
		auto mods = modified | reversed | indexed(0);
		auto sn = modified.size();
		for (auto ns : mods) {
			auto v = boost::find_if(ns.value(), RefFinder(&t));
			if (v != std::end(ns.value())) {
				auto e = depv::value_type(*v,
					referenced[sn - ns.index() - 1]);
				result.push_back(e);
				break;
			}
		}
	}
};

/// For statements, compute which variables they require.
/// Takes only variables in *vars* into account.
/// Has two versions in cases when only a possibility check is performed
/// and when all variables are claculated.
template<bool cross_iter_dep>
class ReferenceCollector {
	const CNSV &modified;
	const SV &stmts;
	const CNS &vars;
	CNSV referenced;
	vector<char> ready;

	/// Recursively fetches variables referenced in *i*th statement
	/// of some block based on referenced and modified variables.
	/// Updates *referenced_vars* when appropriate,
	/// sets bits in *ready* when the corresponding set is ready.
	///
	/// \todo decrease indentation?
	void reach_all_refs(std::size_t i)
	{
		if (ready[i])
			return;
		for (auto ref : referenced[i])
			for (unsigned s = 0; s < i; s++)
				if (modified[s].count(ref)) {
					if (!ready[s])
						reach_all_refs(s);
					referenced[i].insert(
						referenced[s].begin(),
						referenced[s].end());
				}
		ready[i] = 1;
	}

	/// Checks if *var* is modified in any of the first
	/// *stmt_i*-1 statements.
	bool mod_before(const SgInitializedName *var, std::size_t stmt_i)
	{
		auto b_it = modified.begin();
		auto e_it = b_it + stmt_i;
		return std::find_if(b_it, e_it,
			[var] (const CNS &vars) {
				return vars.count(var);
			}) != e_it;
	}

	void reach(std::size_t i)
	{
		auto cvp = boost::find_if(modified[i], RefFinder(&vars));
		if (cvp == std::end(modified[i]))
			// we are not interested in info about any
			// of the variables modified here
			return;
		//if (modified[i].size() > 1)
		//        throw Exception("More than one variable is modified in a statement which is considered to be removed",
		//                        stmts[i]);
		reach_all_refs(i);
	}

public:
	ReferenceCollector(const CNSV &modified, const SV &stmts,
				const CNS &vars)
		: modified(modified), stmts(stmts), vars(vars),
		referenced(stmts.size()), ready(stmts.size(), 0)
	{ }

	CNSV result() { return std::move(referenced); }

	void operator()(SgStatement* stmt, std::size_t index);
};

/// An overload in case we are calculating cross-iteration dependencies
/// for the actual split.
template<>
void ReferenceCollector<true>::operator()(SgStatement* stmt, std::size_t index)
{
	CNS local_refs = get_sec_refs(stmt);
	for (auto referenced_var : local_refs) {
		bool m_before = mod_before(referenced_var, index);
		bool m_any = mod_before(referenced_var, modified.size());
		if (m_before || !m_any)
			local_refs.erase(referenced_var);
	}
	referenced[index].insert(local_refs.begin(), local_refs.end());
	reach(index);
}

/// An oveload if we are checking split possibility.
template<>
void ReferenceCollector<false>::operator()(SgStatement* stmt, std::size_t index)
{
	get_referenced_vars(stmt, referenced[index]);
	reach(index);
}

/// For each variable from *vars* makes a set of variables
/// which it depends on in *block*.
///
/// *Cross_iter_dep* sets if we are searching for cross-iteration dependency
/// to find out whether the split is possible.
///
/// \todo Add array references distinguishing?
/// \todo May more than one of the variables be modified at the same time?
template<bool cross_iter_dep = false>
depv get_dependant_variables(SgBasicBlock* block, const CNS& vars)
{
	using namespace boost::adaptors;
	auto stmts = block->get_statements();
	auto modified = get_vars_modified_in_statements(stmts);

	ReferenceCollector<cross_iter_dep> collector(modified, stmts, vars);
	for (auto stmt : stmts | indexed(0))
		collector(stmt.value(), stmt.index());
	CNSV references = collector.result();
	return boost::for_each(vars, DepGen(modified, references)).get_result();
}

/// A wrapper for
/// get_dependant_variables(SgBasicBlock*, const CNS&, depv&)
///
template<bool cross_iter_dep = false>
depv get_dependant_variables(SgBasicBlock *block, const STRS &vars)
{
	return get_dependant_variables<cross_iter_dep>(block,
				convert_names_to_vars(block, vars));
}
} // namespace

depv get_dependant_variables(SgBasicBlock *block,
				const SgInitializedName *var)
{
	CNS v;
	v.insert(var);
	return get_dependant_variables(block, v);
}

namespace {
typedef struct VarDesc {
	std::size_t gid;
	CNS deps;
	VarDesc(std::size_t gid, CNS&& deps) : gid(gid), deps(std::move(deps))
	{}
} VarDesc;

/// Converts dependency sets into statement sets.
deps descriptors_to_statements(auto&& idep, auto gc, auto&& stmts,
				auto&& stmt_mods)
{
	using namespace boost::adaptors;
	std::multimap<std::size_t, CNS> grouped_deps;
	boost::copy(idep | transformed([] (auto e) {
				e.second.deps.insert(e.first);
				return std::make_pair(e.second.gid,
						std::move(e.second.deps));
			}), std::inserter(grouped_deps, grouped_deps.end()));

	deps result;
	for (std::size_t i = 1; i <= gc; i++) {
		auto rng = grouped_deps.equal_range(i);
		if (rng.first == rng.second)
			continue;
		CNS varset = boost::accumulate(rng | map_values, CNS(),
			[] (auto& acc, auto& e) {
				boost::copy(e, std::inserter(acc, acc.end()));
				return acc;
			});
		SV ss;
		for (std::size_t i = 0; i < stmts.size(); i++)
			if (boost::find_if(stmt_mods[i], RefFinder(&varset))
							!= stmt_mods[i].end())
				ss.push_back(stmts[i]);
		assert(!ss.empty());
		result.push_back(ss);
	}
	return result;
}

/// If *init* is a variable declaration, replace all references in *stmt*
/// that reference the variable declared there with references to the
/// variable declared in *new_init*.
void update_references(SgStatement* stmt, SgForInitStatement* init,
					SgForInitStatement* new_init)
{
	using namespace boost::adaptors;
	auto old_inits = init->get_init_stmt();
	auto new_inits = init->get_init_stmt();
	assert(old_inits.size() == new_inits.size());
	auto oit = old_inits.begin();
	auto nit = new_inits.begin();
	for (; oit != old_inits.end(); oit++, nit++) {
		auto init_d = isSgVariableDeclaration(*oit);
		if (!init_d)
			continue;
		auto init_name = getFirstVarSym(init_d);
		auto new_d = isSgVariableDeclaration(*nit);
		assert(new_d);
		auto new_name = getFirstVarSym(new_d);
		boost::for_each(querySubTree<SgVarRefExp>(stmt)
				| filtered([init_name] (auto r) {
					return r->get_symbol() == init_name;
				}),
			[new_name] (auto r) {
				r->set_symbol(new_name);
			});
	}
}

} // namespace

/// Split all statements making up the body of *loop* into independent sets.
/// 1. Calculate dependencies for each modified variable;
/// 1. Create a map *idep*: `name -> VarDesc`
///    based on the calculated dependencies.
/// 1. Set *gc*, group counter, to 0.
/// 1. For each entry *e* in *idep*:
/// 1. If e is not assigned an id (e.second.gid == 0),
///    assign it a new gid (++gc), thus creating a new group.
/// 1. For each dependency *d*, check its gid. If gid is not assigned, set it to
///    *e*'s gid (*egid*), thus moving the dependency in the *e*'s group.
///    If it is set to *x* and *x* != *egid*, set gids of all
///    entries that have gid == *x* to *egid* (moving them to the *e*'s group).
/// 1. After that, for each set get all statements where at least one of its names
///    is modified, and put them into the result in the order they appear in the loop.
///    The resulting statement sets should not intersect currently.
///    This part is performed by descriptors_to_statements().
deps get_dependency_sets(SgForStatement* loop)
{
	using namespace boost::adaptors;
	auto body = get_nested_loops_main_block(loop);
	auto stmts = body->get_statements();
	auto stmt_mods = get_vars_modified_in_statements(stmts);
	CNS all_modified;
	for (auto& vars : stmt_mods) {
		if (vars.empty())
			throw Exception("There is a statement that doesn't modify any variables in the loop to be distributed.");
		all_modified.insert(vars.begin(), vars.end());
	}
	std::map<const SgInitializedName*, VarDesc> idep;
	boost::copy(get_dependant_variables(body, all_modified)
			| transformed([] (auto d) {
				return std::make_pair(d.first,
					VarDesc(0, std::move(d.second)));
			}), std::inserter(idep, idep.end()));

	std::size_t gc = 0;
	auto merge_groups = [&idep] (std::size_t dest, std::size_t src) {
		boost::for_each(idep | filtered([src] (auto& e) {
						return e.second.gid == src;
					}),
				[dest] (auto& e) {
					e.second.gid = dest;
				});
	};
	for (auto& e : idep) {
		auto& descriptor = e.second;
		if (!descriptor.gid)
			// assigning a new gid
			descriptor.gid = ++gc;
		for (auto& d : descriptor.deps) {
			try {
				auto& nm_entry = idep.at(d);
				if (!nm_entry.gid) {
					nm_entry.gid = descriptor.gid;
				} else if (nm_entry.gid != descriptor.gid) {
					merge_groups(descriptor.gid,
							nm_entry.gid);
				}
			} catch (std::out_of_range& x) {
				; // The dependency is not modified
			}
		}
	}
	boost::for_each(idep, [] (auto& e) {
			l << e.first << " -> " << e.second.gid << endl;
		});

	return descriptors_to_statements(std::move(idep), gc, std::move(stmts),
			std::move(stmt_mods));
}

/// Distributes the loop by splitting all its statements into
/// non-intersecting sets and making a separate copy for each of them.
void distribute_loop(SgForStatement* loop)
{
	using namespace SageBuilder;
	auto sets = get_dependency_sets(loop);
	auto statements = get_nested_loops_main_block(loop)->get_statements();
	std::size_t cumulative_size = std::accumulate(sets.begin(), sets.end(),
						0, [] (int sum, auto& s) {
							return sum + s.size();
						});
	l << "Total: " << cumulative_size << ", expected: "
		<< statements.size() << endl;
	assert(cumulative_size == statements.size());

	auto init = loop->get_for_init_stmt();
	auto test = loop->get_test();
	auto incr = loop->get_increment();
	/// \todo fix deep copy so that initializers in for init statement
	/// would also be copied
	for (auto &s : sets) {
		auto thee_body = buildBasicBlock();
		auto new_init = deepCopy(init);
		auto copy = buildForStatement(new_init, deepCopy(test),
					deepCopy(incr), thee_body);
		insertStatementBefore(loop, copy);
		for (auto stmt : s) {
			removeStatement(stmt);
			appendStatement(stmt, thee_body);
		}
		update_references(copy, init, new_init);
	}
	removeStatement(loop);
	//deepDelete(loop);
}

namespace {
/// Removes statements of *block* that do not modify
/// variables in *referenced_vars*
void remove_unused_statements(SgBasicBlock *block, const CNSV &modified_vars,
					const CNS &referenced_vars)
{
	auto statements = block->get_statements();
	for (unsigned i = 0; i < statements.size(); i++) {
		auto s = statements[i];
		auto cur_mod_vars = modified_vars[i];
		if (cur_mod_vars.empty()) // the statement has no side-effects
			continue;
		auto modified_ref = find_if(cur_mod_vars.begin(),
					cur_mod_vars.end(),
					RefFinder(&referenced_vars));
		if (modified_ref == cur_mod_vars.end()) {
			// none of the referenced variables are modified
			remove_statement(s);
			// deepDelete(s);
		}
	}

}
} // namespace

/// A function that throws if there's something wrong with the code.
///
/// Checks if variables that will be used in more than one copy of block
/// are not set to values depending on state of other such variables.
void LoopSplitter::check_split_possibility(const depv &dependant_vars)
{
	CNS intersection;
	for (unsigned i = 0; i < dependant_vars.size(); i++)
		for (unsigned j = i + 1; j < dependant_vars.size(); j++) {
			auto set0 = dependant_vars[i].second;
			auto set1 = dependant_vars[j].second;
			boost::set_intersection(set0, set1,
						inserter(intersection,
							intersection.end()));
		}
	for (auto index : loop_indices)
		intersection.erase(index);
	l << "Transformed intersection: " << &intersection << endl;
	depv dependencies = get_dependant_variables<true>(block, intersection);
	l << "Dependencies " << &dependencies << endl;
	for (auto vars : dependencies)
		for (auto var : vars.second) {
			auto dependency = intersection.find(var);
			if (dependency != intersection.end()) {
				std::stringstream e;
				e << "Unable to split loop as ";
				e << vars.first->get_name();
				e << " depends on ";
				e << (*dependency)->get_name();
				e << ", which would be left in more than one copy of the loop.";
				throw Exception(e.str(), this->loop);
			}
		}
}

/// Ascertains that all *statements* of a loop body
/// will be left in one of the copies after split.
void LoopSplitter::check_if_all_stmts_are_used(const SV &statements,
						const CNSV &modified_vars,
						const depv &dependencies)
{
	for (unsigned i = 0; i < statements.size(); i++) {
		auto s = statements[i];
		auto cur_mod_vars = modified_vars[i];
		if (cur_mod_vars.empty()) // wtf is this?
			continue;
		bool is_required = false;
		for (const auto &dep : dependencies) {
			auto req = dep.second;
			auto req_pos = find_if(cur_mod_vars.begin(),
						cur_mod_vars.end(), RefFinder(&req));
			if (req_pos != cur_mod_vars.end()) {
				is_required = true;
				break;
			}
		}
		if (!is_required)
			throw Exception(string("Unable to split loop as the statement would be removed.\n") +
					"Check that all required variables are supplied in transformation parameters.",
					this->loop, s);
	}

}

/// Copies *loop* *split*.size() - 1 times
/// and removes references to all but one variable
/// from that set in each copy
///
/// \todo if the loop has side-effects, they souldn't be removed!
void LoopSplitter::split_loop()
{
	auto statements = block->get_statements();

	l << "Calculating dependant variables for split." << endl;
	depv dependant_vars = get_dependant_variables(block, p);
	auto modified_vars = get_vars_modified_in_statements(statements);

	l << &dependant_vars << endl;

	check_if_all_stmts_are_used(statements, modified_vars, dependant_vars);
	check_split_possibility(dependant_vars);

	for (unsigned i = 0; i < dependant_vars.size(); i++) {
		SgStatement *copy = deepCopy(loop);
		insertStatementBefore(loop, copy);
		auto block = get_nested_loops_main_block(copy);
		remove_unused_statements(block, modified_vars,
					get<1>(dependant_vars[i]));
	}
	removeStatement(loop);
	deepDelete(loop);
}

LoopSplitter::LoopSplitter(SgForStatement *loop, const STRS &split)
	:loop(loop), p(split)
{
	block = get_nested_loops_main_block(loop);
	get_nested_loops_indices(loop, loop_indices);
}
} // namespace LoopTransformations

