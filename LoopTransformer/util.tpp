#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#include "util.h"
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
// reimplementing library functions...
#include "ArrayAnnot.h"
#include "ArrayInterface.h"

#include "DFAstAttribute.h"
#include "DepInfoAnal.h"

#include "LVAnalysis.h"
#include "VariableIdMapping.h"

namespace TfUtils {

using SageInterface::querySubTree;
using SageInterface::isArrayReference;
using SPRAY::RDAnalysis;
using SPRAY::Lattice;
using SPRAY::Labeler;
using SPRAY::RDLattice;
using SPRAY::VariableId;

/// A handy erase_if implementation for associative containers.
///
template<typename ContainerT, typename PredicateT>
void erase_if(ContainerT& items, const PredicateT& predicate)
{
	for (auto it = items.begin(); it != items.end(); ) {
		if (predicate(*it))
			it = items.erase(it);
		else
			++it;
	}
};

/// Non-modifying querySubTree implementation.
///
/// We have to drop const qualifier as otherwise
/// we'd have to reimplement all other querySubTree templates
/// \todo reimplement them.
template <typename NodeType>
std::vector<const NodeType*> querySubTree(const SgNode *top,
				VariantT variant/* = (VariantT)NodeType::static_variant*/)
{
	Rose_STL_Container<SgNode*> nodes = NodeQuery::querySubTree(const_cast<SgNode*>(top), variant);
	std::vector<const NodeType*> result(nodes.size(), nullptr);
	int count = 0;
	for (Rose_STL_Container<SgNode*>::const_iterator i = nodes.begin(); i != nodes.end(); ++i, ++count) {
		NodeType* node = dynamic_cast<NodeType*>(*i);
		ROSE_ASSERT(node);
		result[count] = const_cast<const NodeType*>(node);
	}
	return result;
}

/// Faster implementations of querySubTree().
/// const_cast is still required.
template <typename NodeType, typename ContainerT>
void query_subtree(const SgNode *top, ContainerT &result, VariantT variant)
{
	Rose_STL_Container<SgNode*> nodes = NodeQuery::querySubTree(const_cast<SgNode*>(top), variant);
	result.reserve(nodes.size());
	for (auto &n : nodes) {
		NodeType* node = dynamic_cast<NodeType*>(n);
		result.insert(result.end(), const_cast<const NodeType*>(node));
	}
}

template <typename NodeType, typename ContainerT>
void query_subtree(SgNode *top, ContainerT &result, VariantT variant)
{
	Rose_STL_Container<SgNode*> nodes = NodeQuery::querySubTree(top, variant);
	result.reserve(nodes.size());
	for (auto &n : nodes) {
		NodeType* node = dynamic_cast<NodeType*>(n);
		result.insert(result.end(), node);
	}
}

/// Const reimplementation of collectReadWriteRefs(SgStatement).
///
template<typename NodeT>
bool collectReadWriteRefs(const NodeT *stmt, std::vector<const SgNode*> &readRefs,
				std::vector<const SgNode*> &writeRefs)
{
	assert(stmt);

	// get function level information
	auto funcDef = SageInterface::getEnclosingNode<SgFunctionDefinition>(stmt);
	assert(funcDef);
	auto funcBody = funcDef->get_body();
	assert(funcBody);

	// prepare Loop transformation environment
	AstInterfaceImpl faImpl(funcBody);
	AstInterface fa(&faImpl);
	ArrayAnnotation* annot = ArrayAnnotation::get_inst();
	bool useCachedDefUse = false;
	if (useCachedDefUse) {
		ArrayInterface* array_interface = ArrayInterface::get_inst(*annot, fa, funcDef, AstNodePtrImpl(funcDef));
		LoopTransformInterface::set_arrayInfo(array_interface);
	} else {
		ArrayInterface array_interface(*annot);
		array_interface.initialize(fa, AstNodePtrImpl(funcDef));
		array_interface.observe(fa);
		LoopTransformInterface::set_arrayInfo(&array_interface);
	}
	LoopTransformInterface::set_astInterface(fa);

	// variables to store results
	DoublyLinkedListWrap<AstNodePtr> rRef1, wRef1;
	CollectDoublyLinkedList<AstNodePtr> crRef1(rRef1),cwRef1(wRef1);
	/// \todo remove const_cast.... somehow? somewhen?
	AstNodePtr s1 = AstNodePtrImpl(const_cast<NodeT*>(stmt));

	// Actual side effect analysis
	if (!AnalyzeStmtRefs(fa, s1, cwRef1, crRef1))
		return false;

	for (DoublyLinkedEntryWrap<AstNodePtr>* p = rRef1.First(); p != 0; ) {
		DoublyLinkedEntryWrap<AstNodePtr>* p1 = p;
		p = rRef1.Next(p);
		AstNodePtr cur = p1->GetEntry();
		SgNode* sgRef = AstNodePtrImpl(cur).get_ptr();
		assert(sgRef);
		readRefs.push_back(sgRef);
	}
	for (DoublyLinkedEntryWrap<AstNodePtr>* p = wRef1.First(); p != 0; ) {
		DoublyLinkedEntryWrap<AstNodePtr>* p1 = p;
		p = wRef1.Next(p);
		AstNodePtr cur = p1->GetEntry();
		SgNode* sgRef = AstNodePtrImpl(cur).get_ptr();
		ROSE_ASSERT(sgRef != nullptr);
		writeRefs.push_back(sgRef);
	}

	return true;
}

/// A simple implementation of the collectReadWriteVariables()
/// for the cases when it fails (when there is a function call).
template<typename NodeT, typename ContainerT>
void compute_read_write_vars(const NodeT* node, ContainerT &read_vars, ContainerT &write_vars)
{
	assert(node);
	CEV refs;
	get_all_references(node, refs);
	for (auto ref : refs) {
		bool is_read;
		if (is_modified(ref, &is_read) || is_output(ref)) {
			write_vars.insert(write_vars.end(), get_var_initialized_name(ref));
			if (!is_read)
				continue;
		}
		read_vars.insert(read_vars.end(), get_var_initialized_name(ref));
	}
	std::vector<const SgVariableDeclaration*> decls;
	query_subtree<SgVariableDeclaration>(node, decls);
	for (auto decl : decls)
		write_vars.insert(write_vars.end(), (decl->get_variables())[0]);
}

/// Reimplementation of collectReadWriteVariables().
///
template<typename NodeT, typename ContainerT>
void get_rw_vars(const NodeT* node, ContainerT &read_vars, ContainerT &write_vars)
{
	assert(node);
	std::vector<const SgNode*> read_refs, write_refs;
	if (!collectReadWriteRefs(node, read_refs, write_refs)) {
		// fallback in case the analysis failed (it is possible)
		compute_read_write_vars(node, read_vars, write_vars);
		return;
	}
	for (auto n : read_refs) {
		if (!is_a_var_reference(n))
			continue;
		auto name = convertRefToInitializedName(n);
		if (name)
			read_vars.insert(read_vars.end(), name);
	}
	for (auto n : write_refs) {
		if (!is_a_var_reference(n))
			continue;
		auto name = convertRefToInitializedName(n);
		if (name)
			write_vars.insert(write_vars.end(), name);
	}

}

/// Fetches modified vars for each of the *statements* into *result*.
template<typename ContainerT>
vector<CNS> get_vars_modified_in_statements(const ContainerT& statements)
{
	using namespace boost::adaptors;
	vector<CNS> result;
	result.reserve(statements.size());
	boost::copy(statements | transformed([] (const SgStatement* s) {
		CNS r, w;
		get_rw_vars(s, r, w);
		return w;
	}), std::back_inserter(result));
	return result;
}

/// Puts variables referenced in *node* into *result*.
///
/// Currently is a wrapper for collectReadWriteVariables().
template<typename NodeT, typename ContainerT>
void get_referenced_vars(const NodeT* node, ContainerT &result)
{
	CNS read, write;
	get_rw_vars(node, read, write);
	set_union(read.begin(), read.end(), write.begin(), write.end(), inserter(result, result.end()));
}

/// Returns a set with all variables referenced in *node*.
template<typename NodeT>
NS get_referenced_vars(NodeT* node)
{
	using namespace boost::adaptors;
	assert(node);
	NS result;
	EV refs;
	get_all_references(node, refs);
	boost::copy(refs | transformed([] (auto& r) {
				return get_var_initialized_name(r);
			}), std::inserter(result, result.end()));
	return result;
}

/// Fetches variables that are read and not written in *node*.
///
template<typename NodeT, typename ContainerT>
void get_readonly_vars(const NodeT* node, ContainerT &result)
{
	CNS read, write;
	get_rw_vars(node, read, write);
	set_difference(read.begin(), read.end(), write.begin(), write.end(), inserter(result, result.end()));
}

/// Get modified variables for modifiers.
///
/// Examples:
///
/// * x for `x[i, j] = i + j`
/// * y for `y++`
/// * z, x for `z += x = y`
///
/// The first assigned variable is always first in the result,
/// order of others is unspecified. [is is true?]
///
/// If *only_reset* is true only assignments are handled.
///
/// \todo maybe get code from ROSE's collectReadWriteVariables?
/// \todo add a way to distinguish regular assignments from
/// thansformed modifiers
template<typename NodeT, typename ContainerT>
void get_modified_variables(NodeT *node, ContainerT &result, bool only_update)
{
	auto binary = querySubTree<SgBinaryOp>(node);
	for (auto b : binary) {
		// the following is because = and +=, -=, etc are
		// two assignment classes
		if (!isSgAssignOp(b) && !isSgCompoundAssignOp(b))
			continue;
		auto name = get_var_initialized_name(b->get_lhs_operand());
		if (name) {
			if (!only_update || isSgCompoundAssignOp(b)) {
				result.insert(result.end(), name);
			} else {
				if (is_modifier(isSgAssignOp(b)))
					result.insert(result.end(), name);
			}
		}
	}

	auto unary = querySubTree<SgUnaryOp>(node);
	for (auto u : unary) {
		if (!isSgMinusMinusOp(u) && !isSgPlusPlusOp(u))
			continue;
		auto name = get_var_initialized_name(u->get_operand());
		if (name)
			result.insert(result.end(), name);
	}

	auto init = isSgVariableDeclaration(node);
	if (init)
		result.insert(result.end(), (init->get_variables())[0]);
}

/// Puts index variables of all perfectly nested loops located at *loops*
/// into *loop_indices*.
///
/// A variable is considered index in a for loop if it is modified in
/// its header, so for `for (; i++, ++j; x < 5)` this function will extract {i, j}.
template<typename LoopT, typename ContainerT>
void get_nested_loops_indices(const LoopT *loops, ContainerT &loop_indices)
{
	get_modified_variables(get_exp_with_loop_indices(loops), loop_indices);
	SgStatement *body = get_loop_body(loops);
	auto block = isSgBasicBlock(body);
	if (body->variantT() == (VariantT)LoopT::static_variant) {
		get_nested_loops_indices(dynamic_cast<const LoopT*>(body), loop_indices);
	} else if (block) {
		auto body_statements = block->get_statements();
		if ((body_statements.size() == 1) && (body_statements[0]->variantT() == (VariantT)LoopT::static_variant))
			get_nested_loops_indices(dynamic_cast<const LoopT*>(body_statements[0]), loop_indices);
	}
}

/// Returns a block that is being executed in perfectly nested loop nest
/// pointed at by *loops*.
///
/// The block is created if it didn't exist. If *depth* != -1, stops when reaches the body
/// of a loop at *depth* level.
///
/// \todo somehow handle cases when the cast fails
template<typename LoopT>
SgStatement* get_nested_loops_body(const LoopT *loops, int depth/* = -1*/)
{
	SgStatement *body = get_loop_body(loops);

	if (depth != -1) {
		if (depth == 1)
			return body;
		else
			depth--;
	}
	auto block = isSgBasicBlock(body);
	if (body->variantT() == (VariantT)LoopT::static_variant) {
		return get_nested_loops_body(dynamic_cast<const LoopT*>(body));
	} else if (block) {
		auto body_statements = block->get_statements();
		if ((body_statements.size() == 1) && (body_statements[0]->variantT() == (VariantT)LoopT::static_variant))
			return get_nested_loops_body(dynamic_cast<const LoopT*>(body_statements[0]));
	}
	return body;
}

/// Queries all PntrArrRefs and VarRefs
/// which are not contained in another such node
///
/// \todo a[b[c[0]]] => c is returned instead of c[0]
template<typename NodeT, typename ContainerT>
void get_all_references(NodeT *node, ContainerT &container)
{
	typedef typename ContainerT::value_type container_value_t;
	std::vector<container_value_t> arr_refs;
	query_subtree<SgPntrArrRefExp>(node, arr_refs);
	//auto arr_refs = querySubTree<SgPntrArrRefExp>(node);
	arr_refs.erase(remove_if(arr_refs.begin(), arr_refs.end(),
			[] (const SgNode *ref) {
				auto p = isSgPntrArrRefExp(ref->get_parent());
				// If ref is in rhs of p, it's not a
				// subscript but an index, like in a[b[c]].
				return p && (p->get_lhs_operand_i() == ref);
			}),
			arr_refs.end());
	std::vector<container_value_t> subscripts;
	auto p_subscripts = &subscripts;
	std::set<container_value_t> arrays;
	container_value_t ar;
	for (auto r : arr_refs) {
		isArrayReference(r, &ar, &p_subscripts);
		arrays.insert(ar);
	}
	for (auto s : subscripts) {
		if (isArrayReference(s, &ar)) {
			arrays.insert(ar);
			arr_refs.push_back(isSgPntrArrRefExp(s));
		}
	}
	std::vector<container_value_t> var_refs;
	query_subtree<SgVarRefExp>(node, var_refs);
	//auto var_refs = querySubTree<SgVarRefExp>(node);
	var_refs.erase(remove_if(var_refs.begin(), var_refs.end(),
				[&arrays] (container_value_t &ref) { return arrays.count(ref); }),
			var_refs.end());

	for (auto &r : var_refs)
		container.insert(container.end(), r);
	for (auto &r : arr_refs)
		container.insert(container.end(), r);
}

/// Gets all enclosing nodes of type NodeT and puts them into *container*.
///
template<typename NodeT, typename SourceT, typename ContainerT>
void get_enclosing_nodes(SourceT *node, ContainerT &container,
				bool including_self/* = true*/)
{
	auto n = SageInterface::getEnclosingNode<NodeT>(node, including_self);
	while (n) {
		container.insert(container.end(), n);
		n = SageInterface::getEnclosingNode<NodeT>(n, false);
	}
}

template<typename MapKeyT, typename MapElemT>
bool RefFinder::operator() (const std::pair<MapKeyT, MapElemT> &p) const
{
	auto e = p.first;
	CEV e_subexps;
	query_subtree<SgExpression>(e, e_subexps);
	if (e_subexps.size() != subexps->size())
		return negative; // false if not negative
	for (size_t i = 0; i < subexps->size(); i++) {
		auto e0 = e_subexps.at(i);
		auto e1 = subexps->at(i);

		auto val0 = isSgIntVal(e0);
		auto val1 = isSgIntVal(e1);
		if (val0 && val1 && (val0->get_value() == val1->get_value()))
			continue;
		//TODO: check if var is the bottom node?
		auto var0 = isSgVarRefExp(e0);
		auto var1 = isSgVarRefExp(e1);
		if (var0 && var1 && (get_var_name(var0) == get_var_name(var1)))
			continue;
		auto ref0 = isSgPntrArrRefExp(e0);
		auto ref1 = isSgPntrArrRefExp(e1);
		if (ref0 && ref1)
			continue;
		return negative;
	}
	return !negative;
}

template<typename... BoolT>
int how_many_of(BoolT... args)
{
	bool values[] { args... };
	int result = 0;
	for (auto v : values)
		result += v;
	return result;
}

/// If given an expression statement, return the expression it contains.
template<typename NodeT,
	std::enable_if_t<
			std::is_same<std::remove_const_t<NodeT>,
					SgStatement*>::value
			>* = nullptr
	>
auto stmt_to_expr(NodeT s)
{
	using ResultT = std::conditional_t<
				std::is_const<std::remove_pointer_t<NodeT>>
									::value,
				const SgExpression*,
				SgExpression*>;
	auto es = isSgExprStatement(s);
	if (!es)
		return reinterpret_cast<ResultT>(0);
	return es->get_expression();
}

/// Implementation of getEnclosingNode that can stop at the specified border.
/// It returns nulltr in that case.
template <typename NodeType>
NodeType* getEnclosingNode(SgNode* node, SgNode* stopAt/* = nullptr*/,
				const bool includingSelf/* = false*/)
{
	// Find the parent of specified type, but watch out for cycles in the ancestry (which would cause an infinite loop).
	node = (!node || includingSelf) ? node : node->get_parent();
	std::set<const SgNode*> seen; // nodes we've seen, in order to detect cycles
	while (node) {
		if (NodeType *found = dynamic_cast<NodeType*>(node))
			return found;
		if (node == stopAt)
			return nullptr;

		// Traverse to parent (declaration statements are a special case)
		if (SgDeclarationStatement *declarationStatement = isSgDeclarationStatement(node)) {
			auto dd = declarationStatement->get_definingDeclaration();
			auto first_ndd = declarationStatement->get_firstNondefiningDeclaration();
			if (dd && first_ndd && (declarationStatement != first_ndd)) {
				node = dd;
			}
		} else {
			node = node->get_parent();
		}
	}
	return nullptr;
}

}

