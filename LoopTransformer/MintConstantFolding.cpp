/*
	A wrapper for the ROSE's constant folding optimization

	It is very important for Mint to be able
	eliminate all the constants in the index expression
	to perform the on-chip memory optimizations.

	We perform constant folding specificially on
	the array index expressions.

*/

#include "MintConstantFolding.h"
#include "sage3basic.h"
#include "constantFolding.h"

using namespace std;
using namespace SageBuilder;
using namespace SageInterface;

typedef std::map <SgInitializedName*, std::vector < SgExpression*> > MintInitNameMapExpList_t;

static bool isArrayReference(SgExpression* ref, SgExpression** arrayName/*=nullptr*/, std::vector<SgExpression*>* subscripts/*=nullptr*/);
static void getArrayReferenceList(SgNode* kernel_body, MintInitNameMapExpList_t& refList, bool sharedMemRef = true);

void MintConstantFolding::constantFoldingOptimization(SgNode* node, bool remove_casts, bool internalTestingAgainstFrontend)
{
	ROSE_ASSERT(node);

	//calling ROSE's constant folding optimizer first
	ConstantFolding::constantFoldingOptimization(node, internalTestingAgainstFrontend);

	//if ROSE cannot figure out the constants in
	//an expression like this one: A[i + 2 - 3 ]
	//then we call our own constant folding optimizer

	//constant fold the array index expressions
	constantFoldingOnArrayIndexExpressions(node);

	//constant fold the upper bound of loop range
	//constantFoldingOnLoopRange(node);

	if (remove_casts)
		remove_unsigned_casts(node);
}

//bool canWeFold(SgExpression* index, bool minusSign = false)
bool canWeFold(SgExpression* index, bool * minusSign)
{
	//we count the (-) signs affecting the index variable
	//to understand if we should add a minus sign to the variable
	//after folding or not

	int sign = 0;
	bool folding =true;

	Rose_STL_Container<SgNode*> constList = NodeQuery::querySubTree(index, V_SgIntVal);

	//we need at least 2 constants to fold
	if(constList.size() <= 1) {
		folding = false;
	}

	Rose_STL_Container<SgNode*> indexVarExp = NodeQuery::querySubTree(index, V_SgVarRefExp);
	if (indexVarExp.size() == 0 ) { //e.g A[1 + 2 - 3]
		//ROSE handled this
		folding = false;
	} else if (indexVarExp.size() > 1) { //e.g A[i + 2 - 2 + j - 1]
		//cannot handle this case, it is not so common anyways
		folding = false;
	} else { //size() == 1 //e.g A[i + 2 - 2 - 1]
		Rose_STL_Container<SgNode*> expList = NodeQuery::querySubTree(index, V_SgExpression);
		Rose_STL_Container<SgNode*>::iterator exp;

		for (exp=expList.begin(); exp != expList.end() ; exp++) {
			SgExpression* expression= isSgExpression(*exp);

			//All the binary operations should be -/+
			if (isSgBinaryOp(expression)) {
				if (!(isSgAddOp(expression) || isSgSubtractOp(expression))) {
					// we want all operators either - or +
					folding = false;
					break;
				} else if (isSgSubtractOp(expression)) {
					// check if the binary operation changes the sign of the varref
					SgExpression* rhs = isSgBinaryOp(expression)->get_rhs_operand();
					ROSE_ASSERT(rhs);
					Rose_STL_Container<SgNode*> varList = NodeQuery::querySubTree(rhs, V_SgVarRefExp);
					if (varList.size() > 0)
						sign++;
				}
			} else if (isSgMinusOp(expression)) {
				SgExpression* rhs = isSgMinusOp(expression)->get_operand();
				ROSE_ASSERT(rhs);
				Rose_STL_Container<SgNode*> varList = NodeQuery::querySubTree(rhs, V_SgVarRefExp);
				if(varList.size() > 0)
				  sign++;

			} else if(isSgIntVal(expression) || isSgVarRefExp(expression)) {
				  //do nothing
			} else {
				folding = false;
				break;
			}
		}
	}

	if (sign % 2 != 0)
		*minusSign = true;

	return folding;
}

/// Assumptions: constant array indices are always integer
/// we replace var ref exp with 0 and then put that back to the index var
/// after constant folding
/// e.g let's say we have an expression A[i + 2 - 3]
/// 1.step we replace i with 0 (we previously check that all the operations are either -/+)
/// if there is only one occurance of i in the index expression
/// 2.step we call ROSE's constant folding function
/// 3.step we create new expression using the constant and i
/// 4.step replace it with the old index expression
/// Tricky part is to figure out the sign of i.
void MintConstantFolding::constantFoldingOnArrayIndexExpressions(SgNode* node)
{
	MintInitNameMapExpList_t arrRefList;
	getArrayReferenceList(node, arrRefList);

	MintInitNameMapExpList_t::iterator it;

	for (it = arrRefList.begin(); it != arrRefList.end(); it++) {
		SgInitializedName* array = it->first;
		ROSE_ASSERT(array);
		std::vector<SgExpression*> expList = it->second;

		std::vector<SgExpression*>::iterator arrayNode;

		for (arrayNode = expList.begin(); arrayNode != expList.end() ; arrayNode++) {
			ROSE_ASSERT(*arrayNode);

			SgExpression* arrRefWithIndices = isSgExpression(*arrayNode);

			SgExpression* arrayExp;
			vector<SgExpression*>  subscripts; //first index is i if E[j][i]

			if (isArrayReference(arrRefWithIndices, &arrayExp, &subscripts)) {
				std::vector<SgExpression*>::reverse_iterator sub;
				int subNo = subscripts.size();

				for (sub = subscripts.rbegin(); sub != subscripts.rend(); sub++) {
					subNo--;

					//SgExpression* orig_index = *sub;
					SgExpression* index = *sub;

					bool minusSign = false;
					bool folding = canWeFold(index, &minusSign);

					if (folding) {
						Rose_STL_Container<SgNode*> indexVarExp = NodeQuery::querySubTree(index, V_SgVarRefExp);
						ROSE_ASSERT(indexVarExp.size() ==1);

						SgExpression* indexVar = isSgExpression(*(indexVarExp.begin()));
						SgExpression* orig_IndexVar = copyExpression(indexVar);

						replaceExpression(indexVar, buildIntVal(0));

						ConstantFolding::constantFoldingOptimization(arrRefWithIndices);

						//TODO: need to figure out the sign of index var -/+
						//insert back the index var i to the index expression -/+ i + constant index exp
						//we computed that and stored its sign in minusSign variable

						SgExpression* newArrayExp;
						vector<SgExpression*>  newSubscripts; //first index is i if E[j][i]

						if (isArrayReference(arrRefWithIndices, &newArrayExp, &newSubscripts)) {
							vector<SgExpression*>::iterator it = newSubscripts.begin();
							SgExpression* newIndex = *(it + subNo);

							ROSE_ASSERT(newIndex);

							if (isSgIntVal(newIndex)) {
								int folded_val = isSgIntVal(newIndex)->get_value();
								SgExpression* newExp = nullptr;

								if (folded_val > 0 ) {
									if (minusSign)
										newExp = buildSubtractOp(buildIntVal(folded_val), orig_IndexVar);
									else
										newExp = buildAddOp(orig_IndexVar, buildIntVal(folded_val));
								} else if (folded_val == 0 ) {
									newExp = orig_IndexVar;
									if (minusSign)
										newExp = buildMinusOp(orig_IndexVar);
								} else {
									if (minusSign)
										newExp = buildSubtractOp(buildMinusOp(orig_IndexVar), buildIntVal(-folded_val));
									else
										newExp = buildSubtractOp(orig_IndexVar, buildIntVal(-folded_val));
								}
								replaceExpression(newIndex, newExp);
							}
						}
					}//end of folding

				  }//end of for subscript

			}//end of if array exp

		}//end of for references of an array

	}//end of array ref list
}

bool isArrayReference(SgExpression* ref, SgExpression** arrayName/*=nullptr*/, vector<SgExpression*>* subscripts/*=nullptr*/)
{
	SgExpression* arrayRef=nullptr;

	if (ref->variantT() == V_SgPntrArrRefExp) {
		if (subscripts != 0 || arrayName != 0) {
			SgExpression* n = ref;
			while (true) {
				SgPntrArrRefExp *arr = isSgPntrArrRefExp(n);
				if (arr == 0)
					break;
				n = arr->get_lhs_operand();
				// store left hand for possible reference exp to array variable
				if (arrayName!= 0)
					arrayRef = n;
				// right hand stores subscripts
				if (subscripts != 0){
					subscripts->push_back(arr->get_rhs_operand());
					//cout << "sub: " << (arr->get_rhs_operand())->unparseToString() << endl;
				}
			} // end while
			if (arrayName !=nullptr) {
				*arrayName = arrayRef;
			}
		}
		return true;
	}
	return false;
}

void getArrayReferenceList(SgNode* kernel_body, MintInitNameMapExpList_t& refList, bool sharedMemRef)
{
	//sharedMemRef= true, then return shared memory references as well
	//finds all the array references and returns them in a vector
	//groups the array references according to their init name
	//returns a map of expression vector where array ref name is the key
	//For example: E-> E[i][j], E[i+1][j], E[i][j]
	//             R-> R[i][j]

	Rose_STL_Container<SgNode*> arrList = NodeQuery::querySubTree(kernel_body, V_SgPntrArrRefExp);

	Rose_STL_Container<SgNode*>::iterator arr;

	if (sharedMemRef) {
		for(arr = arrList.begin(); arr != arrList.end(); arr++) {

			SgExpression* arrayName;
			vector<SgExpression*>  subscripts; //first index is i if E[j][i]

			//index list are from right to left
			bool yes = isArrayReference(isSgExpression(*arr), &arrayName, &subscripts);
			assert(yes);

			auto array_var = isSgVarRefExp(arrayName);
			if (!array_var) // well, I have no idea why this can happen
				continue;
			SgInitializedName *i_name = SageInterface::convertRefToInitializedName(array_var);

			ROSE_ASSERT(i_name);

			//this is the first time the var appears
			if(refList.find(i_name) == refList.end()) {
				std::vector<SgExpression*> exp;
				exp.push_back(isSgExpression(*arr));

				pair< MintInitNameMapExpList_t ::iterator, bool> ret;

				ret = refList.insert(make_pair(i_name, exp));
			} else {
				refList[i_name].push_back(isSgExpression(*arr));
			}

			int arrayDim = subscripts.size() ;
			arr = arr + arrayDim - 1;
		}
	}
}

/// removes *all* c-style casts from e
/// TODO: probably leave some of them?
void MintConstantFolding::remove_unsigned_casts(SgNode *e)
{
	auto casts = SageInterface::querySubTree<SgCastExp>(e);
	for (auto i = casts.rbegin(); i != casts.rend(); i++) {
		auto c = *i;
		auto parent = c->get_parent();
		auto p_exp = isSgExpression(parent);
		if (!p_exp)
			continue;
		// if (c->get_cast_type() == SgCastExp::e_C_style_cast)
		replaceExpression(c, c->get_operand(), true);
	}
}
