#define BOOST_RESULT_OF_USE_DECLTYPE
#include "DependencyAnalysis.h"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <utility>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/adaptors.hpp>
#include "sage3basic.h"
#include "constantFolding.h"
#include "util.h"
#include "StatementTransformations.h"

using namespace TfUtils;
using namespace SageInterface;
using std::endl;
using std::ostream;

namespace CodeAnalyses {

namespace {

enum class AccessType {
	r,
	w,
	rw
};

AccessType access_type(SgExpression* e)
{
	bool is_read;
	if (is_modified(e, &is_read) || is_output(e)) {
		if (is_read)
			return AccessType::rw;
		else
			return AccessType::w;
	} else {
		return AccessType::r;
	}
}

struct DataAccess {
	SgExpression* e;
	AccessType t;

	DataAccess(SgExpression* e): e(e), t(access_type(e)) {}
};

struct LoopInfo {
	SgForStatement* loop{nullptr};
	bool canonical{false};
	SgInitializedName* name{nullptr};
	// If loop is not canonical, it can have
	// more than one index. Store them here...
	NS all_indices;
	SgExpression* lb{nullptr};
	SgExpression* ub{nullptr};
	int step{0};

	LoopInfo(SgForStatement* l)
		: loop(l)
	{
		using namespace SageBuilder;
		using namespace boost::adaptors;
		SgExpression* s;
		bool incr_it_space, incl_ub;
		canonical = isCanonicalForLoop(l, &name, &lb, &ub, &s, NULL,
						&incr_it_space, &incl_ub);
		if (!canonical) {
			canonical = false;
			// non-canonical loop found, try to get index info
			boost::copy(querySubTree<SgVarRefExp>(l->get_increment())
					| transformed(initialized_name),
				std::inserter(all_indices, all_indices.end()));
		}
		if (incl_ub)
			ub = buildSubtractOp(ub, buildIntVal(1));
		if (auto v = isSgIntVal(s)) {
			// TODO generalize these things
			step = v->get_value();
		} else {
			canonical = false;
			return;
		}
		if (!incr_it_space)
			step = -step;
	}
};

/// Stores information about the environment of a DependencyDetector
/// in question, currently just info about the surrounding loops.
class EnvironmentInfo {
	std::vector<LoopInfo> loops;
	NV indices;
	DefinitionProvider rd;
public:
	// TODO put loops in the order they appear?
	EnvironmentInfo(SgForStatement* l)
		: rd(getEnclosingProcedure(l), false)
	{
		using namespace boost::adaptors;
		boost::copy(get_all_nested_loops(l)
				| transformed([] (auto loop) {
					return LoopInfo(loop);
				}), std::back_inserter(loops));
		boost::copy(get_all_surrounding_loops(l)
				| transformed([] (auto loop) {
					return LoopInfo(loop);
				}), std::back_inserter(loops));
		for (auto li : loops) {
			if (li.canonical)
				indices.push_back(li.name);
			else
				indices.insert(indices.end(),
						li.all_indices.begin(),
						li.all_indices.end());
		}
		rd.generate(); // TODO do only when necessary?
	}
	const auto& get_loops() const { return loops; }
	const auto& get_indices() const { return indices; }
	const LoopInfo* get_loop_info(SgInitializedName* index) const;
};

const LoopInfo* EnvironmentInfo::get_loop_info(SgInitializedName* index) const
{
	auto it = boost::find_if(loops, [index] (auto& l) {
				if (l.name == index)
					return true;
				if (!l.canonical && l.all_indices.find(index)
							!= l.all_indices.end())
					return true;
				return false;
			});
	if (it != loops.end())
		return &(*it);
	else
		return nullptr;
}

/// Represents the concept of dependency.
/// Source here is which access happens first, sink is which is second (I think).
/// Source and sink contain either references to the same variable
/// or access to elements of one array. In both cases information
/// on whether read or write happens is stored.
class Dependency {
public:
	enum class OperationType {
		unknown,
		flow,
		anti,
		output,
		input,
	};
	enum class OrderType {
		forward,
		self,
		backward,
	};
	class LoopInfo;

private:
	SgStatement* source{nullptr};
	SgStatement* sink{nullptr};

	OperationType operation_type{OperationType::unknown};

	bool present{true};

	std::unique_ptr<LoopInfo> loop_info;

public:
	Dependency(DataAccess src, DataAccess snk);
	Dependency(SgExpression* src, SgExpression* snk);
	Dependency(DataAccess src, DataAccess snk, const NS& indices);
	Dependency(SgExpression* src, SgExpression* snk, const NS& indices);
	Dependency(Dependency&& d) = default;
	Dependency& operator=(Dependency&& d) = default;
	~Dependency() = default;
	void merge(Dependency&& b);
	auto get_source() const { return source; }
	auto get_sink() const { return sink; }
	auto get_operation_type() const { return operation_type; }
	bool is_loop_carried() const { return loop_info != nullptr; }
	auto& loop() { return *loop_info; }
	bool exists() const { return present; }
	void make_independent() { present = false; }
	void dump() const;
};

Dependency::Dependency(SgExpression* src, SgExpression* snk)
	: Dependency(DataAccess(src), DataAccess(snk))
{
}

Dependency::Dependency(DataAccess src, DataAccess snk)
{
	auto src_stmt = getEnclosingNode<SgStatement>(src.e);
	auto snk_stmt = getEnclosingNode<SgStatement>(snk.e);
	assert(src_stmt && snk_stmt);
	source = src_stmt;
	sink = snk_stmt;
	if (source == sink) {
		// TODO consider expression priority rules here?
		operation_type = OperationType::unknown;
		return;
	}
	if (src.t == AccessType::r) {
		if (snk.t == AccessType::r)
			operation_type = OperationType::input;
		else
			operation_type = OperationType::anti;
	} else {
		if (snk.t == AccessType::r)
			operation_type = OperationType::flow;
		else
			operation_type = OperationType::output;
	}
}

/// If the dependency is loop-carried, then two references to one array happen.
/// In this case we can get more precise information, like which concrete
/// iterations bring the dependency, a direction vector or a
/// a distance vector.
class Dependency::LoopInfo {
public:
	enum class Direction {
		straight, // <
		none, // =
		backward, // >
		any, // *
		unspecified, // happens only on distinct iterations
	};

private:
	NV indices;
	std::vector<Direction> dirv;
	std::vector<int> distv;
	std::vector<set<int>> iterv;
	OrderType order_type; // TODO what is this?

	friend ostream& operator<<(ostream& o, Direction s);
public:
	LoopInfo(const NS& i)
		: indices(i.begin(), i.end()), dirv(i.size(), Direction::any),
		distv(i.size(), 0), iterv(i.size(), std::set<int>())
	{}
	// LoopInfo(SgInitializedName* i) : indices(1, i), dirv(1, Direction::any)
	// {}
	const auto& get_directions() const { return dirv; }
	const auto& get_distances() const { return distv; }
	const auto& get_iterations() const { return iterv; }
	const auto& get_indices() const { return indices; }
	void merge(Dependency::LoopInfo&& other);
	void normalize();
	// Directions can perhaps be turned into differences,
	// but otherwise these are incompatible --
	// if something is added at i, nothing else should be set for i before that.
	void set_direction(unsigned i, Direction d);
	void set_distance(unsigned i, int d);
	void add_iteration(unsigned i, int it);
	OrderType get_order_type() const { return order_type; }
};

Dependency::Dependency(SgExpression* src, SgExpression* snk,
			const NS& indices)
	: Dependency(src, snk)
{
	loop_info = std::make_unique<Dependency::LoopInfo>(indices);
}

Dependency::Dependency(DataAccess src, DataAccess snk, const NS& indices)
	: Dependency(src, snk)
{
	loop_info = std::make_unique<Dependency::LoopInfo>(indices);
}

void Dependency::LoopInfo::set_distance(unsigned i, int d)
{
	distv[i] = d;
	Direction dir = d ? Direction::straight : Direction::none;
	dir = d > 0 ? Direction::backward : Direction::straight;
	dirv[i] = dir;
	assert(iterv[i].empty());
}

void Dependency::LoopInfo::set_direction(unsigned i, Dependency::LoopInfo::Direction d)
{
	dirv[i] = d;
	assert(distv[i] == 0);
}

void Dependency::LoopInfo::add_iteration(unsigned i, int it)
{
	iterv[i].insert(it);
	dirv[i] = Direction::unspecified;
	assert(distv[i] == 0);
}

void Dependency::LoopInfo::merge(Dependency::LoopInfo&& o)
{
	assert(indices.size() >= o.indices.size());
	for (unsigned i = 0; i != o.indices.size(); i++) {
		const auto& index = o.indices[i];
		auto it = boost::range::find(indices, index);
		assert(it != indices.end());
		auto pos = std::distance(indices.begin(), it);
		assert(dirv[pos] == Direction::any);
		dirv[pos] = o.dirv[i];
		assert(distv[pos] == 0);
		distv[pos] = o.distv[i];
		assert(iterv[pos].empty());
		boost::copy(o.iterv[i], std::inserter(iterv[pos], iterv[pos].begin()));
	}
}

// Just a wrapper for LoopInfo::merge to put some assertions.
void Dependency::merge(Dependency&& d)
{
	assert(source == d.source);
	assert(sink == d.sink);
	assert(exists() && d.exists());	// if any of the subscripts proved independence,
					// we should have returned right after that
	assert(loop_info);	// if there is no loop info, there are no indices to care about,
				//hence no need to merge anything
	loop_info->merge(std::move(*d.loop_info));
}

class SyncAttribute: public AstAttribute {
	SgStatement* stmt;
public:
	SyncAttribute(SgStatement* stmt): stmt(stmt) {}
	auto get_stmt() { return stmt; }
};

const std::string SYNC_ATTRIBUTE = "sync";

SgStatement* get_sync_source(SgStatement* s)
{
	auto attr = dynamic_cast<SyncAttribute*>(s->getAttribute(SYNC_ATTRIBUTE));
	assert(attr);
	return attr->get_stmt();
}

// TODO this doesn't dump the data in reasonable form
void Dependency::dump() const
{
	if (!exists())
		return;
	if (get_operation_type() == OperationType::input)
		return; // we don't care for input dependencies on shared memory
	const string line = "D|-------------------------------------------------------------|";
	auto& o = l;
	auto src = get_sync_source(get_source());
	auto snk = get_sync_source(get_sink());
	o << line << std::endl;
	o << "D| Dependency from " << src << " to " << snk << std::endl;
	if (is_loop_carried()) {
		o << std::endl << line << std::endl;
		auto& indices = loop_info->get_indices();
		auto& directions = loop_info->get_directions();
		auto& distances = loop_info->get_distances();
		auto& iterations = loop_info->get_iterations();
		auto depth = indices.size();
		assert(depth == iterations.size());
		assert(depth == distances.size());
		assert(depth == directions.size());
		o << "D| index\t| direction\t| distance\t| iterations\t|" << std::endl;
		for (unsigned i = 0; i != depth; i++) {
			o << "D| " << indices[i] << "\t\t| ";
			if (iterations[i].empty()) {
				o << directions[i];
				if (directions[i] == Dependency::LoopInfo::Direction::any)
					o << "\t\t| " << "\t\t| ";
				else
					o << "\t\t| " << distances[i] << "\t\t| ";
			} else {
				o << "\t\t| " << "\t\t| ";
				for (auto it : iterations[i]) {
					o << it << " ";
				}
			}
			o << "\t\t|" << std::endl;
		}
		o << std::endl << line << std::endl;
	} else {
		o << get_operation_type() << " #";
	}
}

SgValueExp* simplify(SgExpression* e)
{
	if (auto v = isSgValueExp(e))
		return v;
	auto foldConst = ConstantFolding::returnConstantFoldedValueExpression;
	auto folded = foldConst(remove_casting(e, false), false);
	l << "folded " << e->unparseToString() << " to ";
	l << (folded ? folded->unparseToString() : "NULL") << endl;
	return folded;
}

// Given two expressions, checks if the resulting value may change between them.
// The expressions have to be textually equal and values of variables they are
// accessing should not change in between.
// TODO implement.
bool is_const_between(SgExpression* a, SgExpression* b)
{
	if (!equal_as_strings(a, b))
		return false;
	return true;
}

/// Folds expression to find whether it is provably equal (or not equal) to zero
bool prove_zero(SgExpression* e, bool equal = true)
{
	// Array elements present - consider non-provable.
	if (!querySubTree<SgPntrArrRefExp>(e).empty())
		return false;
	auto vars = querySubTree<SgVarRefExp>(e);
	if (vars.empty()) {
		auto folded = simplify(e);
		if (!folded)
			return false;
		if (equal)
			return folded->unparseToString() == "0";
		else
			return folded->unparseToString() != "0";
	} else {
		// If variables are present, we try to replace them with
		// some numbers. Aruguably somewhat, if two substitutions bring
		// us zero, the expression equals zero.
		// TODO how many iterations do we actually need?
		using namespace boost::adaptors;
		using namespace SageBuilder;
		NS names;
		boost::copy(vars | transformed([] (auto v) {
				return initialized_name(v);
			}), std::inserter(names, names.end()));
		std::vector<EV> var_sets;
		boost::copy(names | transformed([&vars] (auto n) {
				EV s;
				boost::copy(vars | filtered([n] (auto v) {
						return n == initialized_name(v);
					}), std::back_inserter(s));
				return s;
			}), std::back_inserter(var_sets));
		for (int i = 0; i != 2; i++) {
			for (auto& s : var_sets) {
				auto value = std::rand();
				auto exp = buildIntVal(value);
				for (auto& v : s) {
					auto t = deepCopy(exp);
					replaceExpression(v, t);
					v = t;
				}
				deepDelete(exp);
			}
			auto folded = simplify(e);
			if (!folded || (folded->unparseToString() != "0"))
				return false;
		}
		return true;
	}
}

/// Represents a subscript, that is, a pair of expressions
/// used as indexes on same levels of two different expressions
/// performing access to elements of one array.
/// For example, for a pair of SgPntrArrRefExps in `a[i][j] = a[j-1][j+1]`
/// there will be two subscripts: (i, j-1) and (j, j+1).
class Subscript {
protected:
	SgExpression* first;
	SgExpression* second;
	const EnvironmentInfo& env;
	NS indices;
private:
	enum class Type {
		unknown,
		ziv,
		siv,
		miv,
	};

	Type type{Type::unknown};

	Type cmp_type();
protected:
	Dependency dependency;

public:
	Subscript(SgExpression* first, SgExpression* second,
					const EnvironmentInfo& env)
		: first(first), second(second), env(env),
		type(cmp_type()), dependency(first, second, indices)
	{
	}
	Subscript(Subscript&&);
	Subscript& operator=(Subscript&&);
	virtual ~Subscript() = default;

	// This API leaves the object in a potentially undefined state.
	// Do not reuse it after it's called.
	virtual Dependency analyse();

	SgExpression* get_first() const { return first; }
	SgExpression* get_second() const { return second; }
};

Subscript::Subscript(Subscript&& o)
	: first(o.first), second(o.second), env(o.env),
	indices(std::move(o.indices)), type(o.type),
	dependency(std::move(o.dependency))
{
}

Subscript& Subscript::operator=(Subscript&& o)
{
	std::swap(o, *this);
	return *this;
}


// Determine type of this subscript based on how many of the enclosing
// loops' indices it contains. Here we assume all variables that are
// dependent on indices have been substituted or something.
Subscript::Type Subscript::cmp_type()
{
	using namespace boost::adaptors;
	auto to_name = [] (auto e) {
		return get_var_initialized_name(e);
	};
	NS v0, v1, all_vars;
	boost::copy(querySubTree<SgVarRefExp>(first) | transformed(to_name),
			std::inserter(v0, v0.end()));
	boost::copy(querySubTree<SgVarRefExp>(second) | transformed(to_name),
			std::inserter(v1, v1.end()));
	boost::set_union(v0, v1, std::inserter(all_vars, all_vars.end()));
	NS sorted_indices;
	boost::copy(env.get_indices(), std::inserter(sorted_indices,
						sorted_indices.end()));
	boost::set_intersection(all_vars, sorted_indices,
				std::inserter(indices, indices.end()));
	switch (indices.size()) {
	case 0:
		type = Type::ziv;
		break;
	case 1:
		type = Type::siv;
		break;
	default:
		type = Type::miv;
	}
	return type;
}

// A subscript that references only one loop induction variable.
class SivSubscript: public Subscript {
	SgInitializedName* index;

	unique_expr_ptr get_bound_diff();
	bool bootstrap_linear_test(SgExpression* a, SgExpression* b,
					SgExpression* c, int& d, int coef = 0);
	void analyse_strong(SgExpression* a, SgExpression* fb, SgExpression* sb);
	void analyse_weak_zero(SgExpression* a, SgExpression* fb, SgExpression* sb);
	void analyse_weak_crossing(SgExpression* a, SgExpression* fb, SgExpression* sb);
public:
	SivSubscript(Subscript&& orig);
	~SivSubscript() = default;
	Dependency analyse() override;
};

Dependency Subscript::analyse()
{
	using namespace SageBuilder;
	switch (type) {
	case Type::ziv: {
		// TODO is_const_between(???, ???)
		auto diff = unique_expr_ptr(
				buildSubtractOp(deepCopy(first),
						deepCopy(second)),
				deepDelete);
		if (prove_zero(diff.get(), false)) {
			// Values not equal => no dependency
			dependency.make_independent();
		} else {
			// This subscript doesn't give us any dependency info
			// TODO should pass something to the caller to communicate this?
		}
		return std::move(dependency);
	}
	case Type::siv: {
		SivSubscript self(std::move(*this));
		return self.analyse();
	}
	case Type::miv:
		assert(!"Not implemented");
		return std::move(dependency);
	default:
		assert(0);
	}
}

// -(+(-a)) -> a, negate = false. In theory.
SgExpression* skip_unary_arithmetic(SgExpression* e, bool& negate)
{
	negate = false;
	while (auto ul = isSgUnaryOp(e)) {
		e = ul->get_operand();
		if (isSgMinusMinusOp(e))
			negate = !negate;
		else if (!isSgUnaryAddOp(e) && !isSgCastExp(e))
			return e;
	}
	return e;
}

// Returns true and fills parameters accordingly if e is of form a*index + b
// and i is one of the indices.
bool is_linear_over_index(SgExpression* e, SgInitializedName* index,
				SgExpression*& a, SgExpression*& b)
{
	using namespace SageBuilder;
	auto cleanup = [&a, &b] {
		a = nullptr;
		b = nullptr;
	};
	auto simplify = [&a, &b] {
		bool negate = false;
		a = skip_unary_arithmetic(a, negate);
		if (negate)
			a = buildMinusOp(a);
		negate = false;
		b = skip_unary_arithmetic(b, negate);
		if (negate)
			b = buildMinusOp(b);
	};
	cleanup();
	if (!isSgAddOp(e) && !isSgSubtractOp(e)) {
		if (expr_contains_any_of_vars(e, index)) {
			// b == 0
			SgExpression* lhs = buildIntVal(1);
			SgExpression* rhs = deepCopy(e);
			if (!express_var_in_rhs(lhs, rhs, index))
				return false;
			auto div = isSgDivideOp(lhs);
			if (!div) {
				a = buildIntVal(1);
				b = buildIntVal(0);
				return true;
			}
			a = div->get_rhs_operand_i();
			b = buildIntVal(0);
			simplify();
			return true;
		}
		// No index present => a == 0.
		a = SageBuilder::buildIntVal(0);
		b = e;
		return true;
	}
	auto bin = isSgBinaryOp(e);
	assert(bin);
	auto lhs = bin->get_lhs_operand_i();
	auto rhs = bin->get_rhs_operand_i();
	assert(lhs && rhs);
	bool i_in_lhs = expr_contains_any_of_vars(lhs, index);
	bool i_in_rhs = expr_contains_any_of_vars(rhs, index);
	if (i_in_lhs == i_in_rhs) // index should be in one and only in one
		return false;     // half of the expression

	auto t_lhs = deepCopy(rhs);
	auto t_rhs = deepCopy(lhs);
	if (i_in_rhs)
		swap(t_lhs, t_rhs);
	if (!express_var_in_rhs(t_lhs, t_rhs, index))
		return false;
	auto div = isSgDivideOp(t_lhs);
	if (!div) {
		a = buildIntVal(1);
		b = t_lhs;
		return true;
	} else {
		a = div->get_rhs_operand_i();
		b = div->get_lhs_operand_i();
	}
	if (isSgSubtractOp(e))
		b = buildMinusOp(b);
	simplify();
	return true;
}

// Evaluates e as a constant expression,
// throws if it is not const.
int eval(SgExpression* e)
{
	auto vars = querySubTree<SgVarRefExp>(e);
	if (!vars.empty())
		throw 0;
	if (auto value = isSgValueExp(e))
		return getSignedIntegerConstantValue(value);
	auto folded = simplify(e);
	if (!folded)
		throw 0;
	return getSignedIntegerConstantValue(folded);
}

/// Checks wheher argument evaluates to non-integer.
bool is_non_integer(SgExpression* e)
{
	auto div = isSgDivideOp(e);
	if (!div)
		// Only divisions are considered currently
		// as that's the call scenario.
		return false;
	auto num = div->get_lhs_operand_i();
	auto denum = div->get_rhs_operand_i();
	int nv = 0;
	int dv = 0;
	try {
		nv = eval(num);
		dv = eval(denum);
	} catch (...) {
		return false;
	}
	if (dv == 0)
		return false;
	auto r = nv/dv;
	if (r*dv != nv)
		return true;
	return false;
}

/// Checks whether the difference of a and b evaluates to zero.
bool are_equal(SgExpression* a, SgExpression* b)
{
	auto& bso = SageBuilder::buildSubtractOp;
	unique_expr_ptr d(bso(deepCopy(a), deepCopy(b)), deepDelete);
	try {
		int diff = eval(d.get());
		if (!diff)
			return true;
	} catch (...) {
	}
	return false;
}

unique_expr_ptr SivSubscript::get_bound_diff()
{
	auto lp = env.get_loop_info(index);
	assert(lp);
	SgExpression* ub = lp->ub;
	SgExpression* lb = lp->lb;
	if (!lb || !ub) {
		l << lp->loop << " ";
		l << "Index of a loop with unknown bounds occured in dependency analysis.";
		l << " Supposing dependency exists." << endl;
		return unique_expr_ptr(nullptr, deepDelete);
	}
	using namespace SageBuilder;
	return unique_expr_ptr(buildSubtractOp(deepCopy(ub),
						deepCopy(lb)),
				deepDelete);
}

// Returns whether *dependency* is ready for this test.
// Constructs an expression (b-c)/a, then
// * Tries to evaluate it as int (d =)
// * If it can't be evaluated, returns true
// * If it can be and the result is non-integer,
//   makes the *dependency* non-existent and returns true
// * Also gets diff = the difference between bounds and returns
//   true if it is not a plain int
// * Finally, if |d| > |diff|, marks *dependency* as non-existent
//   and returns true
// * Otherwise returns false and saves d.
bool SivSubscript::bootstrap_linear_test(SgExpression* a, SgExpression* b,
					SgExpression* c, int& d, int coef)
{
	using namespace SageBuilder;
	assert(a && b && c);
	auto raw_d = unique_expr_ptr(
			buildDivideOp(buildSubtractOp(deepCopy(b),
							deepCopy(c)),
					deepCopy(a)), deepDelete);
	if (is_non_integer(raw_d.get())) {
		// If (fb-sb)/a is not int, expressions can't reference
		// the same element on any pair of iterations.
		dependency.make_independent();
		return true;
	}
	auto raw_distance = get_bound_diff();
	if (!raw_distance)
		return true;
	int distance = 0;
	try {
		d = eval(raw_d.get());
		distance = eval(raw_distance.get());
	} catch (...) {
		return true;
	}
	if (coef)
		d *= coef;
	if (abs(d) > abs(distance)) {
		// If abs((fb-sb)/a) > abs(U-L), we do not have
		// enough iterations for the dependence to exist.
		dependency.make_independent();
		return true;
	}
	return false;
}

// a*i + fb <=> a*i + sb
void SivSubscript::analyse_strong(SgExpression* a, SgExpression* fb, SgExpression* sb)
{
	using namespace SageBuilder;
	int d = 0;
	if (bootstrap_linear_test(a, fb, sb, d))
		return;
	if (d != 0) {
		dependency.loop().set_distance(0, d);
	} else {
		auto fs = getEnclosingNode<SgStatement>(first);
		auto ss = getEnclosingNode<SgStatement>(second);
		if (fs == ss) {
			// If d == 0 and the references are in the same
			// statement, no dependency exists.
			dependency.make_independent();
		}
	}
}

// a*i + fb <=> sb
void SivSubscript::analyse_weak_zero(SgExpression* a, SgExpression* fb, SgExpression* sb)
{
	int d = 0;
	if (bootstrap_linear_test(a, sb, fb, d))
		return;
	dependency.loop().add_iteration(0, d);
}

// a*i + fb <=> -a*i + sb
void SivSubscript::analyse_weak_crossing(SgExpression* a, SgExpression* fb, SgExpression* sb)
{
	int d = 0;
	if (bootstrap_linear_test(a, sb, fb, d, 0.5))
		return;
	dependency.loop().add_iteration(0, d);
}

Dependency SivSubscript::analyse()
{
	using namespace SageBuilder;
	SgExpression* fa = nullptr;
	SgExpression* fb = nullptr;
	SgExpression* sa = nullptr;
	SgExpression* sb = nullptr;
	bool is_linear = is_linear_over_index(first, index, fa, fb)
				&& is_linear_over_index(second, index, sa, sb);
	bool strong_type = is_linear && is_const_between(fa, sa);
	bool weak_zero_type = is_linear && (prove_zero(fa) || prove_zero(sa));
	bool weak_crossing_type = is_linear && are_equal(fa, buildMinusOp(sa));
	if (strong_type) {
		analyse_strong(fa, fb, sb);
	} else if (weak_zero_type) {
		bool fa0 = prove_zero(fa);
		bool sa0 = prove_zero(sa);
		// This means ZIV - TODO?
		assert(!fa0 || !sa0);
		auto a = fa0 ? sa : fa;
		auto fbn = fa0 ? sb : fb;
		auto sbn = fa0 ? fb : sb;
		analyse_weak_zero(a, fbn, sbn);
	} else if (weak_crossing_type) {
		analyse_weak_crossing(fa, fb, sb);
	}
	return std::move(dependency);
}

SivSubscript::SivSubscript(Subscript&& orig)
	: Subscript(std::move(orig))
{
	assert(indices.size() == 1);
	index = *(indices.begin());
}

/// A set of subscripts. Generally, two subscripts belong to the same
/// set if they share references to a variable.
class SubscriptSet {
	std::vector<Subscript> s;
	NS names; // TODO may array elements be indices?
public:
	SubscriptSet(Subscript&& x)
	{
		boost::set_union(get_referenced_vars(x.get_first()),
				get_referenced_vars(x.get_second()),
				std::inserter(names, names.end()));
		s.push_back(std::move(x));
	}
	bool intersection_empty(const SubscriptSet& other) const
	{
		NS intersection;
		boost::set_intersection(names, other.names,
			std::inserter(intersection, intersection.end()));
		return intersection.empty();
	}
	SubscriptSet& operator&=(SubscriptSet&& other)
	{
		names.insert(other.names.begin(), other.names.end());
		std::move(other.s.begin(), other.s.end(), std::back_inserter(s));
		return *this;
	}
	std::size_t size() const { return s.size(); }
	Subscript& operator[](std::size_t i) { return s[i]; }
	const auto& get_subscripts() const { return s; }
	const auto& get_names() const { return names; }

	SubscriptSet(const SubscriptSet&) = delete;
};

ostream& operator<<(ostream& o, const Subscript& s)
{
	o << "[" << s.get_first()->unparseToString() << ":";
	o << s.get_second()->unparseToString() << "]";
	return o;
}

ostream& operator<<(ostream& o, const SubscriptSet& ss)
{
	auto size = ss.get_subscripts().size();
	o << "{";
	for (unsigned i = 0; i != size; i++) {
		o << ss.get_subscripts()[i];
		if (i != size - 1)
			o << ", ";
	}
	return o << "}";
}

ostream& operator<<(ostream& o, Dependency::OperationType s)
{
	using Type = Dependency::OperationType;
	switch (s) {
	case Type::flow:
		return o << "FLOW";
	case Type::anti:
		return o << "ANTI";
	case Type::output:
		return o << "OUTPUT";
	case Type::input:
		return o << "INPUT";
	case Type::unknown:
		return o << "UNKNOWN / SELF";
	default:
		assert(0);
	}
}

ostream& operator<<(ostream& o, Dependency::LoopInfo::Direction s)
{
	using Type = Dependency::LoopInfo::Direction;
	switch (s) {
	case Type::straight:
		return o << "<";
	case Type::none:
		return o << "=";
	case Type::backward:
		return o << ">";
	case Type::any:
		return o << "*";
	case Type::unspecified:
		return o << "?";
	default:
		assert(0);
	}
}

/// Given two expressions, possibly accessing the same memory location,
/// in this case two variable references or two references to the same array,
/// and, possibly, information about the surrounding loops, detects
/// dependencies that can happen.
/// TODO consider alias somehow?
class DependencyDetector {
	DataAccess first;
	DataAccess second;
	const EnvironmentInfo& env;

	std::list<SubscriptSet> subscripts;

	Dependency result;

	bool extract_subscripts();
	void build_sets();
	void analyse();
public:
	DependencyDetector(DataAccess first, DataAccess second,
				const EnvironmentInfo& info)
		: first(first), second(second), env(info), result(first, second)
	{
		NS all_indices;
		boost::copy(env.get_indices(), std::inserter(all_indices,
							all_indices.end()));
		result = Dependency(first, second, all_indices);
		assert(get_var_initialized_name(first.e)
				== get_var_initialized_name(second.e));
	}
	void run();
	Dependency get_result() { return std::move(result); }
};

/// Fill *subscripts* with subscript sets; each subscript into its own set.
/// Return whether we should continue analysis.
bool DependencyDetector::extract_subscripts()
{
	using namespace boost::adaptors;
	auto v0 = isSgPntrArrRefExp(first.e);
	auto v1 = isSgPntrArrRefExp(second.e);
	if (!v0 || !v1) {
		// The same regular variable is modified.
		// TODO do we need to merge anything anywhere?
		// Dependency d(first, second, env);
		// result.merge(std::move(d));
		return false;
	}
	auto s0 = get_subscripts(v0);
	auto s1 = get_subscripts(v1);
	if (s0.size() != s1.size()) {
		// TODO review this branch
		auto v = (s0.size() < s1.size() ? v0 : v1);
		l << LOG_WARN << v;
		l << " Access to a sub-array in dependency analysis." << endl;
		// dependencies.emplace_back(first, second, env);
		return false;
	}
	for (unsigned i = 0; i != s0.size(); i++) {
		subscripts.emplace_back(Subscript(s0[i], s1[i], env));
	}
	return true;
}

/// Merge all sets in *subscripts* that have common variables.
void DependencyDetector::build_sets()
{
	for (auto it = subscripts.begin(); it != subscripts.end(); it++) {
		auto merge_it = std::next(it);
		auto common = [it] (const SubscriptSet& s) {
			return !(it->intersection_empty(s));
		};
		while (true) {
			merge_it = std::find_if(merge_it, subscripts.end(), common);
			if (merge_it == subscripts.end())
				break;
			*it &= std::move(*merge_it);
			merge_it = subscripts.erase(merge_it);
		}
	}
	l << "Subscript sets: ";
	for (auto& s : subscripts)
		l << s << ", ";
	l << endl;
}

/// Launch per-set analyses and query the results.
void DependencyDetector::analyse()
{
	std::vector<Dependency> dependencies;
	for (auto& s : subscripts) {
		if (s.size() == 1) {
			auto& x = s[0];
			Dependency d = x.analyse();
			if (d.exists()) {
				dependencies.push_back(std::move(d));
			} else {
				// Proved independence
				result = std::move(d);
				return;
			}
		} else {
			// TODO: multi-subscript analyses
		}
	}
	for (auto&& d : dependencies) {
		result.merge(std::move(d));
	}
}

void DependencyDetector::run()
{
	if (!extract_subscripts())
		return;
	build_sets();
	analyse();
}

} // namespace

class DependencyAnalysis::Impl {
	SgForStatement* orig_loop;
	unique_for_stmt_ptr target_loop;
	depv result;
	bool ready{false};
	SV statements;
	std::unique_ptr<EnvironmentInfo> env{nullptr};
	std::vector<DependencyDetector> candidates;

	void get_candidates();
public:
	Impl(SgForStatement* loop);
	void run();
	void annotate_source();
	void annotate_ast();
};

// Returns a copy of *orig* where all statements have an attribute
// pointing at the corresponding statement in *orig*.
unique_stmt_ptr synchronous_tree(SgStatement* orig)
{
	unique_stmt_ptr sync(deepCopy(orig), deepDelete);
	auto orig_stmts = querySubTree<SgStatement>(orig);
	auto sync_stmts = querySubTree<SgStatement>(sync.get());
	assert(orig_stmts.size() == sync_stmts.size());
	for (unsigned i = 0; i != orig_stmts.size(); i++) {
		sync_stmts[i]->setAttribute(SYNC_ATTRIBUTE,
					new SyncAttribute(orig_stmts[i]));
	}
	return std::move(sync);
}

DependencyAnalysis::Impl::Impl(SgForStatement* loop)
	: orig_loop(loop),
	target_loop(isSgForStatement(synchronous_tree(orig_loop).release()),
			deepDelete)
{
}

/// Get all pairs of expressions that can form a dependency,
/// that is those that enclose references to same variables.
void DependencyAnalysis::Impl::get_candidates()
{
	using namespace boost::adaptors;
	using boost::copy;
	// Get all var and array refs in order they happen in statements
	std::vector<DataAccess> top_refs;
	for (auto s : statements)
		copy(querySubTree<SgExpression>(s)
			| filtered([s] (auto e) {
				if (!isSgPntrArrRefExp(e) && !isSgVarRefExp(e))
					return false;
				auto ea = getEnclosingNode<SgPntrArrRefExp>(e, s);
				return ea == nullptr;
			})
			| transformed([] (SgExpression* r) {
				return DataAccess(r);
			}), std::back_inserter(top_refs));

	for (auto it = top_refs.begin(); it != top_refs.end(); it++) {
		if (it->t == AccessType::rw) {
			// For expressions like a[i] += 1 we consider a[i] to be
			// both the source and the sink of a dependency.
			candidates.emplace_back(*it, *it, *env);
			continue;
		}
		auto pred = [it] (auto v) {
			return same_var_referenced(v.e, it->e);
		};
		auto cur = std::find_if(it + 1, top_refs.end(), pred);
		while (cur != top_refs.end()) {
			if (cur->t != AccessType::r || it->t != AccessType::r) {
				// We do not care about input dependencies
				candidates.emplace_back(*it, *cur, *env);
			}
			cur = std::find_if(++cur, top_refs.end(), pred);
		}
	}
	l << "found " << candidates.size() << " candidates" << endl;
}

auto get_all_vars(SgStatement* stmt)
{
	using namespace boost::adaptors;
	std::set<std::string> result;
	boost::copy(querySubTree<SgVarRefExp>(stmt)
			| transformed(get_var_name),
			std::inserter(result, result.end()));
	return result;
}

// Create a copy of target statement, propagate all variables there and
// put it instead of the original one. Get all possible sources of dependencies,
// detect them. Put source statement back after that.
// TODO can we delete it??
void DependencyAnalysis::Impl::run()
{
	replaceStatement(orig_loop, target_loop.get());
	BlockTraveller::BTParams p;
	p.propagate = get_all_vars(target_loop.get());
	BlockTraveller t(target_loop.get(), std::move(p), false);
	t.traverse_block();
	env = std::make_unique<EnvironmentInfo>(target_loop.get());
	statements = split_block_into_statements(
				get_nested_loops_main_block(target_loop.get()));
	get_candidates();
	for (auto& detector : candidates) {
		detector.run();
		// for now just dump results to the console
		auto dep = detector.get_result();
		if (dep.exists()) {
			dep.dump();
			l << endl;
		}
	}
	replaceStatement(target_loop.get(), orig_loop);
}

void DependencyAnalysis::Impl::annotate_source()
{
	assert(!"Not implemented.");
}

void DependencyAnalysis::Impl::annotate_ast()
{
	assert(!"Not implemented.");
}

DependencyAnalysis::DependencyAnalysis(SgForStatement* loop)
{
	impl = std::make_unique<DependencyAnalysis::Impl>(loop);
}

DependencyAnalysis::~DependencyAnalysis() = default;

void DependencyAnalysis::run()
{
	impl->run();
}

void DependencyAnalysis::annotate_source()
{
	impl->annotate_source();
}

void DependencyAnalysis::annotate_ast()
{
	impl->annotate_ast();
}

} // namespace CodeAnalyses

