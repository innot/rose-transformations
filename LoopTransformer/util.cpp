#define BOOST_RESULT_OF_USE_DECLTYPE
#include "util.h"
#include "patternRewrite.h"
#include <unistd.h>
#include <regex>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace SPRAY;

namespace TfUtils {

Logger l;
const string BORDER_ATTRIBUTE = "border";
const string ID_ATTRIBUTE = "id";
const string RD_ATTRIBUTE = "rd-analysis-in";

namespace {
const string col_bg = "\033[";
const string col_off = "\033[0m";
const bool is_term = isatty(2); // Whether STDERR is a terminal

const string level_messages[] = {
	"[DEBUG]",
	"[INFO]",
	"[WARNING]",
	"[ERROR]",
};

const string level_colors[] = {
	"34m",
	"32m",
	"33m",
	"31m",
};

std::string lvl_to_msg(std::size_t lvl)
{
	if (is_term)
		return col_bg + level_colors[lvl] +
			level_messages[lvl] + col_off + ": ";
	else
		return level_messages[lvl] + ": ";
}
} // namespace

Logger::Logger(log_level min_level, ostream *o, log_level default_level)
{
	this->o = o;
	this->min_level = min_level;
	this->default_level = default_level;
	this->current_level = LOG_DEF;
	this->errors = 0;
}

Logger& Logger::operator<<(log_level lvl)
{
	current_level = (lvl == LOG_DEF ? default_level : lvl);
	if (current_level >= min_level)
		(*o) << lvl_to_msg(current_level);
	if (lvl == LOG_ERR)
		errors++;
	return *this;
}

Logger& Logger::operator<<(ostream& (*pf)(ostream&))
{
	if (current_level >= min_level)
		o = &(pf(*o));
	current_level = LOG_DEF;
	return *this;
}

ostream &operator<<(ostream &o, const SgInitializedName *x)
{
	return o << x->get_name().str();
}

ostream &operator<<(ostream &o, const CNS *in)
{
	o << " { ";
	for (auto i = in->begin(); i != in->end(); i++) {
		o << *i << " ";
	}
	return o << "}";
}

ostream &operator<<(ostream &o, const NS *in)
{
	o << " { ";
	for (auto i = in->begin(); i != in->end(); i++) {
		o << *i << " ";
	}
	return o << "}";
}

ostream &operator<<(ostream &o, const CNV *in)
{
	o << " { ";
	for (auto i = in->begin(); i != in->end(); i++) {
		o << *i << " ";
	}
	return o << "}";
}

ostream &operator<<(ostream &o, const NV *in)
{
	o << " { ";
	for (auto i = in->begin(); i != in->end(); i++) {
		o << *i << " ";
	}
	return o << "}";
}

ostream &operator<<(ostream &o, const map<const SgPntrArrRefExp*, const SgExpression*> *in)
{
	if (!in->size())
		return o << " { \u2205 } ";
	o << " {\n";
	for (auto i : *in) {
		o << "\t" << i.first->unparseToString();
		o << " : " << i.second->unparseToString() << "\n";
	}
	return o << "}";
}

ostream &operator<<(ostream &o, const map<const SgInitializedName*, const SgExpression*> *in)
{
	if (!in->size())
		return o << " { \u2205 } ";
	o << " {\n";
	for (auto i : *in) {
		o << "\t" << i.first->unparseToString();
		o << " : " << i.second->unparseToString() << "\n";
	}
	return o << "}";
}

std::ostream &operator<<(std::ostream &o, const std::set<std::string>& in)
{
	if (!in.size())
		return o << " { \u2205 } ";
	o << " { ";
	for (auto i : in)
		o << i << " ";
	return o << "}";
}

std::ostream &operator<<(std::ostream &o,
			const std::map<string, std::size_t>& in)
{
	if (!in.size())
		return o << "{ \u2205 }";
	o << "{ ";
	std::size_t n = 0;
	for (const auto i : in) {
		o << i.first << ": " << i.second;
		if (++n != in.size())
			o << ", ";
	}
	return o << " }";
}

ostream &operator<<(ostream &o, const depv *in)
{
	if (!in->size())
		return o << " { \u2205 } ";
	o << " {\n";
	for (auto i : *in)
		o << "\t" << get<0>(i) << " : " << &get<1>(i) << "\n";
	return o << "}";
}

ostream &operator<<(ostream &o, const SgNode *x)
{
	auto file_pos = x->get_startOfConstruct();
	if (!file_pos)
		return o << "<somewhere>";
	string full_name = file_pos->get_filenameString();
	full_name.replace(0, 1 + full_name.rfind("/"), "");
	return o << "<" << full_name << " @ "
		<< file_pos->get_line() << ":" << file_pos->get_col() << ">";
}

std::ostream& operator<<(std::ostream &o, const Exception& e)
{
	auto node0 = e.get_node0();
	auto node1 = e.get_node1();
	if (node0)
		o << node0 << " ";
	if (node1)
		o << node1 << " ";
	return o << e.get_desc() << endl;
}

TransformParams::TransformParams() = default;

/// Checks whether *stmt* is a for-, while- or do-loop.
///
bool is_a_loop(const SgStatement *stmt)
{
	return isSgForStatement(stmt) ||
		isSgWhileStmt(stmt) || isSgDoWhileStmt(stmt);
}

/// Checks whether *node* is a something convertRefToInitializedName()
/// can convert to a name.
bool is_a_var_reference(const SgNode *node)
{
	return isSgVarRefExp(node) || isSgPntrArrRefExp(node)
			|| isSgInitializedName(node) || isSgDotExp(node)
			|| isSgArrowExp(node) || isSgPointerDerefExp(node);
}

/// Returns a block that is being executed in perfectly nested loop nest
/// pointed at by *loops*.
///
/// The block is created if it didn't exist.
/// If *depth* != -1, stops when reaches the body
/// of a loop at *depth* level.
///
SgBasicBlock* get_nested_loops_main_block(SgStatement *loops, int depth)
{
	SgBasicBlock *body;
	if (auto for_loop = isSgForStatement(loops))
		body = ensureBasicBlockAsBodyOfFor(for_loop);
	else if (auto while_loop = isSgWhileStmt(loops))
		body = ensureBasicBlockAsBodyOfWhile(while_loop);
	else if (auto do_loop = isSgDoWhileStmt(loops))
		body = ensureBasicBlockAsBodyOfDoWhile(do_loop);
	else
		assert(0);

	if (depth != -1) {
		if (depth == 1)
			return body;
		else
			depth--;
	}
	auto body_statements = body->get_statements();
	if ((body_statements.size() == 1) && is_a_loop(body_statements[0]))
		return get_nested_loops_main_block(body_statements[0], depth);
	else
		return body;
}

SgInitializedName* (&initialized_name)(const SgNode *e) = get_var_initialized_name;

bool equal_as_strings(SgNode* a, SgNode* b)
{
	return a->unparseToString() == b->unparseToString();
}

/// Convert "__some__complicated__name__" to
/// "_some_complicated_name".
std::string strip(std::string&& s)
{
	auto beg_end = std::regex("__(.*)__");
	return std::regex_replace(std::regex_replace(s, beg_end, "_$1"),
					std::regex("__"), "_");
}

/// Checks if two for loops have the same iteration space.
///
/// If they are canonical, compares counters,
/// bounds and step by string equivalent,
/// otherwise compares raw init, test and increment,
/// again, by string equivalent equality.
bool loops_have_equal_heads(SgForStatement *l0, SgForStatement *l1)
{
	using namespace std::placeholders;
	SgInitializedName *ivar0, *ivar1;
	SgExpression *lb0, *lb1;
	SgExpression *ub0, *ub1;
	SgExpression *step0, *step1;
	bool can0 = isCanonicalForLoop(l0, &ivar0, &lb0, &ub0, &step0);
	bool can1 = isCanonicalForLoop(l1, &ivar1, &lb1, &ub1, &step1);
	if (!can0 || !can1) {
		l << LOG_WARN << "one of the loops at ";
		l << l0 << l1 << "is not canonical;";
		l << "comparing their heads literally" << endl;
		return equal_as_strings(l0->get_for_init_stmt(),
						l1->get_for_init_stmt()) &&
			equal_as_strings(l0->get_test(), l1->get_test()) &&
			equal_as_strings(l0->get_increment(),
					l1->get_increment());
	}
	return equal_as_strings(ivar1, ivar0) && equal_as_strings(lb0, lb1)
		&& equal_as_strings(ub0, ub1) && equal_as_strings(step0, step1);
}

bool is_modified(const SgNode *node, bool *is_read)
{
	auto parent = node->get_parent();
	bool is_unary_mod = isSgPlusPlusOp(parent) || isSgMinusMinusOp(parent)
					|| isSgAddressOfOp(parent);
	if (is_unary_mod) {
		if (is_read)
			*is_read = true;
		return true;
	}
	if (auto compound_assign = isSgCompoundAssignOp(parent)) {
		if (is_read)
			*is_read = true;
		return compound_assign->get_lhs_operand() == node;
	}
	if (auto assign = isSgAssignOp(parent)) {
		if (is_read)
			*is_read = false;
		return assign->get_lhs_operand() == node;
	}
	return false;
}

/// Checks wheter *node*'s value can be written to screen, disk, external memory
/// or whatever. Currently returns true if there is a function call above
/// the node.
bool is_output(const SgNode* node)
{
	auto call = SageInterface::getEnclosingNode<SgFunctionCallExp>(node);
	if (!call)
		return false;
	auto parent = node->get_parent();
	return !isSgPntrArrRefExp(parent);
}

/// Returns the number of pairwise equal loops from *nest0* and *nest1*.
///
int get_merge_head_depth(const vector<SgForStatement*> &nest0,
			const vector<SgForStatement*> &nest1)
{
	int result = 0;
	for (unsigned i = 0; i < nest0.size(); i++) {
		if (i >= nest1.size())
			return result;
		if (!loops_have_equal_heads(nest0[i], nest1[i]))
			return result;
		result++;
	}
	return result;
}

std::vector<SgForStatement*> query_loop_nests(SgStatement* root)
{
	auto result = querySubTree<SgForStatement>(root);
	boost::remove_erase_if(result, [root] (auto l) {
		if (l == root)
			return false;
		auto p = l->get_parent();
		if (isSgForStatement(p))
			return true;
		auto bb = isSgBasicBlock(p);
		if (!bb)
			return false;
		if (isSgForStatement(bb->get_parent()) &&
				(bb->get_statements().size() == 1))
			return true;
		return false;
	});
	return result;
}

/// Fetches all perfectly nested for loops starting at *loops*.
///
vector<SgForStatement*> get_all_nested_loops(SgForStatement *loops)
{
	vector<SgForStatement*> result;
	result.push_back(loops);
	auto body = loops->get_loop_body();
	if (auto loop = isSgForStatement(body)) {
		boost::copy(get_all_nested_loops(loop),
				std::back_inserter(result));
	} else if (auto block = isSgBasicBlock(body)) {
		auto body_statements = block->get_statements();
		if (body_statements.size() != 1)
			return result;
		auto loop_stmt = isSgForStatement(body_statements[0]);
		if (loop_stmt) {
			boost::copy(get_all_nested_loops(loop_stmt),
					std::back_inserter(result));
		}
	}
	return result;
}

vector<SgForStatement*> get_all_surrounding_loops(SgStatement* stmt)
{
	auto enc_f = getEnclosingFunctionDeclaration(stmt);
	vector<SgForStatement*> result;
	while (auto enc_loop = getEnclosingNode<SgForStatement>(stmt, enc_f)) {
		result.push_back(enc_loop);
		stmt = enc_loop;
	}
	return result;
}

/// Checks if *expr* contains references to *var*
///
bool expr_contains_any_of_vars(const SgExpression *expr,
				const SgInitializedName* var)
{
	CNS vars;
	vars.insert(var);
	return expr_contains_any_of_vars(expr, vars);
}

/// Checks if *expr* contains references to any of the variables from *vars*.
///
bool expr_contains_any_of_vars(const SgExpression *expr, const CNS &vars)
{
	CNS referenced_vars;
	get_referenced_vars(expr, referenced_vars);
	auto expr_to_substitute = find_if(referenced_vars.begin(),
					referenced_vars.end(), RefFinder(&vars));
	return expr_to_substitute != referenced_vars.end();
}


/// For SgVarRefExps and SgPntrArrRefExps returns SgInitializedName
/// of the variable *e* contains.
/// \todo why const SgNode?
SgInitializedName* get_var_initialized_name(const SgNode *e)
{
	auto var_ref = isSgVarRefExp(e);
	auto arr_ref = isSgPntrArrRefExp(e);
	if (!var_ref && !arr_ref)
		return nullptr;
	if (var_ref) {
		return var_ref->get_symbol()->get_declaration();
	} else {
		const SgExpression *name;
		isArrayReference(arr_ref, &name);
		return get_var_initialized_name(name);
	}
}

/// A dirty wrapper for get_var_initialized_name().
///
/// \todo replace it with standard function?
string get_var_name_from_node(const SgExpression *e)
{
	auto t = get_var_initialized_name(e);
	if (t)
		return t->get_name().getString();
	else
		assert(0);
}

SgExpression *get_unary_modifier_expression(SgExpression *um)
{
	auto pp = isSgPlusPlusOp(um);
	auto mm = isSgMinusMinusOp(um);
	if (pp)
		return pp->get_operand_i();
	else if (mm)
		return mm->get_operand_i();
	else
		assert(0);
}

void output_statement_alteration(const string &orig, const SgStatement *changed)
{
	string new_s = changed->unparseToString();
	if (orig == new_s)
		l << "statement " << new_s << " left unchanged" << endl;
	else
		l << "changed " << orig << " to " << new_s << endl;
}

bool is_child_of(const SgStatement *definition, const SgStatement *block)
{
	auto all_statements = querySubTree<SgStatement>(block);
	auto pos = find(all_statements.begin(), all_statements.end(), definition);
	return pos != all_statements.end();
}

bool node_references_var(const SgNode *node, const string &var)
{
	auto all_vars = querySubTree<SgVarRefExp>(node);
	return find_if(all_vars.begin(), all_vars.end(),
			[&var] (const SgVarRefExp *v) {
				return get_var_initialized_name(v)->get_name() == var;
			}) != all_vars.end();
}

/// Const qualified reimplementation of library function.
///
/// Variable references can be introduced by SgVarRefExp, SgPntrArrRefExp,
/// SgInitializedName, SgMemberFunctionRefExp etc.
/// This function will convert them all to a top level SgInitializedName.
const SgInitializedName* convertRefToInitializedName(const SgNode* current)
{
	const SgInitializedName* name = nullptr;
	const SgExpression* nameExp = nullptr;
	ROSE_ASSERT(current != nullptr);
	if (isSgInitializedName(current)) {
		name = isSgInitializedName(current);
	} else if (isSgPntrArrRefExp(current)) {
		bool suc=false;
		suc = isArrayReference(isSgExpression(current),&nameExp);
		ROSE_ASSERT(suc == true);
		 // has to resolve this recursively
		return convertRefToInitializedName(nameExp);
	} else if (isSgVarRefExp(current)) {
		SgNode* parent = current->get_parent();
		if (isSgDotExp(parent))
		{
			 if (isSgDotExp(parent)->get_rhs_operand() == current)
				return convertRefToInitializedName(parent);
		}
		else if(isSgArrowExp(parent))
		{
			if (isSgArrowExp(parent)->get_rhs_operand() == current)
				return convertRefToInitializedName(parent);
		}
		name = isSgVarRefExp(current)->get_symbol()->get_declaration();
	} else if (isSgDotExp(current)) {
		SgExpression* lhs = isSgDotExp(current)->get_lhs_operand();
		ROSE_ASSERT(lhs);
		 // has to resolve this recursively
		return convertRefToInitializedName(lhs);
	} else if (isSgArrowExp(current)) {
		SgExpression* lhs = isSgArrowExp(current)->get_lhs_operand();
		ROSE_ASSERT(lhs);
		 // has to resolve this recursively
		return convertRefToInitializedName(lhs);
	} // The following expression types are usually introduced by left hand operands of DotExp, ArrowExp
	else if (isSgPointerDerefExp(current)) {
		return convertRefToInitializedName(isSgPointerDerefExp(current)->get_operand());
	} else if (isSgCastExp(current)) {
		return convertRefToInitializedName(isSgCastExp(current)->get_operand());
	} else {
		l << "In convertRefToInitializedName(const SgNode*): unhandled reference type:";
		l << current->class_name() << endl;
	}
	return name;
}

string get_var_name(const SgNode *e)
{
	auto init_name = convertRefToInitializedName(e);
	assert(init_name);
	return init_name->get_name();
}

bool RefFinder::operator() (const SgInitializedName *p) const
{
	if (vars) {
		auto result = vars->find(p) != vars->end();
		return negative ? !result : result;
	} else if (names) {
		auto result = names->find(p->get_name()) != names->end();
		return negative ? !result : result;
	} else {
		assert(0); // Bad construction of RefFinder
	}
}

bool RefFinder::operator() (const pair<const SgInitializedName* const,
					const SgExpression*> &p) const
{
	if (vars) {
		auto result = vars->find(p.first) != vars->end();
		return negative ? !result : result;
	} else if (names) {
		auto result = names->find((p.first)->get_name())
							!= names->end();
		return negative ? !result : result;
	} else {
		assert(0); // Bad construction of RefFinder
	}
}

bool AttributeFinder::operator() (const SgStatement *s)
{
	auto attr = s->getAttribute(ID_ATTRIBUTE);
	IdAttribute *id_attr = dynamic_cast<IdAttribute*>(attr);
	if (!id_attr)
		return false;
	return id_attr->get_id() == target_id;
}

/*
 * // \todo you know, remove the second one.
 * bool isArrayReference(SgExpression* ref, SgExpression** arrayName, EV* subscripts)
 * {
 *         using namespace boost::adaptors;
 *         CEV s;
 *         auto ss = &s;
 *         auto result = isArrayReference(const_cast<const SgExpression*>(ref),
 *                                         const_cast<const SgExpression**>(arrayName),
 *                                         &ss);
 *         if (subscripts)
 *                 boost::copy(s | transformed([] (auto s) {
 *                                         return const_cast<SgExpression*>(s);
 *                                 }), std::back_inserter(*subscripts));
 *         return result;
 * }
 */

EV get_subscripts(SgPntrArrRefExp* a)
{
	EV result;
	auto pr = &result;
	assert(isArrayReference(a, NULL, &pr));
	return result;
}

bool isArrayReference(const SgExpression* ref, const SgExpression** arrayName,
				vector<const SgExpression*> **subscripts)
{
	const SgExpression* arrayRef = nullptr;
	if (ref->variantT() == V_SgPntrArrRefExp) {
		if (subscripts != 0 || arrayName != 0) {
			const SgExpression* n = ref;
			while (true) {
				const SgPntrArrRefExp *arr = isSgPntrArrRefExp(n);
				if (!arr)
					break;
				n = arr->get_lhs_operand();
				// store left hand for possible reference exp to array variable
				if (arrayName)
					arrayRef = n;
				// right hand stores subscripts
				if (subscripts)
					(*subscripts)->push_back(arr->get_rhs_operand());
			} // end while
			if (arrayName)
				*arrayName = arrayRef;
		}
		return true;
	}
	return false;
}

bool isAssignmentStatement(const SgNode *s, const SgExpression** lhs,
				const SgExpression** rhs/*=nullptr*/,
				bool* readlhs/*=nullptr*/)
{
	const SgExprStatement *n = isSgExprStatement(s);
	const SgExpression *exp = (n != 0)? n->get_expression() : isSgExpression(s);
	if (exp != 0) {
		switch (exp->variantT()) {
		case V_SgPlusAssignOp:
		case V_SgMinusAssignOp:
		case V_SgAndAssignOp:
		case V_SgIorAssignOp:
		case V_SgMultAssignOp:
		case V_SgDivAssignOp:
		case V_SgModAssignOp:
		case V_SgXorAssignOp:
		case V_SgAssignOp: {
			const SgBinaryOp* s2 = isSgBinaryOp(exp);
			if (lhs != 0)
				*lhs = s2->get_lhs_operand();
			if (rhs != 0) {
				const SgExpression* init = s2->get_rhs_operand();
				if (init->variantT() == V_SgAssignInitializer)
					init = isSgAssignInitializer(init)->get_operand();
				*rhs = init;
			}
			if (readlhs != 0)
				*readlhs = (exp->variantT() != V_SgAssignOp);
			return true;
		}
		default:
			return false;
		}
	}
	return false;
}

/// Returns the variable that is declared in *d*.
/// Asserts that there is only one variable.
const SgInitializedName* get_declared_var(const SgVariableDeclaration* d)
{
	auto variables = d->get_variables();
	// each declarations contains only one variable
	assert(variables.size() == 1);
	return variables[0];
}

/// Accepts the following constructs:
/// * a = b;
/// * int a = b;
bool isSomeAssignment(const SgNode* s, const  SgInitializedName** lhs/* = nullptr*/,
					const SgExpression** rhs/* = nullptr*/)
{
	return isPureAssignment(s, lhs, rhs)
		|| isInitializedVariableDeclaration(s, lhs, rhs);
}

/// Checks whether s is an assignment (not +=, /= or someting like that).
/// Sets *lhs* to assigned variable and *rhs* to the assigned expression.
bool isInitializedVariableDeclaration(const SgNode* s,
					const SgInitializedName** lhs/* = nullptr*/,
					const SgExpression** rhs/* = nullptr*/)
{
	const SgVariableDeclaration* d = isSgVariableDeclaration(s);
	if (!d)
		return false;
	auto v = get_declared_var(d);
	auto gen_init = v->get_initializer();
	if (auto assign_init = isSgAssignInitializer(gen_init)) {
		if (lhs)
			*lhs = v;
		if (rhs)
			*rhs = assign_init->get_operand();
		return true;
	} else {
		return false;
	}
}

SgExpression* skip_casting(SgExpression* exp)
{
	SgCastExp *cast_exp = isSgCastExp(exp);
	if (cast_exp)
		return skip_casting(cast_exp->get_operand());
	else
		return exp;
}

const SgExpression* skip_casting(const SgExpression* exp)
{
	const SgCastExp *cast_exp = isSgCastExp(exp);
	if (cast_exp)
		return skip_casting(cast_exp->get_operand());
	else
		return exp;
}

// Replaces all (?) casts with the value they are casting in e.
// Used to remove these ugly (unsigned int)val inserted by value propagation.
// TODO avoid deepCopy()?
SgExpression* remove_casting(SgExpression* e, bool only_generated)
{
	using namespace boost::adaptors;
	auto casts = querySubTree<SgCastExp>(e);
	for (auto c : casts | reversed) {
		if (only_generated && !c->isTransformation())
			continue; // present in the source, leave as-is
		auto arg = deepCopy(c->get_operand());
		replaceExpression(c, arg);
		if (c == e) {
			e = arg;
		}
	}
	return e;
}

/// Replaces all references to constant variables in \a e
/// with their values
void propagate_constants(SgExpression *e)
{
	vector<SgVarRefExp*> var_refs;
	query_subtree<SgVarRefExp>(e, var_refs);
	for (auto v : var_refs) {
		auto init_name = v->get_symbol()->get_declaration();
		auto type = init_name->get_type();
		if (!isConstType(type))
			continue;
		auto ass_init = isSgAssignInitializer(init_name->get_initializer());
		if (!ass_init)
			continue;
		replaceExpression(v, deepCopy(ass_init->get_operand()));
	}
}

/// A custom version of SageInterface::forLoopNormalization()
///
/// Does not normalize initialization statement and propagates constant variables into bounds.
/// Is requested as modifying existing function would require rebuild of the whole framework.
/// Is used in loop unrolling.
bool modifiedForLoopNormalization(SgForStatement* loop)
{
	assert(loop);

	// Propagate constants into init statement
	auto initializers = loop->get_for_init_stmt()->get_init_stmt();
	if (initializers.size() > 1)
		return false;
	if (!initializers.empty()) {
		auto init = initializers[0];
		auto exp_stmt = isSgExprStatement(init);
		auto decl_stmt = isSgVariableDeclaration(init);
		if (decl_stmt) {
			auto initializer = isSgAssignInitializer(decl_stmt->get_variables()[0]->get_initializer());
			if (initializer)
				propagate_constants(initializer->get_operand_i());
		}
		if (exp_stmt)
			propagate_constants(exp_stmt->get_expression());
	}

	// Normalized the test expressions
	// -------------------------------------
	SgExpression* test = loop->get_test_expr();
	SgExpression* testlhs=nullptr, * testrhs=nullptr;

	// Propagate constants
	propagate_constants(test);

	if (isSgBinaryOp(test)) {
		testlhs = isSgBinaryOp(test)->get_lhs_operand();
		testrhs = isSgBinaryOp(test)->get_rhs_operand();
		assert(testlhs && testrhs);
	} else {
		return false;
	}
	// keep the variable since test will be removed later on
	SgVarRefExp* testlhs_var = isSgVarRefExp(skip_casting(testlhs));
	if (testlhs_var == nullptr)
		return false;
	SgVariableSymbol * var_symbol = testlhs_var->get_symbol();
	if (var_symbol==nullptr)
		return false;

	switch (test->variantT()) {
	case V_SgLessThanOp:	// i<x is normalized to i<= (x-1)
		replaceExpression(test, buildLessOrEqualOp(deepCopy(testlhs),
					buildSubtractOp(deepCopy(testrhs), buildIntVal(1))));
		// deepDelete(test);// replaceExpression() does this already by default.
		break;
	case V_SgGreaterThanOp: // i>x is normalized to i>= (x+1)
		replaceExpression( test, buildGreaterOrEqualOp(deepCopy(testlhs),
					buildAddOp(deepCopy(testrhs), buildIntVal(1))));
		break;
	case V_SgLessOrEqualOp:
	case V_SgGreaterOrEqualOp:
	case V_SgNotEqualOp: //TODO Do we want to allow this?
		break;
	default:
		return false;
	}
	// Normalize the increment expression
	// -------------------------------------
	SgExpression* incr = loop->get_increment();
	assert(incr != nullptr);

	// Propagate constants
	propagate_constants(incr);
	switch (incr->variantT()) {
	case V_SgPlusPlusOp: //i++ is normalized to i+=1
		{
			// check if the variables match
			SgVarRefExp* incr_var = isSgVarRefExp(skip_casting(isSgPlusPlusOp(incr)->get_operand()));
			if (incr_var == nullptr) return false;
			if ( incr_var->get_symbol() != var_symbol)
				return false;
			replaceExpression(incr,
					buildPlusAssignOp(isSgExpression(deepCopy(incr_var)),buildIntVal(1)));
			break;
		}
	case V_SgMinusMinusOp: //i-- is normalized to i+=-1
		{
			// check if the variables match
			SgVarRefExp* incr_var = isSgVarRefExp(skip_casting(isSgMinusMinusOp(incr)->get_operand()));
			if (incr_var == nullptr) return false;
			if ( incr_var->get_symbol() != var_symbol)
				return false;
			replaceExpression(incr,
					buildPlusAssignOp(isSgExpression(deepCopy(incr_var)), buildIntVal(-1)));
			break;
		}
	case V_SgMinusAssignOp: // i-= s is normalized to i+= -s
		{
			SgVarRefExp* incr_var = isSgVarRefExp(skip_casting(isSgMinusAssignOp(incr)->get_lhs_operand()));
			SgExpression* rhs = isSgMinusAssignOp(incr)->get_rhs_operand();
			assert(rhs != nullptr);
			if (incr_var == nullptr) return false;
			if ( incr_var->get_symbol() != var_symbol)
				return false;
			replaceExpression(incr,
					buildPlusAssignOp(isSgExpression(deepCopy(incr_var)),
						buildMultiplyOp(buildIntVal(-1), copyExpression(rhs))));
			break;
		}
	case V_SgAssignOp:
	case V_SgPlusAssignOp:
		break;
	default:
		return false;
	}

	// Normalize the loop body: ensure there is a basic block
	SgBasicBlock* body = ensureBasicBlockAsBodyOfFor(loop);
	assert(body!=nullptr);
	// Liao, 9/22/2009
	// folding entire loop may cause decreased accuracy for floating point operations
	// we only want to fold the loop controlling expressions
	//constantFolding(loop->get_parent());
	constantFolding(loop->get_test());
	constantFolding(loop->get_increment());

	return true;
}

/// Reimplementing this one because providing order as number among all index permutations
/// is... well, not user-friendly at all.
void loop_interchange(SgForStatement* loop, size_t depth, vector<size_t> &changed_order)
{
	// parameter verification
	ROSE_ASSERT(loop != nullptr);
	//must have at least two levels
	ROSE_ASSERT(depth >1);
	//TODO need to verify the input loop has n perfectly-nested children loops inside
	// save the loop nest's headers: init, test, and increment
	std::vector<SgForStatement* > loopNest = SageInterface::querySubTree<SgForStatement>(loop,V_SgForStatement);
	ROSE_ASSERT(loopNest.size()>=depth);
	std::vector<std::vector<SgNode*> > loopHeads;
	for (std::vector<SgForStatement* > ::iterator i = loopNest.begin(); i!= loopNest.end(); i++) {
		SgForStatement* cur_loop = *i;
		std::vector<SgNode*> head;
		head.push_back(cur_loop->get_for_init_stmt());
		head.push_back(cur_loop->get_test());
		head.push_back(cur_loop->get_increment());
		loopHeads.push_back(head);
	}

	// rewrite the loop nest to reflect the permutation
	// set the header to the new header based on the permutation array
	for (size_t i=0; i<depth; i++) {
		// only rewrite if necessary
		if (i != changed_order[i]) {
			SgForStatement* cur_loop = loopNest[i];
			std::vector<SgNode*> newhead = loopHeads[changed_order[i]];

			SgForInitStatement* init = isSgForInitStatement(newhead[0]);
			//ROSE_ASSERT(init != nullptr) // could be nullptr?
			ROSE_ASSERT(init != cur_loop->get_for_init_stmt());
			cur_loop->set_for_init_stmt(init);
			if (init) {
				init->set_parent(cur_loop);
				setSourcePositionForTransformation(init);
			}

			SgStatement* test = isSgStatement(newhead[1]);
			cur_loop->set_test(test);
			if (test) {
				test->set_parent(cur_loop);
				setSourcePositionForTransformation(test);
			}

			SgExpression* incr = isSgExpression(newhead[2]);
			cur_loop->set_increment(incr);
			if (incr) {
				incr->set_parent(cur_loop);
				setSourcePositionForTransformation(incr);
			}
		}
	}
}

/// A wrapper for SPRAY analysis.
///
LVAnalysis *initialize_liveness_analysis(SgProject *root)
{
	LVAnalysis *lvAnalysis = new LVAnalysis();
	lvAnalysis->setBackwardAnalysis();
	lvAnalysis->initialize(root);
	lvAnalysis->initializeTransferFunctions();
	lvAnalysis->initializeGlobalVariables(root);
	return lvAnalysis;
}

/// Calculates LV analysis results for a function.
///
void update_lv_analysis(LVAnalysis *lva, SgFunctionDefinition *f)
{
	lva->determineExtremalLabels(f);
	lva->run();
}

bool is_2d_variable_declaration(const SgStatement *s)
{
	auto decl = isSgVariableDeclaration(s);
	if (!decl)
		return false;
	auto var = decl->get_variables()[0];
	if (!var)
		return false; // ?
	return isSgArrayType(var->get_type());
}

SgStatement* get_loop_body(const SgForStatement *loop)
{
	return loop->get_loop_body();
}

SgStatement* get_loop_body(const SgWhileStmt *loop)
{
	return loop->get_body();
}

SgStatement* get_loop_body(const SgDoWhileStmt *loop)
{
	return loop->get_body();
}

SgNode* get_exp_with_loop_indices(const SgForStatement *loop)
{
	return loop->get_increment();
}

SgNode* get_exp_with_loop_indices(const SgWhileStmt *loop)
{
	return loop->get_condition();
}

SgNode* get_exp_with_loop_indices(const SgDoWhileStmt *loop)
{
	return loop->get_condition();
}

/// A wrapper for removeStatement() which removes enclosing loops when removing their bodies
///
void remove_statement(SgStatement *stmt)
{
	auto parent = isSgStatement(stmt->get_parent());
	if (is_a_loop(parent)) {
		removeStatement(parent);
		return;
	}
	auto block = isSgBasicBlock(parent);
	if (block && block->get_statements().size() == 1) {
		auto grandparent = isSgStatement(block->get_parent());
		if (is_a_loop(grandparent)) {
			removeStatement(grandparent);
			return;
		}
	}
	removeStatement(stmt);
}

/// Selects all expression statements and declarations in *block*
/// which can actually be a single statement.
/// Used in propagation and mapping.
///
/// \todo remove loop conditional expressions, etc...
SV split_block_into_statements(SgStatement *stmt)
{
	SV result = querySubTree<SgStatement>(stmt);
	result.erase(remove_if(result.begin(), result.end(),
			[] (const SgStatement *s) {
				return !(isSgExprStatement(s) ||
						isSgVariableDeclaration(s)) ||
					isSgForInitStatement(s->get_parent());
			}), result.end());
	return result;
}

void get_live_variables(LVAnalysis *lva, SgStatement *stmt, set<string> &live_outs)
{
	auto labeler = lva->getLabeler();
	auto mapping = lva->getVariableIdMapping();
	auto label = labeler->getLabel(stmt);
	if (label == Labeler::NO_LABEL) {
		assert(0);
		return;
	}
	//LVLattice* live_out = dynamic_cast<LVLattice*>(lva->getPostInfo(label));
	LVLattice* live_vars = dynamic_cast<LVLattice*>(lva->getPreInfo(label));
	for (auto it = live_vars->begin(); it != live_vars->end(); it++) {
		auto var = *it;
		/// \todo get declaration and an initialized name here?
		auto symbol = mapping->getSymbol(var);
		live_outs.insert(symbol->get_name());
	}
}

bool is_modifier(const SgAssignOp *assignment)
{
	assert(assignment);
	auto lhs = assignment->get_lhs_operand();
	auto rhs = assignment->get_rhs_operand();
	auto name = get_var_initialized_name(lhs);
	assert(name);
	CNS rhs_refs;
	get_referenced_vars(rhs, rhs_refs);
	return rhs_refs.count(name);
}

CNS convert_names_to_vars(SgScopeStatement* block, const STRS& vars)
{
	CNS result;
	for (auto v : vars) {
		auto s = lookupVariableSymbolInParentScopes(SgName(v), block);
		if (!s)
			throw Exception("Couldn't find variable " +
					s->get_name().getString() +
					" in provided and surrounding scopes.",
					block);
		result.insert(s->get_declaration());
	}
	return result;
}

namespace {
/// A wrapper for SPRAY reaching definitions analysis.
///
auto initialize_rd_analysis(SgProject *root)
{
	auto rda = std::make_unique<SPRAY::RDAnalysis>();
	rda->initialize(root);
	rda->initializeTransferFunctions();
	rda->initializeGlobalVariables(root);
	return rda;
}
} // namespace

DefinitionProvider::DefinitionProvider(SgFunctionDefinition* f,
					bool log/* = true*/)
: f(f), log(log)
{
	assert(f);
}

namespace {
/// Removes indices of all loops containing both *e0* and *e1*
/// from the set.
void remove_enclosig_loops_indices(const SgExpression *e0,
					const SgExpression *e1,
					NS &names)
{
	set<const SgForStatement*> e0_enc_loops;
	set<const SgForStatement*> e1_enc_loops;
	set<const SgForStatement*> enclosing_loops;
	get_enclosing_nodes<SgForStatement>(e0, e0_enc_loops);
	get_enclosing_nodes<SgForStatement>(e1, e1_enc_loops);
	set_intersection(e0_enc_loops.begin(), e0_enc_loops.end(),
			e1_enc_loops.begin(), e1_enc_loops.end(),
			inserter(enclosing_loops, enclosing_loops.end()));
	NS indices;
	for (auto l : enclosing_loops)
		get_nested_loops_indices(l, indices);
	NS result;
	set_difference(names.begin(), names.end(),
			indices.begin(), indices.end(),
			inserter(result, result.end()));
	names.swap(result);
}

class NameRemover {
	const NS &referenced_names;
public:
	NameRemover(const NS &r): referenced_names(r) {}
	bool operator()(auto e) const {
		auto name = get_var_initialized_name(e);
		return !referenced_names.count(name);
	}
};

/// Remove indices of all enclosing loops from both sets.
///
/// A CNS is created because loop indices are names
/// and def and prop are expression sets.
void prepare_references(const lref& reaching_definition,
			const lref& propagated_definition,
			ES& def, ES& prop)
{
	get_all_references(reaching_definition.first, def);
	get_all_references(propagated_definition.first, prop);
	NS referenced_names;
	for (auto r : def)
		referenced_names.insert(get_var_initialized_name(r));
	remove_enclosig_loops_indices(reaching_definition.first,
				propagated_definition.first, referenced_names);

	NameRemover nameRemover(referenced_names);
	erase_if(def, nameRemover);
	erase_if(prop, nameRemover);
	assert(def.size() == prop.size());
}

/// A hack to fix that operator -> leads to insertion of all
/// program variables to reaching definitions.
///
/// /todo Remove this when fixed.
void remove_buggy_defs(SV &definitions)
{
	if (definitions.size() < 2)
		return;
	definitions.erase(std::remove_if(definitions.begin(), definitions.end(),
			[] (auto s) {
				if (!s)
					return false;
				if (!querySubTree<SgArrowExp>(s).empty())
					return true;
				return false;
			}), definitions.end());
}
} // namespace

/// Checks if reaching definitions for variable and array references
/// are same in both parameters' expressions.
bool DefinitionProvider::propagation_was_correct(const lref &reaching_definition,
					const lref &propagated_definition)
{
	assert(rda);
	auto cur_log = log;
	log = false; // temporary disable logging
	ES references_in_definition;
	ES references_in_propagation;
	prepare_references(reaching_definition, propagated_definition,
			references_in_definition, references_in_propagation);

	reaching_defs reaching_in_definition;
	reaching_defs reaching_in_propagation;
	get_definitions(reaching_definition.second, references_in_definition,
			reaching_in_definition);
	get_definitions(propagated_definition.second, references_in_propagation,
			reaching_in_propagation);
	log = cur_log; // turn the logging back
	if (reaching_in_definition.size() != reaching_in_propagation.size())
		return false;
	for (auto def_it : reaching_in_definition) {
		auto def_ref = def_it.first;
		auto cdef_ref = const_cast<const SgExpression*>(def_ref);
		lref prop_ref;
		auto subexpressions = querySubTree<SgExpression>(cdef_ref);
		RefFinder finder(&subexpressions);
		auto prop_ref_it = find_if(reaching_in_propagation.begin(),
					reaching_in_propagation.end(), finder);
		if (prop_ref_it == reaching_in_propagation.end())
			return false;
		if (def_it.second.first != prop_ref_it->second.first)
			return false;
	}
	return true;
}

/// Calculates RD analysis results for *f*.
///
/// \todo Does it recalculate?
void DefinitionProvider::refresh()
{
	assert(rda);
	rda->determineExtremalLabels(f);
	rda->run();
}

/// Generates the actual definitions.
void DefinitionProvider::generate()
{
	if (!rda) {
		rda = initialize_rd_analysis(getProject());
		refresh();
	}
}

namespace {
/// Checks some conditions that can prevent us from using *expr*
/// as an expression to be propagated instead of *var*.
///
/// Checks if there are modifiers (++, += or something like that) in *expr*.
/// Also checks if there are references to *var_name*.
/// Prints messages on errors.
bool expr_can_be_used_for_propagation(SgInitializedName *var,
					SgExpression *expr)
{
	// \todo We do not need to check this as there should be no modifiers anyway?
	//NV modified_vars;
	//get_modified_variables(expr, modified_vars);
	//RefFinder finder(&(p.propagate));
	//auto expr_to_substitute = find_if(modified_vars.begin(),
	//                                modified_vars.end(), finder);
	//if (expr_to_substitute != modified_vars.end())
	//        throw Exception("more than one modifier of propagated variables in a statement; this is not supported",
	//                        expr);
	if (querySubTree<SgPlusPlusOp>(expr).size() ||
			querySubTree<SgMinusMinusOp>(expr).size() ||
			querySubTree<SgCompoundAssignOp>(expr).size() ||
			querySubTree<SgAssignOp>(expr).size()) {
		l << LOG_WARN << expr;
		l << "the variable which is requested to be propagated (";
		l << var;
		l << ") is assigned a variable's modifier, not propagating";
		l << endl;
		return false;
	}
	if (expr_contains_any_of_vars(expr, var)) {
		l << LOG_WARN << expr;
		l << "the variable which is requested to be propagated (";
		l << var << ") is assigned itself, not propagating" << endl;
		return false;
	}
	return true;
}

/// Returns conforming initializer for *ref* from *initialization*.
///
/// \todo Implement this for aggregate initialization.
SgExpression* get_initializer(SgExpression* ref, SgVariableDeclaration* init)
{
	assert(ref);
	assert(init);
	auto variables = init->get_variables();
	auto v = variables[0]; // each declarations contains olny one variable
	auto gen_init = v->get_initializer();
	auto assign_init = isSgAssignInitializer(gen_init);
	auto aggregate_init = isSgAggregateInitializer(gen_init);
	if (assign_init) {
		auto rhs = assign_init->get_operand();
		if (!expr_can_be_used_for_propagation(v, rhs))
			return nullptr;
		return rhs;
	} else if (aggregate_init) {
		return nullptr;
		/*
		auto initializers = aggregate_init->get_initializers()->get_expressions();
		for (auto i : initializers)
			propagate_selected_variables(i);
		initializers = aggregate_init->get_initializers()->get_expressions();
		if ((p.propagate).count(v->get_name().getString())) {
			bool updated = true;
			for (unsigned i = 0; i < initializers.size(); i++)
				updated = updated && update_var_state_from_declaration(v, initializers[i], i);
			if (updated)
				statements_to_check[v].push_back(init);
		} else if (p.bringout_vars.count(v->get_name().getString())) {
			bool possible = true;
			for (auto i : initializers)
				possible = possible && statement_can_be_brought_out(v, i, true);
			if (!possible) {
				l << LOG_ERR << init << "initialization of " << v;
				l << ", which is requested to be brought out, is not a loop invariant" << endl;
				p.bringout_vars.erase(v->get_name().getString());
			} else {
				l << "found initialization of " << v
					<< ", which will be brought out of the loop block" << endl;
				// \todo handle symbol tables
				removeStatement(init);
				insertStatementBefore(target, init);
			}
		} else {
			l << "initialization of " << v << " will be left as is" << endl;
			for (auto i : initializers)
				exprs_to_search_in.push_back(i);
		}
		*/
	} else if (gen_init) {
		l << LOG_WARN << init;
		l << "unsupported initializer of " << v << endl;
	}
	return nullptr;
}
} // namespace

/// Returns whether the definition was found.
/// Puts the expression that should be propagated
/// instead of *ref* into *result*.
///
/// Currently there should be exactly one element in *definitions*.
/// Distinguishes assignments and var declarations, checks if propagation
/// is possible using expr_can_be_used_for_propagation().
/// If *root* is set, locality to that node is checked.
bool DefinitionProvider::select_reaching_definition(SgExpression *ref,
			SV&& definitions, lref &result,
			SgStatement* root/* = nullptr*/)
{
	auto defined_var_name = get_var_initialized_name(ref);
	assert(defined_var_name);
	remove_buggy_defs(definitions);
	if (definitions.empty())
		return false;
	if (definitions.size() > 1) {
		if (log) {
			l << LOG_WARN << ref << " Not propagating variable ";
			l << defined_var_name;
			l << " as its reaching definition is ambiguous." << endl;
		}
		return false;
	}
	auto definition = definitions[0];
	if (root && (!is_child_of(definition, root))) {
		if (log) {
			l << LOG_WARN << ref << " Not propagating variable ";
			l << defined_var_name;
			l << " as its reaching definition is out of the target block's scope";
			l << " and you have requested local propagation for it.";
			l << endl;
		}
		return false;
	}
	// get value from definition
	///
	/// \todo put the following into a separate function.
	/// Or the preceding.
	if (auto expr_stmt = isSgExprStatement(definition)) {
		auto assignment = isSgAssignOp(expr_stmt->get_expression());
		if (!assignment)
			return false;
		auto lhs = assignment->get_lhs_operand();
		auto rhs = assignment->get_rhs_operand();
		SgInitializedName *var_name = get_var_initialized_name(lhs);
		if (!var_name)
			return false;
		if (var_name != get_var_initialized_name(ref))
			return false;
		if (!expr_can_be_used_for_propagation(var_name, rhs))
			return false;
		result = make_pair(rhs, definition);
		return true;
	} else if (isSgCompoundAssignOp(definition)) {
		if (log) {
			l << LOG_WARN << ref;
			l << " Not propagating variable " << defined_var_name;
			l << " as its reaching definition ";
			l << definition << " is of unexpected type." << endl;
		}
		// all modifying definitions should be
		// converted to assignments at this point?
		return false;
	} else if (auto declaration = isSgVariableDeclaration(definition)) {
		auto initializer = get_initializer(ref, declaration);
		if (!initializer)
			return false;
		result = make_pair(initializer, definition);
		return true;
	} else {
		return false;
	}
}

/// Gets reaching definitions for all references in *refs*
/// (which should be children of *stmt*) which have them.
///
/// \todo Maybe get *stmt* from *refs* using getEnclosingNode?
void DefinitionProvider::get_definitions(SgStatement* stmt, const rd_query& refs,
			reaching_defs& result, SgStatement* root/* = nullptr*/)
{
	assert(rda);
	auto labeler = rda->getLabeler();
	auto mapping = rda->getVariableIdMapping();
	auto label = labeler->getLabel(stmt);
	if (label == Labeler::NO_LABEL) {
		l << "Skipping generated statement: " << stmt->unparseToString() << endl;
		return;
	}
	auto reaching_definitions = dynamic_cast<RDLattice*>(rda->getPreInfo(label));
	for (auto elem : refs) {
		auto ref = elem.second;
		auto local = elem.first;
		auto var_ref = isSgVarRefExp(ref);
		auto arr_ref = isSgPntrArrRefExp(ref);
		VariableId var_id;
		if (var_ref)
			var_id = mapping->variableId(var_ref);
		else if (arr_ref)
			var_id = mapping->idForArrayRef(arr_ref);
		else
			assert(0);
		auto cur_rdefs = reaching_definitions->getRDs(var_id);
		if (cur_rdefs.empty()) // No reaching definitions for this reference
			continue;
		SV node_definitions;
		for (auto label : cur_rdefs) {
			auto node = labeler->getNode(label);
			auto stmt = isSgStatement(node);
			auto exp = isSgExpression(node);
			if (stmt) {
				node_definitions.push_back(stmt);
			} else if (exp) {
				// That's the case if a variable is modified in for loop increment;
				// We just put a placeholder here so that it would not get propagated
				node_definitions.push_back(nullptr);
			} else {
				l << "Found ";
				l << labeler->getNode(label)->unparseToString();
				l << " when searching for reaching definitions.";
				l << endl;
				continue;
			}
		}
		lref reaching_definition;
		if (!select_reaching_definition(ref, std::move(node_definitions),
						reaching_definition,
						(local ? root : nullptr)))
			continue;
		result.insert(make_pair(ref, reaching_definition));
	}
}

/// A wrapper for the other get_definitions() which doesn't give anything about
/// locality of propagation.
void DefinitionProvider::get_definitions(SgStatement* stmt, const ES& refs,
							reaching_defs& result)
{
	using namespace boost::adaptors;
	rd_query t;
	boost::copy(refs | transformed([] (auto r) {
				return std::make_pair(false, r);
			}), std::back_inserter(t));
	get_definitions(stmt, t, result);
}

/// Checks whether s is an assignment to a variable
/// (and not +=, /= or someting like that).
bool isPureAssignment(SgNode* s, SgInitializedName** lhs/* = nullptr*/,
					SgExpression** rhs/* = nullptr*/)
{
	return isPureAssignment(const_cast<const SgNode*>(s),
				const_cast<const SgInitializedName**>(lhs),
				const_cast<const SgExpression**>(rhs));
}

bool isPureAssignment(const SgNode* s,
			const SgInitializedName** lhs/* = nullptr*/,
			const SgExpression** rhs/* = nullptr*/)
{
	auto n = isSgExprStatement(s);
	auto exp = n ? n->get_expression() : isSgExpression(s);
	if (exp && (exp->variantT() == V_SgAssignOp)) {
		auto s2 = isSgBinaryOp(exp);
		auto l = isSgVarRefExp(s2->get_lhs_operand());
		if (!l)
			return false;
		if (lhs)
			*lhs = get_var_initialized_name(l);
		if (rhs) {
			auto init = s2->get_rhs_operand();
			// \todo is this required?
			if (init->variantT() == V_SgAssignInitializer)
				init = isSgAssignInitializer(init)->get_operand();
			*rhs = init;
		}
		return true;
	} else {
		return false;
	}
}

/// Recognizes assignments (lhs = rhs).
bool isPureAssignment(SgNode* s, SgExpression** lhs,
			SgExpression** rhs/* = nullptr*/)
{
	assert(lhs && s);
	auto n = isSgExprStatement(s);
	auto ass = isSgAssignOp(n ? n->get_expression() : isSgExpression(s));
	if (ass) {
		*lhs = ass->get_lhs_operand();
		if (rhs)
			*rhs = ass->get_rhs_operand();
		return true;
	} else {
		return false;
	}
}

namespace {
/// A simple implementation of the collectReadWriteRefs()
/// for the cases when it fails (when there is a function call).
void compute_read_write_refs(SgNode* node, EV& read_refs, EV& write_refs)
{
	EV refs;
	get_all_references(node, refs);
	for (auto ref : refs) {
		bool is_read;
		if (is_modified(ref, &is_read) || is_output(ref)) {
			write_refs.push_back(ref);
			if (!is_read)
				continue;
		}
		read_refs.push_back(ref);
	}
}
} // namespace

/// Imaplementation of collectReadWriteVariables() that
/// returns only expressions
void get_rw_refs(SgStatement* node, EV& read_refs, EV& write_refs)
{
	using namespace boost::adaptors;
	assert(node);
	std::vector<SgNode*> r, w;
	if (!SageInterface::collectReadWriteRefs(node, r, w)) {
		// fallback in case the analysis failed
		compute_read_write_refs(node, read_refs, write_refs);
		return;
	}
	boost::copy(r | transformed([] (auto r) {
				return isSgExpression(r);
			}) | filtered([] (auto r) {
				return is_a_var_reference(r);
			}), std::back_inserter(read_refs));
	boost::copy(w | transformed([] (auto r) {
				return isSgExpression(r);
			}) | filtered([] (auto r) {
				return is_a_var_reference(r);
			}), std::back_inserter(write_refs));
}

SgInitializedName* same_var_referenced(SgNode* a, SgNode* b)
{
	auto an = get_var_initialized_name(a);
	auto bn = get_var_initialized_name(b);
	if (an == bn)
		return an;
	else
		return nullptr;
}

/// Queries PntrArrRefs and VarRefs
/// which are not contained in another such node.
///
/// An implementation that distinguishes reads and writes.
///
/// \todo merge with the template version?
void get_top_references(SgNode *node, EV &r, EV &w)
{
	// query all nodes of interest and remove these having an array ref
	// as a parent
	auto top_filter = [node] (auto e) {
		auto enc_arr = getEnclosingNode<SgPntrArrRefExp>(e, node);
		return enc_arr != nullptr;
	};
	auto arr_refs = querySubTree<SgPntrArrRefExp>(node);
	auto var_refs = querySubTree<SgVarRefExp>(node);
	boost::remove_erase_if(arr_refs, top_filter);
	boost::remove_erase_if(var_refs, top_filter);

	// separate the modified ones from those just read
	// \todo functions?
	auto sorter = [&r, &w] (auto ref) {
		bool is_read;
		if (is_modified(ref, &is_read) || is_output(ref)) {
			w.push_back(ref);
			if (!is_read)
				return;
		}
		r.push_back(ref);
	};
	boost::for_each(var_refs, sorter);
	boost::for_each(arr_refs, sorter);
}

long long getSignedIntegerConstantValue(SgValueExp* expr)
{
	switch (expr->variantT()) {
	case V_SgCharVal: return (long long)(isSgCharVal(expr)->get_value());
	case V_SgUnsignedCharVal: return isSgUnsignedCharVal(expr)->get_value();
	case V_SgShortVal: return (long long)(isSgShortVal(expr)->get_value());
	case V_SgUnsignedShortVal: return isSgUnsignedShortVal(expr)->get_value();
	case V_SgIntVal: return (long long)(isSgIntVal(expr)->get_value());
	case V_SgUnsignedIntVal: return isSgUnsignedIntVal(expr)->get_value();
	case V_SgLongIntVal: return (long long)(isSgLongIntVal(expr)->get_value());
	case V_SgUnsignedLongVal: return isSgUnsignedLongVal(expr)->get_value();
	case V_SgLongLongIntVal: return isSgLongLongIntVal(expr)->get_value();
	case V_SgUnsignedLongLongIntVal: return isSgUnsignedLongLongIntVal(expr)->get_value();
	default: throw "Unexpected type in getSignedIntegerConstantValue()";
	}
}

/// Expresses one variable/array ref in terms of other expressions forming an assignment.
///
/// Works recursively, moving those parts of binary operations found in *rhs*
/// that don't contain *map_to* to *lhs* while there are no supported operations
/// left on top. If a variable or an array is left, the rule is created, else
/// a warning is issued and mapping does not happen.
///
/// * a[i][j] = +/-b[j][i] ---> b[j][i] : -/+a[i][j]
/// * x = a + b - c*y      ---> y : (a + b - x)/c
bool express_var_in_rhs(SgExpression*& lhs, SgExpression*& rhs, const string &target_var)
{
	auto cleanup = [&lhs, &rhs] {
		deepDelete(lhs);
		lhs = nullptr;
		deepDelete(rhs);
		rhs = nullptr;
	};
	rhs = skip_casting(rhs);
	if (isSgVarRefExp(rhs) || isSgPntrArrRefExp(rhs)) {
		// we reached the bottom
		auto var = get_var_initialized_name(rhs);
		if (!var || (var->get_name() != target_var)) {
			cleanup();
			return false;
		}
		return true;
	}
	// unary +/-
	if (auto minus_op = isSgMinusOp(rhs)) {
		lhs = buildMinusOp(lhs, SgUnaryOp::prefix);
		rhs = minus_op->get_operand();
		return express_var_in_rhs(lhs, rhs, target_var);
	} else if (auto plus_op = isSgUnaryAddOp(rhs)) {
		rhs = plus_op->get_operand();
		return express_var_in_rhs(lhs, rhs, target_var);
	}

	// binary operators
	auto bin_op = isSgBinaryOp(rhs);
	if (!bin_op) {
		cleanup();
		return false;
	}
	auto rhs_op = bin_op->get_rhs_operand();
	auto lhs_op = bin_op->get_lhs_operand();
	bool var_is_in_lhs = node_references_var(lhs_op, target_var);
	if (isSgAddOp(rhs)) {
		if (var_is_in_lhs)
			swap(lhs_op, rhs_op);
		lhs = buildSubtractOp(lhs, lhs_op);
		rhs = rhs_op;
	} else if (isSgMultiplyOp(rhs)) {
		if (var_is_in_lhs)
			swap(lhs_op, rhs_op);
		lhs = buildDivideOp(lhs, lhs_op);
		rhs = rhs_op;
	} else if (isSgSubtractOp(rhs)) {
		if (var_is_in_lhs) {
			lhs = buildAddOp(lhs, rhs_op);
			rhs = lhs_op;
		} else {
			lhs = buildSubtractOp(lhs_op, lhs);
			rhs = rhs_op;
		}
	} else if (isSgDivideOp(rhs)) {
		if (var_is_in_lhs) {
			lhs = buildMultiplyOp(lhs, rhs_op);
			rhs = lhs_op;
		} else {
			lhs = buildDivideOp(lhs_op, lhs);
			rhs = rhs_op;
		}
	} else {
		cleanup();
		return false;
	}
	return express_var_in_rhs(lhs, rhs, target_var);
}

bool express_var_in_rhs(SgExpression*& lhs, SgExpression*& rhs, SgInitializedName* target_var)
{
	string var = target_var->get_name().str();
	return express_var_in_rhs(lhs, rhs, var);
}

} // namespace TfUtils

