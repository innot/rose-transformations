#pragma once

#include "sage3basic.h"
#include <string>
#include <set>
#include <vector>
#include <map>
#include "util.h"

/// This class handles user input - parses commandline parameters,
/// pragmas, and calls transforming or analysing code according to
/// the extracted orders.
class PragmaProcessor {
	/// Root node for the handled project.
	SgProject* project;
	/// Inline all functions.
	bool inline_all;
	/// States whether we should call gcc after transformations.
	bool compile;
	/// When unparsing, indentaion level will be this many spaces.
	int indentation;
public:
	PragmaProcessor(int argc, char *argv[]);
	/// Parses pragmas and performs the requests.
	void process();
	/// Unparses the result or calls backend depending on *compile* value.
	/// \todo add filename as a parameter?
	int backend();
};

