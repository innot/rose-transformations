#include "CodeAnalyser.h"
#include "sage3basic.h"
#include "util.h"
#include "ReductionAnalysis.h"
#include "DependencyAnalysis.h"

using namespace SageInterface;
using namespace TfUtils;

namespace {

} // namespace

CodeAnalyser::CodeAnalyser(SgStatement* block, TfUtils::AnalysisParams&& params)
: p(params), block(block)
{
}

CodeAnalyser::~CodeAnalyser() = default;

void CodeAnalyser::handle_block()
{
	using namespace CodeAnalyses;
	for (auto loop : query_loop_nests(block)) {
		if (p.reduction) {
			ReductionAnalysis a(loop);
			a.run();
		}
		if (p.dependency) {
			// TODO ...
			DependencyAnalysis a(loop);
			a.run();
			//a.annotate_source();
		}
	}
}
