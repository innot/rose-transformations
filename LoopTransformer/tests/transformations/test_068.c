#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	double b[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++) {
			a[i][j] = i*j*3 - i + j*2 + 1;
			b[i][j] = i*i*3 - j*j + i*j*2 + 8;
		}

	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;
	unsigned i = 0;
	#pragma x unroll
	while (i < 4) {
		#pragma x unroll
		for (j = 0; j < 3; j++)
			bc[i][j] = i*j - j;
		a[i][0] = bc[i % 2][i % 2];
		i++;
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, a[i][j]);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, b[i][j]);
	return 0;
}
