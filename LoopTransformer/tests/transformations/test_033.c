#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0;
	#pragma x propagate(t)
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			t = 0;
			switch (j) {
			case 0:
				vol += i/10.0;
				t++;
				break;
			case 1:
				vol += 0.2;
				break;
			default:
				vol += j/10.0;
			}
			t += 10*vol;
			a[i][j] += t;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

