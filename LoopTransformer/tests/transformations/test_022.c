#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5, k = 0;
	int c[2] = {2, 3};
	int c2[2][2] = {{2, 3}, {4, 5}};
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(t, c2, vol)
		for (unsigned j = 0; j < 4; j++) {
			int vol = 2;
			t = j;
			#pragma x unroll
			for (int na = 0; na < 2; na++)
			for (int ic = 0; ic < 2; ic++)
				c2[na][ic] = c2[na][ic]/vol;
			a[i][j] = c2[i/2][j/2] + c2[1][1] - t;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

