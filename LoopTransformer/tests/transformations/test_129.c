#include <stdio.h>

int main()
{
	const int n = 10;
	const int m = 10;
	int a[m][n];
	int t, t1, t2 = 4;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;

	for (unsigned i = 0; i < m; i++)
		#pragma x propagate(t, t2)
		for (unsigned j = 0; j < n; j++) {
			t = 15;
			t = 16;
			t2 = 0;
			if (i)
				t = 0;
			if (j)
				a[i][j] = t;
			else
				a[i][j] = i - j;
		}


	t = 10;
	a[0][t2] = 55;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%i \n", a[i][j]);

}
