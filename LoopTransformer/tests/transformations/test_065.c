#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	double b[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++) {
			a[i][j] = i*j*3 - i + j*2 + 1;
			b[i][j] = i*i*3 - j*j + i*j*2 + 8;
		}

	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j = 0;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;
	#pragma x split(a, b)
	for (unsigned i = 0; i < 4; i++) {
		t1 = i*i;
		double x[4] = {t, t1, 5, 0.2*t};
		#pragma x unroll
		for (j = 0; j < 3; j++)
			bc[i][j] = x[j] - j;
		double vol = bc[i][0] + bc[i][1] + bc[i][2];
		#pragma x unroll
		for (j = 0; j < 3; j++)
			bc[i][j] /= vol;
		a[i][0] = bc[i % 2][i % 2];
		for (j = 0; j < 4; j++)
			b[i][j] += i * j;
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, a[i][j]);
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%u:%u %f \n", i, j, b[i][j]);
	return 0;
}


