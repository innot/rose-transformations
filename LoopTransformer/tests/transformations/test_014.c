#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 5, t1 = 5, t2 = 6;
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(t1)
		for (unsigned j = 0; j < 4; j++) {
			t++;
			t1++;
			a[i][j] = t;
			a[0][j] = t1;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

