#include <stdio.h>

int f(int x)
{
        int r = x;
        for (int i = 0; i < x; i++) 
                r *= r;
        return r;
}

void g(int x)
{
        int r = x;
        for (int i = 0; i < x; i++) 
                r *= r;
        printf("%i\n",r);
}

int id(int x)
{
        return x;
}

int main()
{
        int a = 5;
        int r = 6;
        double b = 0.42;
        double c = 5e2;
        int results[6];
        for (int i = id(0); i < id(6); i++) {
                int rose_temp__4;
                int _x_20 = i;
                rose_temp__4 = _x_20;
                results[_x_20] = f(i);
        }
        for (int i = 0; i < 6; i++) 
                printf("%i ",results[i]);
}
