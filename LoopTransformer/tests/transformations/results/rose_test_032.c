#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        int a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        int t = 0;
        int t1 = 3;
        int t2 = 5;
        int k = 0;
        double vol = 0;
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        switch(j){
                                case 0:
{
                                        vol += i / 10.0;
                                        break; 
                                }
                                case 1:
{
                                        vol += 0.2;
                                        break; 
                                }
                                default:
                                vol += j / 10.0;
                        }
// problems with type conversion
// t = 10*(int)vol fixes the test :)
                        a[i][j] *= ((int )(((double )10) * vol));
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%i \n",a[i][j]);
}
