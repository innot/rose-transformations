#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        int a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        double bc[3][3] = {{(0), (1)}, {(1), (2)}};
        double bs[3][3] = {{(0), (1)}, {(1), (2)}};
        int t = 0;
        int t1 = 3;
        int t2 = 5;
        int k = 0;
        double vol = 0;
        double vol0 = 0;
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        bs[1][1] = (a[0][1] - a[i][j]);
                        bs[1][2] = (a[1][2] + a[j][i]);
                        bs[2][1] = (a[3][0] - a[j][i]);
                        bs[2][2] = (a[0][3] + a[i][j]);
                        bc[1][1] = bc[2][1] + 2 * ((double )(a[0][3] + a[i][j]));
                        bc[1][2] += (i * j) + -2 * ((double )(a[3][0] - a[j][i]));
                        bc[2][1] = ((double )(a[0][1] - a[i][j])) - 2 * ((double )(a[1][2] + a[j][i]));
                        bc[2][2] = (t1 + (t2++) / (i + 1) - i * 5 * j) + 2 * ((double )(a[0][1] - a[i][j]));
                        vol = ((double )(a[0][1] - a[i][j])) * ((double )(a[0][3] + a[i][j])) - ((double )(a[1][2] + a[j][i])) * ((double )(a[3][0] - a[j][i]));
                        bc[1][1] = bc[1][1] / vol;
                        bc[1][2] = bc[1][2] / vol;
                        bc[2][1] = bc[2][1] / vol;
                        bc[2][2] = bc[2][2] / vol;
                        vol0 = vol + bc[1][1] * bc[2][2] - bc[1][2] * bc[2][1];
                        a[i][j] += vol + bc[1][1] * bc[2][2] - bc[1][2] * bc[2][1];
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%i \n",a[i][j]);
}
