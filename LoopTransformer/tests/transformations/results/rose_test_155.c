#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        double a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        double b[4][4] = {{(34), (34), (675), (897)}, {(45), (34), (672), (234)}, {(45), (56), (9234), (54)}, {(-12), (34), (672), (234)}};
        const unsigned int bk = 3;
        double bc[bk][bk];
        double bs[bk][bk];
        for (unsigned int i = 0; i < bk; i++) 
                for (unsigned int j = 0; j < bk; j++) {
                        bc[i][j] = (i * j * 5 - j + i * 2 + 1);
                        bc[i][j] = (i * j * 5 - j - i * 5 + 2);
                }
        int j;
        double t = 0;
        double t1 = 3;
        double t2 = 5;
        double k = 0;
        double vol = 0.1;
        double vol0 = 0.1;
        unsigned int y = 0;
        double total = 0;
        for (int i = 0; i < 4; i++) {
                t = ((double )i);
                a[i][0] += ((double )2) * t;
                total += a[i][0];
        }
        for (int i = 0; i < 4; i++) {
                t = i;
                b[i][0] += 5 * t;
                total += b[i][0];
        }
        for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                        a[i][0] /= total;
                }
        }
        for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                        b[i][0] /= total;
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%u:%u %f \n",i,j,a[i][j]);
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%u:%u %f \n",i,j,b[i][j]);
        return 0;
}
