#include <stdio.h>

int f(int x[6])
{
        return x[0] + x[1];
}

void g(int x[5])
{
        const int _mm3 = 4;
        int t0[_mm3];
        int t1[_mm3 + 1];
        int t2[_mm3][2];
        t0[0] = 5;
        t1[1] = 6;
        t2[1][2] = 0;
        x[0] = t1[1] + t2[1][2];
        x[1] = t0[0];
}

int id(int x)
{
        return x;
}

int main()
{
        int a = 5;
        int r = 6;
        double b = 0.42;
        double c = 5e2;
        int results[6];
        for (int i = id(0); i < id(6); i++) 
                results[i] = i;
        for (int i = id(0); i < id(6); i++) {
                int _x_20 = i;
                int *_x_61 = results;
                results[_x_20] = _x_61[0] + _x_61[1];
                int *_x_92 = results;
                int _t04[_mm3];
                int _t15[_mm3 + 1];
                int _t26[_mm3][2];
                _t04[0] = 5;
                _t15[1] = 6;
                _t26[1][2] = 0;
                _x_92[0] = _t15[1] + _t26[1][2];
                _x_92[1] = _t04[0];
        }
        for (int i = 0; i < 6; i++) 
                printf("%i ",results[i]);
}
