#include <stdio.h>

int main()
{
        const unsigned int m = 4;
        const unsigned int n = 4;
        int a[m][n];
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < m; j++) 
                        a[i][j] = (i * j * 3 - i + j * 2 + 1);
        int t;
        int t1;
        int t2;
        double vol;
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        t = 15;
                        t1 = i;
                        while(t1-- > 0){
                                t = t * 0.5;
                        }
                        a[i][j] /= 0.5;
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%i \n",a[i][j]);
}
