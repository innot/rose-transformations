#include <stdio.h>

int main()
{
        const unsigned int n = 16;
        const unsigned int m = 4;
        const unsigned int k = 3;
        int a[m][n];
        double bc[k][k];
        double bs[k][k];
        double c[k];
        int j;
        int i;
        double t = 0;
        double t1 = 3;
        double t2 = 5;
        double vol = 0.1;
        double vol0 = 0.1;
        for (unsigned int i = 0; i < m; i++) 
                for (unsigned int j = 0; j < n; j++) 
                        a[i][j] = (i * j * 3 - 5 * j + i + 8);
        for (unsigned int i = 0; i < k; i++) {
                c[i] = (i * k * 6 - 3 * i - 2);
                for (unsigned int j = 0; j < k; j++) {
                        bs[i][j] = (i * i * 3 + i);
                        bc[i][j] = (j * k * 6 - 5 * j + i + 8);
                }
        }
// here c[u] should'n be propagated as it's initialized
// in a scope which is not a parent of the one we are propagation into
        for (unsigned int i = 0; i < 4; i++) {
                for (unsigned int j = 0; j < 4; j++) {
                        int u;
                        vol = 50;
                        for (u = 0; u < k; u++) {
                                c[u] = vol * u;
                                vol = vol + c[u];
                        }
                        vol = j;
                        for (u = 0; u < k; u++) {
                                vol = vol + c[u];
                                a[i][j] -= vol * k;
                        }
                        a[i][j] = (c[1] - vol + ((double )0));
                }
        }
        for (unsigned int i = 0; i < 4; i++) 
                for (unsigned int j = 0; j < 4; j++) 
                        printf("%i \n",a[i][j]);
}
