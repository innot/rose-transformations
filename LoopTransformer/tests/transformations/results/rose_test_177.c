#include <stdio.h>

int f(int x[6])
{
        return x[0] + x[1];
}

void g(int x[5])
{
        int t = x[0];
        x[0] = x[1];
        x[1] = t;
}

int id(int x)
{
        return x;
}

int main()
{
        int a = 5;
        int r = 6;
        double b = 0.42;
        double c = 5e2;
        int results[6];
        for (int i = id(0); i < id(6); i++) 
                results[i] = i;
        for (int i = id(0); i < id(6); i++) {
                int _x_20 = i;
                int *_x_61 = results;
                results[_x_20] = _x_61[0] + _x_61[1];
                int *_x_92 = results;
                int _t3 = _x_92[0];
                _x_92[0] = _x_92[1];
                _x_92[1] = _t3;
        }
        for (int i = 0; i < 6; i++) 
                printf("%i ",results[i]);
}
