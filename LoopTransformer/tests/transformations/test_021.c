#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 0, t1 = 3, t2 = 5, k = 0;
	int c[2] = {2, 3};
	int c2[2][2] = {{2, 3}, {4, 5}};
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(t, c)
		for (unsigned j = 0; j < 4; j++) {
			t = j;
			#pragma x unroll
			for (k = 0; k < 2; k++) {
				c[k] = t*k;
				c[1 - k] += j;
			}
			a[i][j] = (i ? 3 : c[0]) + t/2;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

