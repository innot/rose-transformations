#include <stdio.h>

int main()
{
	const unsigned n = 16;
	const unsigned m = 4;
	int a[m][n];
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j, i;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;

	#pragma x swap(0:1)
	{
	#pragma x id(1) nontemp
	{
		double x = 1;
		printf("%f\n", x);
	}

	#pragma x id(0) nontemp
	{
		double x = 1;
		printf("%f\n", x);
	}
	}

	#pragma x swap(0&1)
	{
	#pragma x id(1)
	{
		double x = 1;
		printf("%f\n", x);
	}

	#pragma x id(0) nontemp
	{
		double x = 1;
		printf("%f\n", x);
	}
	}
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%d \n", a[i][j]);

}

