#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double bc[3][3] = {{0,1}, {1,2}}, bs[3][3] = {{0,1}, {1,2}};
	int t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0, vol0 = 0;
	#pragma x propagate(vol0) map(bc:bs)
	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++) {
			bs[1][1] = a[0][1] - a[i][j];
			bs[1][2] = a[1][2] + a[j][i];
			bs[2][1] = a[3][0] - a[j][i];
			bs[2][2] = a[0][3] + a[i][j];

			bc[1][1] = 2*bs[2][2];
			bc[1][2] = -2*bs[2][1];
			bc[2][1] = -2*bs[1][2];
			bc[2][2] = 2*bs[1][1];
			vol = bs[1][1] * bs[2][2] - bs[1][2] * bs[2][1];

			#pragma x unroll
			for (int na = 1; na < 3; na++)
				for (int ic = 1; ic < 3; ic++)
					bc[na][ic] = bc[na][ic]/vol;

			vol0 = vol + bc[1][1] * bc[2][2] - bc[1][2] * bc[2][1];

			a[i][j] += vol0;
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

