#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t, t1, t2;
	int c[2];
	int x[2];
	for (unsigned i = 0; i < 4; i++)
		#pragma x propagate(c, x, t, t1)
		for (unsigned j = 0; j < 4; j++) {
			x[0] = i*i;
			x[1] = j*j;
			c[0] = i*j;
			c[1] = j*i;
			t = 15;
			t1 = t + c[0];
			if (j)
				a[i][j] = x[0] + c[1];
			else
				a[i][j] = t1 + x[1];
		}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}
