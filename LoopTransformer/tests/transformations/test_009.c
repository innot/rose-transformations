#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	int a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	int t = 7, t1 = 4, t2 = 8;
	int c[2] = {54, 76};
	int c2[4] = {10, 16};
	for (unsigned i = 0; i < 4; i++) {
		#pragma x propagate(c, t, t1)
		for (unsigned j = 0; j < 4; j++) {
			c[0] = i + j;
			c[1] = j;
			c2[j] = j;
			t = 15;
			t1 = (i - j ? t : c2[j]);
			a[i][c2[j]] = t1;
		}
	}

	for (unsigned i = 0; i < 4; i++)
		for (unsigned j = 0; j < 4; j++)
			printf("%i \n", a[i][j]);

}

