#include <stdio.h>

int f(int x)
{
	int r = x;
	for (int i = 0; i < x; i++)
		r *= r;
	return r;
}

void g(int x)
{
	int r = x;
	for (int i = 0; i < x; i++)
		r *= r;
	printf("%i\n", r);
}

int h(int x, int y)
{
	int r = x + y;
	for (int i = 0; i < x - y; i++)
		r *= r;
	printf("%i\n", r);
	return r;
}

int main()
{
	#pragma x inline
	if (f(3))
		printf("true 1\n");
	else
		printf("false 1\n");

	#pragma x inline
	if (f(4) + f(5))
		printf("true 2\n");
	else if (h(6, 2))
		printf("not true 2\n");

	#pragma x inline
	if (f(f(4)) + h(5, 2))
		printf("true 3\n");
}

