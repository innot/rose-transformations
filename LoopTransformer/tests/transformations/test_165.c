#include <stdio.h>

int f(int x)
{
	return (x ? x*f(x-1) : 1);
}

double g(double x, double y)
{
	return x*x - y*y;
}

int main()
{
	int a = 5;
	double b = 0.42;
	double c = 5e2;
	#pragma x inline propagate(t)
	for (int i = 0; i < 6; i++) {
		int t = f(g(a, a));
		printf("%i\n", t);
	}
}

