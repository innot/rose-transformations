#include <stdio.h>

int main()
{
	const unsigned n = 16;
	const unsigned m = 4;
	int a[m][n];
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j, i;
	double t = 0, t1 = 3, t2 = 5, k = 0;
	double vol = 0.1, vol0 = 0.1;

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			a[i][j] = i*j*3 - 5*j + i + 8;

	#pragma x merge(0, 1, 2)
	{
	#pragma x id(0)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			a[i][j] = j*3;
		}

	#pragma x id(1)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int tmp = i*j*3;
			a[i][j] -= tmp;
		}

	#pragma x id(2)
	for (i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++) {
			int tmp = i*j*2;
			a[i][j] += tmp;
		}
	}

	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < n; j++)
			printf("%d \n", a[i][j]);

}
