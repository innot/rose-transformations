BEGIN {
	printf "Test";
	system("./test_analyser.sh " type " header");
	printf "Status|Warnings|Errors\n";
	printf "----";
	system("./test_analyser.sh " type);
	printf "------|--------|------\n";
}

{ printf "%s", $1; }
{ system("./test_analyser.sh " type " file " type "/" $1); }
{ printf "%s|", $2; }
/\[W\]/ { printf "W"; }
{ printf "|"; }
/\[E\]/ { printf "E"; }
{ printf "\n"; }
