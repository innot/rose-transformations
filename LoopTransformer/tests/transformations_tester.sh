#!/usr/bin/env bash
exec 2> /dev/null

cd ./results;
test=$1;
test_path="../$test";
old_res="${test}_orig";
new_res="${test}_new";
translator_path="../../../translator";
translator_params = "--edg:no_warnings";
log_path="$test.log"

cleanup() {
	rm -f $old_res $new_res $log_path $test.o rose_$test.o;
}

echo -n $test " ";
echo "a" > $old_res;
echo "b" > $new_res;
rm $log_path
$($translator_path $translator_params $test_path &> $log_path);
tr_code=$?
if [[ $tr_code != 0 && $tr_code != 23 ]]; then # 23 means transformation error
	echo -e "\t\033[35m[ABORTED WITH CODE=$tr_code]\033[0m";
	cleanup;
	exit;
fi;
gcc -std=c99 -g -o $test.o $test_path &>> $log_path;
timeout 1 ./$test.o > $old_res;
orig_code=$?
if [ $orig_code -eq 124 ]; then
	echo -e "\t\033[36m[TIMED OUT / TEST]\033[0m";
	cleanup;
	exit;
elif [ $orig_code -ne 0 ]; then
	echo -e "\t\033[31m[FAILED]\033[0m";
	cleanup;
	exit;
fi;
gcc -std=c99 -g -o rose_$test.o rose_$test &>> $log_path;
timeout 1 ./rose_$test.o > $new_res 2> /dev/null;
res_code=$?
if [ $res_code -eq 124 ]; then
	echo -e "\t\033[36m[TIMED OUT / RESULT]\033[0m";
	cleanup;
	exit;
elif [ $res_code -ne 0 ]; then
	echo -e "\t\033[31m[FAILED]\033[0m";
	cleanup;
	exit;
fi;
if [ "x$(diff -up $old_res $new_res)" == "x" ]; then
	echo -en "\t\033[32m[PASSED]\033[0m";
else
	echo -en "\t\033[31m[FAILED]\033[0m";
fi;
if [ "x$(grep WARNING $log_path)" != "x" ]; then
	echo -en "\t\033[33m[W]\033[0m";
else
	echo -en "\t   ";
fi;
if [ "x$(grep ERROR $log_path)" != "x" ]; then
	echo -en "\033[31m[E]\033[0m";
fi;
echo;
cleanup;

cd ..;

