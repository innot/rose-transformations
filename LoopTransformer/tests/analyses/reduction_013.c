#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 3, yt = 6;
	double maxc0 = c[0];
	double maxc1 = c[0];
	double maxc2 = c[0];
	double maxc3 = c[0];
	double maxc4 = c[0];
	double maxc5 = c[0];
	double maxc6 = c[0];
	double maxc7 = c[0];
	double maxc8 = c[0];
	double maxc9 = c[0];
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		maxc0 = (c[i] < maxc1 ? maxc1 : c[i]);
	for (int i = 0; i < m; i++)
		maxc1 = (c[i] < maxc0 ? maxc1 : c[i]);
	for (int i = 0; i < m; i++)
		maxc2 = (c[0] < maxc2 ? maxc2 : c[0]);
	for (int i = 0; i < m; i++)
		maxc3 = (c[i] < maxc3 ? maxc3 : c[0]);
	for (int i = 0; i < m - 1; i++)
		maxc4 = (c[i + 1] < maxc4 ? maxc4 : c[i]);
	for (int i = 0; i < m; i++)
		maxc5 = (c[i]++ < maxc5 ? maxc5 : c[i]);
	for (int i = 0; i < m; i++)
		maxc6 = (c[i] < maxc6 ? maxc6 : --c[i]);
	for (int i = 0; i < m; i++)
		maxc7 = (c[i] < maxc7 ? (maxc7 + 1) : c[i]);
	for (int i = 0; i < m; i++)
		maxc8 = (c[i] < maxc8 ? (maxc8 += 1) : c[i]);
	for (int i = 0; i < m; i++)
		maxc9 = (c[i] < maxc9-- ? maxc9 : c[i]);
	}
	return 0;
}
//+F pragma PRG REDUCTION(maxc0(MAX))
//+F pragma PRG REDUCTION(maxc1(MAX))
//+F pragma PRG REDUCTION(maxc2(MAX))
//+F pragma PRG REDUCTION(maxc3(MAX))
//+F pragma PRG REDUCTION(maxc4(MAX))
//+F pragma PRG REDUCTION(maxc5(MAX))
//+F pragma PRG REDUCTION(maxc6(MAX))
//+F pragma PRG REDUCTION(maxc7(MAX))
//+F pragma PRG REDUCTION(maxc8(MAX))
//+F pragma PRG REDUCTION(maxc9(MAX))
