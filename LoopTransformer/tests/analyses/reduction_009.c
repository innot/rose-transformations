#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 3, yt = 6;
	double maxc = c[0];
	double maxc1 = c[0];
	double minc = c[0];
	double maxa = a[0][0];
	double mina = a[0][0];
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		maxc = (c[i] < maxc ? maxc : c[i]);
	for (int i = 0; i < m; i++)
		maxc1 = (c[i] > maxc1 ? c[i] : maxc1);
	for (int i = 0; i < m; i++)
		minc = (c[i] > minc ? minc : c[i]);
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			maxa = (maxa > a[i][j] ? maxa : a[i][j]);
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			mina = (mina > a[i][j] ? a[i][j] : mina);
	}
	printf("%f", maxc);
	printf("%f", minc);
	printf("%f", maxa);
	printf("%f", mina);
	return 0;
}
//+T pragma PRG REDUCTION(MAX(maxc))
//+T pragma PRG REDUCTION(MAX(maxc1))
//+T pragma PRG REDUCTION(MIN(minc))
//+T pragma PRG REDUCTION(MAX(maxa))
//+T pragma PRG REDUCTION(MIN(mina))
