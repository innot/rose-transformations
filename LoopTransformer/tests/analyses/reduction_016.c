#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double x[4] = {1,2,3,4};
	double y[4] = {1,2,3,4};
	int r0[4] = {0, 0, 0, 0};
	int r1[4] = {0, 0, 0, 0};
	int r2[4] = {0, 0, 0, 0};
	#pragma a reduction
	{
	for (int i = 0; i < m; i++) {
		if (x[i] != y[i])
			r0[i] = 0;
		else
			r0[i] = 1;
	}
	for (int i = 0; i < m; i++)
		if (x[i] == y[i])
			r1[i] = 1;
		else
			r1[i] = 0;
	for (int i = 0; i < m; i++)
		if (x[i] == y[i])
			r2[i] = 1;
	}
	return 0;
}
//+T pragma PRG REDUCTION(NEQV(r0))
//+T pragma PRG REDUCTION(EQV(r1))
//+T pragma PRG REDUCTION(EQV(r2))
