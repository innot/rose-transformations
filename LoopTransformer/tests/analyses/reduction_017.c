#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	double y = 3, yt = 6;
	double maxc0 = c[0];
	double maxc1 = c[0];
	double maxc2 = c[0];
	double maxc3 = c[0];
	double maxc4 = c[0];
	double maxc5 = c[0];
	int loc[5] = { -1, -1, -1, -1, -1 };
	int loc0 = -1;
	int loc1 = -1;
	int loc2 = -1;
	int loc3[5] = { -1, -1, -1, -1, -1 };
	int loc4[5] = { -1, -1, -1, -1, -1 };
	int loc5 = -1;
	#pragma a reduction
	{
	for (int i = 0; i < m; ++i) {
		double y = c[i] + 3;
		if (maxc0 < y) {
			maxc0 = y;
			loc[0] = i;
			loc[1] = i;
			loc[2] = i*i;
			loc[3] = i;
			loc[4] = i;
		}
	}
	for (int i = 0; i < m; ++i) {
		double y = c[i];
		if (maxc1 < y) {
			maxc1 = y;
			loc1 = i;
			loc2 = i*i;
		}
	}
	for (int i = 0; i < m; ++i) {
		double y = c[i] + 3;
		if (maxc4 < y) {
			maxc4 = y;
			loc3[0] = i;
			loc3[1] = y;
			loc3[3] = maxc4;
			loc3[4] = m;
		}
	}
	for (int i = 0; i < m; ++i) {
		double y = c[i] + 3;
		if (maxc5 < y) {
			maxc5 = y;
			loc4[0] = i;
			loc4[1] = y;
			loc4[2] = y;
			loc4[3] = maxc5;
			loc4[4] = m;
			loc5 = m + maxc5;
		}
	}
	}
	printf("%f %f ", maxc0, loc[0]);
	printf("%f %f %f ", maxc1, loc2, loc1);
	printf("%f %f ", maxc4, loc3[1]);
	printf("%f %f %f ", maxc5, loc4[1], loc5);
	return 0;
}
//+T pragma PRG REDUCTION(MAXLOC(maxc0, loc, 5))
//+TR pragma PRG REDUCTION(MAXLOC(maxc1, .*, 2))
//+T pragma PRG REDUCTION(MAXLOC(maxc4, loc3, 5))
//+TR pragma PRG REDUCTION(MAXLOC(maxc5, .*, 6))
//+X This one is (probably) transformed and output equality should be checked
