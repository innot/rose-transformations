#include <stdio.h>

double f(double x)
{
	return x*3.145;
}

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 3, yt = 6;
	double maxc0 = c[0] + 1;
	double maxc1 = 2*c[0];
	double maxc2 = c[0] + f(c[0]);
	double maxc3 = c[0] + y;
	double maxc4 = c[0] + c[1];
	double maxc5 = c[0] + y*c[0];
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		if (c[0] > maxc0)
			maxc0 = c[i];
	for (int i = 0; i < m; i++)
		if (c[i] + 1 > maxc1)
			maxc1 = c[i];
	for (int i = 0; i < m; i++)
		if (c[i] + 1 > maxc2)
			maxc1 = c[i] + 1;
	for (int i = 0; i < m; i++)
		if (c[i] + 2*y++ > maxc3)
			maxc3 = c[i] + 2*y++;
	for (int i = 1; i < m; i++)
		if (c[i - 1] > maxc4)
			maxc4 = c[i];
	}
	return 0;
}
//+F pragma PRG REDUCTION(MAX(maxc0))
//+F pragma PRG REDUCTION(MAX(maxc1))
//+F pragma PRG REDUCTION(MAX(maxc2))
//+F pragma PRG REDUCTION(MAX(maxc3))
//+F pragma PRG REDUCTION(MAX(maxc4))
