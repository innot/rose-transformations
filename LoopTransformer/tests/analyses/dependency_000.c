#include <stdio.h>

// Single-sized subscripts, ZIV
int main()
{
	const unsigned m = 10;
	const unsigned n = 10;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4,5,6,7,8,9,0};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, total = 0, t = 0;
	const int N = 1;
	const int M = 2;
	const int X = 5;
	#pragma a dependency
	{
	for (int i = 0; i < m; i++) {
		c[M+N] = c[M+N] - c[M];
	}
	for (int i = 0; i < m; i++) {
		c[5] = c[5] + c[6] + c[X];
	}
	for (int i = 0; i < m; i++) {
		y += 1;
	}
	for (int i = 0; i < m; i++) {
		c[M+N] = c[M+N] - c[M];
		c[5] = c[5] + c[6];
	}
	for (int i = 0; i < m-1; i++) {
		c[M+N] = c[5] - c[M];
		c[5] = c[M+N] + c[6] - y;
		y = y + 1;
	}
	}
	return 0;
}

// TODO: does y += 1 reduction mean loop-independent dependency?
//+TL #.*@ 40.*@ 40.*SELF #
// M_N != M should be detected.
//+FL #.*@ 40.*@ 40.*dir \\* #
//+TL #.*@ 43.*@ 43.*SELF #
// 5 == X
//+TL #.*@ 43.*@ 43.*SELF #
// 5 != 6 should be detected.
//+FL #.*@ 43.*@ 43.*dir \\* #
//+TL #.*@ 46.*@ 46.*SELF #
//+TL #.*@ 49.*@ 49.*SELF #
//+FL #.*@ 49.*@ 49.*dir \\* #
//+TL #.*@ 50.*@ 50.*SELF #
//+FL #.*@ 50.*@ 50.*dir \\* #
//+FL #.*@ 53.*@ 53.*dir \\* #
// TODO order?
//+TL #.*@ 53.*@ 54.*ANTI #
//+TL #.*@ 53.*@ 54.*FLOW #
//+FL #.*@ 53.*@ 54.*dir \\* #
