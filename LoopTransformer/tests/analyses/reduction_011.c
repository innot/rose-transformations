#include <stdio.h>

double f(double x)
{
	return x*3.145;
}

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	unsigned ct[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 3, yt = 6;
	double maxc0 = c[0] + 1;
	double maxc1 = 2*c[0];
	double maxc2 = c[0] + f(c[0]);
	double maxc3 = c[0] + y;
	double maxc4 = c[0] + c[1];
	double maxc5 = c[0] + y*c[0];
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		if (c[i] + 1 > maxc0)
			maxc0 = c[i] + 1;
	for (int i = 0; i < m; i++)
		if (2*c[i] > maxc1)
			maxc1 = 2*c[i];
	for (int i = 0; i < m; i++)
		if (c[i] + f(c[i]) > maxc2)
			maxc2 = c[i] + f(c[i]);
	for (int i = 0; i < m; i++)
		if (c[i] + y > maxc3)
			maxc3 = c[i] + y;
	for (int i = 0; i < m; i++)
		if (c[i] + c[1] > maxc4)
			maxc4 = c[i] + c[1];
	for (int i = 0; i < m; i++)
		if (c[i] + y*c[i] > maxc5)
			maxc5 = c[i] + y*c[i];
	}
	return 0;
}
//+T pragma PRG REDUCTION(MAX(maxc0))
//+T pragma PRG REDUCTION(MAX(maxc1))
//+T pragma PRG REDUCTION(MAX(maxc2))
//+T pragma PRG REDUCTION(MAX(maxc3))
//+T pragma PRG REDUCTION(MAX(maxc4))
//+T pragma PRG REDUCTION(MAX(maxc5))
