#include <stdio.h>

int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4};
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0;
	double total = 0;
	double t0 = 0;
	double t1 = 0;
	double t2 = 0;
	double t3 = 0;
	double t4 = 0;
	double t5 = 0;
	double t6 = 0;
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		t0 += 1;
	for (int i = 0; i < m; i++)
		t1 += i;
	for (int i = 0; i < m; i++)
		t2 += i*i + y;
	for (int i = 0; i < m; i++)
		t3 += c[i];
	for (int i = 0; i < m; i++)
		t4 += c[i] + a[i][0];
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			t5 += a[i][j];
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			t6 += a[i][j] * b[i][j];
	}
	return 0;
}
//+T pragma PRG REDUCTION(SUM(t0))
//+T pragma PRG REDUCTION(SUM(t1))
//+T pragma PRG REDUCTION(SUM(t2))
//+T pragma PRG REDUCTION(SUM(t3))
//+T pragma PRG REDUCTION(SUM(t4))
//+T pragma PRG REDUCTION(SUM(t5))
//+T pragma PRG REDUCTION(SUM(t6))
