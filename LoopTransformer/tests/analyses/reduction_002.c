#include <stdio.h>

int main()
{
	const unsigned m = 4;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0;
	double total = 0, t0 = 0, t1 = 0, t2 = 0;
	#pragma a reduction
	{
	for (int i = 0; i < m; i++)
		total++;
	for (int i = 0; i < m; i++)
		t0--;
	for (int i = 0; i < m; i++)
		++t1;
	for (int i = 0; i < m; i++)
		--t2;
	}
	return 0;
}
//+T pragma PRG REDUCTION(SUM(total))
//+T pragma PRG REDUCTION(SUM(t1))
//+T pragma PRG REDUCTION(DIFF(t0))
//+T pragma PRG REDUCTION(DIFF(t2))
