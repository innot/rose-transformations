#include <stdio.h>

// Single-sized subscripts, Strong SIV
int main()
{
	const unsigned m = 4;
	const unsigned n = 4;
	double a[m][n];
	for (unsigned i = 0; i < m; i++)
		for (unsigned j = 0; j < m; j++)
			a[i][j] = i*j*3 - i + j*2 + 1;


	double b[4][4] = {
		{34,34,675,897},
		{45,34,672,234},
		{45,56,9234,54},
		{-12,34,672,234},
	};
	double c[] = {1,2,3,4,5,6,6,7,8};
	int cc[] = {1,2,3,4,5,6,6,7,8};
	const unsigned bk = 3;
	double bc[bk][bk];
	double bs[bk][bk];
	for (unsigned i = 0; i < bk; i++)
		for (unsigned j = 0; j < bk; j++) {
			bc[i][j] = i*j*5 - j + i*2 + 1;
			bc[i][j] = i*j*5 - j - i*5 + 2;
		}


	int j;
	double vol = 0.1, vol0 = 0.1;
	unsigned y = 0, total = 0, t = 0;
	const int N = 1;
	const int M = 2;
	#pragma a dependency
	{
	for (int i = 0; i < m; i++)
		c[i] += c[i];
	for (int i = 0; i < m; i++)
		c[i] = c[i] + 5;
	for (int i = 0; i < m; i++)
		c[i + 1] = c[i] + 5;
	for (int i = 0; i < m; i++)
		c[i + 800] = c[i] + 5;
	for (int i = 0; i < m; i++)
		c[i] = c[i-1] - a[i][0];
	for (int i = 0; i < m; i++)
		c[2*i] = c[2*i];
	for (int i = 0; i < m; i++)
		c[2*i-1] = c[2*i-1];
	for (int i = 0; i < m; i++)
		c[3*i-1] = c[3*i-2];
	for (int i = 0; i < m; i++)
		c[2*i+3] = c[2*i-1];
	for (int i = 0; i < m; i++)
		c[2*N*i+3] = c[2*N*i-1];
	for (int i = 0; i < m; i++)
		c[M*i+cc[i]] = c[M*i-1];
	}
	return 0;
}
//+FL #.*@ 40.*@ 40.* #
//+FL #.*@ 42.*@ 42.* #
//+TL #.*@ 44.*@ 44.*dist 1 #
//+FL #.*@ 46.*@ 46.* #
//+TL #.*@ 48.*@ 48.*dist -1 #
//+FL #.*@ 50.*@ 50.* #
//+FL #.*@ 52.*@ 52.* #
//+FL #.*@ 54.*@ 54.* #
//+TL #.*@ 56.*@ 56.*dist 1 #
//+TL #.*@ 58.*@ 58.*dist 1 #
//+TL #.*@ 60.*@ 60.*dir \\* #
