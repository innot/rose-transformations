## Testing transformations. ##\r
Test|bringout|unroll|propagate|inline|swap|merge|index|nontemp|split|permut|id|distribute|map|Status|Warnings|Errors
----|-|-|-|-|-|-|-|-|-|-|-|-|-|------|--------|------
test_000.c| | |p| | | | | | | | | | |[PASSED]|W|
test_001.c| | |p| | | | | | | | | | |[PASSED]||
test_002.c| | |p| | | | | | | | | | |[PASSED]|W|
test_003.c| | |p| | | | | | | | | | |[PASSED]|W|
test_004.c| | |p| | | | | | | | | | |[PASSED]||
test_005.c| | |p| | | | | | | | | | |[PASSED]||
test_006.c| | |p| | | | | | | | | | |[PASSED]||
test_007.c| | |p| | | | | | | | | | |[PASSED]||
test_008.c| | |p| | | | | | | | | | |[PASSED]|W|
test_009.c| | |p| | | | | | | | | | |[PASSED]||
test_010.c| | |p| | | | | | | | | | |[PASSED]|W|
test_011.c| | |p| | | | | | | | | | |[PASSED]|W|
test_012.c| | |p| | | | | | | | | | |[PASSED]||
test_013.c| | |p| | | | | | | | | | |[PASSED]|W|
test_014.c| | |p| | | | | | | | | | |[PASSED]|W|
test_015.c| | |p| | | | | | | | | | |[PASSED]|W|
test_016.c| | |p| | | | | | | | | | |[PASSED]|W|
test_017.c| | |p| | | | | | | | | | |[PASSED]||
test_018.c| | |p| | | | | | | | | | |[PASSED]|W|
test_019.c| | |p| | | | | | | | | | |[PASSED]|W|
test_020.c| |u|p| | | | | | | | | | |[PASSED]|W|
test_021.c| |u|p| | | | | | | | | | |[PASSED]|W|
test_022.c| |u|p| | | | | | | | | | |[PASSED]|W|
test_023.c| |u|p| | | | | | | | | | |[PASSED]|W|
test_024.c| | |p| | | | | | | | | | |[PASSED]|W|
test_025.c| |u|p| | | | | | | | | | |[PASSED]|W|E
test_026.c| | |p| | | | | | | | | | |[PASSED]|W|
test_027.c|b| |p| | | | | | | | | | |[PASSED]|W|
test_028.c|b| |p| | | | | | | | | | |[PASSED]|W|E
test_029.c|b| |p| | | | | | | | | | |[PASSED]|W|
test_030.c| | |p| | | | | | | | | | |[PASSED]|W|
test_031.c| | |p| | | | | | | | | | |[PASSED]|W|
test_032.c| | |p| | | | | | | | | | |[PASSED]|W|
test_033.c| | |p| | | | | | | | | | |[PASSED]|W|
test_034.c| | |p| | | | | | | | | |m|[PASSED]||
test_035.c| | |p| | | | | | | | | |m|[PASSED]||
test_036.c| |u|p| | | | | | | | | |m|[PASSED]|W|
test_037.c|b|u|p| | | | | | | | | | |[PASSED]||E
test_038.c|b|u|p| | | | | | | | | | |[PASSED]||E
test_039.c|b|u|p| | | | | | | | | | |[PASSED]|W|E
test_040.c|b|u|p| | | | | | | | | | |[PASSED]|W|E
test_041.c|b|u|p| | | | | | | | | | |[PASSED]||
test_042.c|b|u|p| | | | | | | | | | |[PASSED]||E
test_043.c|b|u|p| | | | | | | | | | |[PASSED]||E
test_044.c| |u|p| | | | | | | | | | |[PASSED]|W|
test_045.c| | |p| | | | | | | | | | |[PASSED]|W|
test_046.c| | |p| | | | | | | | | | |[PASSED]|W|
test_047.c| | |p| | | | | | | | | | |[PASSED]||
test_048.c| | |p| | | | | | | | | | |[PASSED]||
test_049.c| | |p| | | | | | | | | | |[PASSED]|W|
test_050.c| | |p| | | | | | | | | | |[PASSED]|W|
test_051.c| |u|p| | | | | | | | | | |__[FAILED]__||
test_052.c| |u|p| | | | | | | | | | |[PASSED]||
test_053.c|b|u|p| | | | | | | | | | |[PASSED]||E
test_054.c|b| |p| | | | | | | | | | |[PASSED]||
test_055.c|b| |p| | | | | | | | | | |[PASSED]|W|E
test_056.c|b| | | | | | | | | | | | |[PASSED]||E
test_057.c|b| | | | | | | | | | | | |[PASSED]||E
test_058.c|b| | | | | | | | | | | | |[PASSED]||E
test_059.c|b| |p| | | | | | | | | | |[PASSED]||E
test_060.c| | |p| | | | | | | | | | |[PASSED]|W|
test_061.c| |u|p| | | | | |s| | | | |__[FAILED]__||
test_062.c| |u|p| | | | | | | | | | |[PASSED]||E
test_063.c| |u|p| | | | | |s| | | | |[PASSED]||
test_064.c| |u| | | | | | |s| | | | |[PASSED]||
test_065.c| |u| | | | | | |s| | | | |__[ABORTED]__||
test_066.c| |u| | | | | | |s| | | | |[PASSED]||
test_067.c| |u| | | | | | |s| | | | |[PASSED]||E
test_068.c| |u| | | | | | | | | | | |[PASSED]||E
test_069.c| |u|p| | | | | | | | | | |[PASSED]||E
test_070.c| |u|p| | | | | | | | | | |[PASSED]||E
test_071.c| | | | | | |x| | |P| | | |[PASSED]||
test_072.c|b| | | | | | | | | | | | |[PASSED]||E
test_073.c| |u| | | | | | | | | | | |[PASSED]||
test_074.c| |u| | | | | | | | | | | |[PASSED]||
test_075.c| |u| | | | | | | | | | | |[PASSED]||
test_076.c| |u| | | | | | | | | | | |[PASSED]||
test_077.c| |u| | | | | | | | | | | |[PASSED]||
test_078.c| |u| | | | | | | | | | | |[PASSED]||
test_079.c| |u| | | | | | | | | | | |[PASSED]||
test_080.c| |u| | | | | | | | | | | |[PASSED]||
test_081.c| | |p| | | | | | | | | |m|[PASSED]||
test_082.c| |u| | | | | | | | | | | |[PASSED]||
test_083.c| |u| | | | | | | | | | | |[PASSED]||
test_084.c| | | | | | | | | |P| | | |[PASSED]||
test_085.c| |u| | | | | | | | | | | |[PASSED]||E
test_086.c| |u|p| | | | | | | | | |m|[PASSED]|W|
test_087.c| |u|p| | | | | | | | | |m|[PASSED]|W|
test_088.c| | |p| | | | | | | | | |m|__[FAILED]__||
test_089.c| |u|p| | | | | | | | | | |[PASSED]||
test_090.c| |u|p| | | | | | | | | |m|__[FAILED]__||
test_091.c| | | | | | | | |s| | | | |[PASSED]||E
test_092.c|b| | | | | | | |s| | | | |[PASSED]||
test_093.c| | | | | | | | |s| | | | |[PASSED]||
test_094.c| |u|p| | | | | | | | | |m|[PASSED]||E
test_095.c| | | | | | | | | |P| | | |[PASSED]||E
test_096.c| | | | | | | | | |P| | | |[PASSED]||
test_097.c| | | | | | | | | |P| | | |[PASSED]||
test_098.c| | | | | | |x| | |P| | | |[PASSED]||
test_099.c| | | | | | |x| | |P| | | |[PASSED]||
test_100.c| | | | | | |x| | | | | | |[PASSED]||
test_101.c| | | | | | |x| | | | | | |[PASSED]||
test_102.c| | | | | | |x| | | | | | |__[FAILED]__||
test_103.c| | | | |S| | | | | |i| | |[PASSED]||
test_104.c| | | | |S| | | | | |i| | |[PASSED]||
test_105.c| | | | |S| | | | | |i| | |[PASSED]||E
test_106.c| | | | |S| | | | | |i| | |[PASSED]||E
test_107.c| | | | |S| | | | | |i| | |[PASSED]||E
test_108.c| | | | |S| | | | | |i| | |[PASSED]||E
test_109.c| | | | |S| | | | | |i| | |[PASSED]||E
test_110.c| | | | | |M| | | | |i| | |__[FAILED]__||
test_111.c| | | | | |M| | | | |i| | |[PASSED]||
test_112.c| | | | | |M| | | | |i| | |[PASSED]||
test_113.c| | | | | |M| | | | |i| | |__[FAILED]__||
test_114.c| | | | | |M| | | | |i| | |[PASSED]||
test_115.c| | | | | |M| | | | |i| | |[PASSED]||
test_116.c| | | | | |M| | | | |i| | |[PASSED]||
test_117.c| | | | |S|M| | | | |i| | |[PASSED]||
test_118.c| | | | |S|M| | | | |i| | |[PASSED]||
test_119.c| | |p| | | | | | | | | | |[PASSED]|W|
test_120.c| |u| | | | | | | | | | | |[PASSED]||
test_121.c| |u| | | | | | | | | | | |[PASSED]||
test_122.c| |u| | | | | | | | | | | |[PASSED]||
test_123.c| |u| | | | | | | | | | | |[PASSED]||
test_124.c| | |p| | | | | | | | | | |[PASSED]|W|
test_125.c| | | | | |M| |n| | |i| | |[PASSED]||
test_126.c| |u|p| | | | | |s| | | | |__[FAILED]__||
test_127.c| | |p| | | | | | | | | | |[PASSED]|W|
test_128.c| | | | | |M| |n| | |i| | |[PASSED]||
test_129.c| | |p| | | | | | | | | | |[PASSED]|W|
test_130.c| | |p| | | | | | | | | | |[PASSED]|W|
test_131.c|b| |p| | | | | | | | | | |[PASSED]|W|E
test_132.c| | |p| | | | | | | | | | |[PASSED]|W|
test_133.c| | |p| | | | | | | | | | |[PASSED]|W|
test_134.c| | |p| | | | | | | | | | |[PASSED]|W|
test_135.c| | |p| | | | | | | | | |m|[PASSED]||
test_136.c| | | | | |M| | | | |i| | |__[FAILED]__||
test_137.c| | |p| | | | | | | | | | |[PASSED]||
test_138.c| | |p| | | | | | | | | | |[PASSED]||
test_139.c|b| | | | | | | | | | | | |[PASSED]||
test_140.c|b| | | | | | | | | | | | |[PASSED]||
test_141.c|b| |p| | | | | | | | | | |[PASSED]||
test_142.c|b| | | | | | | | | | | | |[PASSED]||
test_143.c|b|u| | | | | | | | | | | |[PASSED]||E
test_144.c|b| |p| | | | | | | | | | |[PASSED]||
test_145.c| | |p| | | | | | | | | | |[PASSED]||
test_146.c| | | | | | | | | | |i| | |[PASSED]||
test_147.c|b| | | | | | | | | |i| | |[PASSED]||
test_148.c| | |p| | | | | | | | | | |[PASSED]|W|
test_149.c| | |p| | | | | | | | | | |[PASSED]|W|
test_150.c| | |p| | | | | | | | | | |[PASSED]|W|
test_151.c| |u|p| | | | | | | | | | |[PASSED]||
test_152.c| | | | | | | | |s| | | | |[PASSED]||E
test_153.c| | |p| | | | | | | | | | |[PASSED]||
test_154.c| | |p| | | | | | | | | | |[PASSED]|W|
test_155.c| | | | | | | | |s| | | | |[PASSED]||
test_156.c| | | | | | | | |s| | | | |[PASSED]|W|E
test_157.c| |u| | | | | | | | | | | |[PASSED]|W|
test_158.c| | |p| | | | | | | | | | |[PASSED]||
test_159.c| | | | | | | | | | | | | |[PASSED]||
test_160.c|b| | | | | | | | | | | | |[PASSED]||
test_161.c|b| | | | | | | | | | | | |[PASSED]||
test_162.c| | | | |S| | |n| | |i| | |[PASSED]|W|
test_163.c| | | |I| | | | | | | | | |__[ABORTED]__||
test_164.c| | | |I| | | | | | |i| | |[PASSED]|W|
test_165.c| | |p|I| | | | | | | | | |[PASSED]|W|
test_166.c| | | |I| | | | | | | | | |[PASSED]||
test_167.c| | |p|I| | | | | | | | | |__[FAILED]__||
test_168.c| | | | |S| | | | | |i| | |[PASSED]|W|
test_169.c| | |p| | | | | | | | | | |[PASSED]|W|
test_170.c| | | |I| | | | | | |i| | |__[ABORTED]__||
test_171.c| | | |I| | | | | | |i| | |[PASSED]||
test_172.c| | | |I| | | | | | |i| | |[PASSED]||
test_173.c| | | |I| | | | | | |i| | |__[FAILED]__|W|
test_174.c| |u| | | | | | | | | | | |[PASSED]||
test_175.c| |u|p| | | | | | | | | |m|__[FAILED]__|W|
test_176.c| | | |I| | | | | | |i| | |[PASSED]||
test_177.c| | | |I| | | | | | |i| | |[PASSED]||
test_178.c| | |p| | | | | | | | | | |__[ABORTED]__||
test_179.c| | | |I| | | | | | |i| | |__[FAILED]__||
test_180.c| | | |I| | | | | | |i| | |__[FAILED]__||
test_181.c| | | | | | | | | | | |d| |[PASSED]||
test_182.c| | | | | | | | | | | |d| |[PASSED]||
test_183.c| | | | | | | | |s| | | | |__[FAILED]__||
test_184.c| | | | | | | | | | | |d| |[PASSED]||
test_185.c| | | | | | | | | | | |d| |[PASSED]||
test_186.c| | | | | | | | | | | | | |[PASSED]||

=============RESULTS=============

PASSED: 168 / 187 

TIMED OUT: 0 

ABORTED: 4 

WITH ERRORS: 33 

WITH WARNINGS: 60 

## Testing analyses. ##\r
Test|independent|reduction|Status|Warnings|Errors
----|-|-|------|--------|------
reduction_000.c| |r|[3/3]||
reduction_001.c| |r|[2/2]||
reduction_002.c| |r|[4/4]||
reduction_003.c| |r|[7/7]||
reduction_004.c| |r|[6/6]||
reduction_005.c| |r|__[5/7]__||
reduction_006.c| |r|[7/7]||
reduction_007.c| |r|[8/8]||
reduction_008.c| |r|[7/7]||
reduction_009.c| |r|[5/5]||
reduction_010.c| |r|__[3/4]__||
reduction_011.c| |r|__[5/6]__||
reduction_012.c| |r|[5/5]||
reduction_013.c| |r|[10/10]||
reduction_014.c| |r|__[8/11]__||
reduction_015.c| |r|[15/15]||
reduction_016.c| |r|__[0/3]__||
reduction_017.c| |r|[4/4]||

=============RESULTS=============

PASSED: 104 / 132 

TIMED OUT: 0 

ABORTED: 0 

WITH ERRORS: 0 

WITH WARNINGS: 0 

