#!/usr/bin/env bash
exec 2> /dev/null


test=$1;

grep '^//+' $test > task_$test;
grep -v '^//+' $test > filtered_$test;

test_path="filtered_$test";
translator_path="../../translator";
translator_params = "--edg:no_warnings";
log_path="$test.log"
old_res="$test.old.log"
new_res="$test.new.log"

cleanup() {
	rm ${log_path};
	rm ${test}.o;
	rm rose_${test_path}.o;
	rm rose_${test_path};
	rm task_${test};
	rm ${test_path};
	rm -f ${rose_test}.o;
	rm -f ${old_res};
	rm -f ${new_res};
}

echo -n $test;

gcc -std=c99 -g -o $test.o $test_path &> /dev/null;
cc_code=$?
if [ $cc_code -ne 0 ]; then
	echo -e "\t\033[31m[BAD TEST]\033[0m";
	cleanup;
	exit;
fi;

$($translator_path $translator_params $test_path &> $log_path);
tr_code=$?
if [[ $tr_code != 0 && $tr_code != 23 ]]; then # 23 means transformation error
	echo -e "\t\033[35m[ABORTED WITH CODE=$tr_code]\033[0m";
	cleanup;
	exit;
fi;

if [ "$(grep '^//+X' task_$test)" != "" ]; then
	# transformations may have occured
	# check results equivalence
	timeout 1 ./$test.o > $old_res;
	orig_code=$?
	if [ $orig_code -eq 124 ]; then
		echo -e "\t\033[36m[TIMED OUT / TEST]\033[0m";
		cleanup;
		exit;
	elif [ $orig_code -ne 0 ]; then
		echo -e "\t\033[31m[FAILED]\033[0m";
		cleanup;
		exit;
	fi;
	gcc -std=c99 -g -o rose_${test_path}.o rose_${test_path} &>> $log_path;
	timeout 1 ./rose_${test_path}.o > $new_res 2> /dev/null;
	res_code=$?
	if [ $res_code -eq 124 ]; then
		echo -e "\t\033[36m[TIMED OUT / RESULT]\033[0m";
		cleanup;
		exit;
	elif [ $res_code -ne 0 ]; then
		echo -e "\t\033[31m[FAILED]\033[0m";
		cleanup;
		exit;
	fi;
	diff -up $old_res $new_res
	if [ "$(diff -up $old_res $new_res)" != "" ]; then
		echo -en "\t\033[32m[FAILED]\033[0m";
		cleanup;
		exit;
	fi;
fi;

total=0;
passed=0;
while read tst; do
	t='undefined';
	if [ "$(echo $tst | grep '^//+T')" != "" ]; then
		t='match';
	elif [ "$(echo $tst | grep '^//+F')" != "" ]; then
		t='not match';
	fi;
	if [ "$t" == "undefined" ]; then
		continue;
	fi;

	(( total = total + 1 ));

	if [ "$(echo $tst | grep '^//+.R')" != "" ]; then
		grep_mode='';
	else
		grep_mode='-F';
	fi;

	if [ "$(echo $tst | grep '^//+.L')" != "" ]; then
		grep_mode='';
		scan_log=1;
		target=$log_path
	else
		target=rose_${test_path}
	fi;

	tst=$(echo $tst | sed "s/\/\/+[^ ]* \(.*\)/\1/");
	if [ "$(grep $grep_mode "$tst" $target)" == "" ]; then
		if [ "$t" == "not match" ]; then
			(( passed = $passed + 1 ))
		fi;
	else
		if [ "$t" == "match" ]; then
			(( passed = $passed + 1 ))
		fi;
	fi;
done < task_$test;
cleanup;

echo -en "\t[$passed/$total]";

if (( total == passed )); then
	echo -en "\t\033[32m[PASSED]\033[0m";
else
	echo -en "\t\033[31m[FAILED]\033[0m";
fi;
if [ "x$(grep WARNING $log_path)" != "x" ]; then
	echo -en "\t\033[33m[W]\033[0m";
else
	echo -en "\t   ";
fi;
if [ "x$(grep ERROR $log_path)" != "x" ]; then
	echo -en "\033[31m[E]\033[0m";
fi;
echo;

