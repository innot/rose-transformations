BEGIN {
	total = 0;
	passed = 0;
	timeout = 0;
	aborted = 0;
	errors = 0;
	warnings = 0;
}
{ total++; }
/\[.*\/.*\]/ {
	stats_s = substr($2, 2, length($2) - 2);
	split(stats_s, stats, "/");
	passed += stats[1];
	total += stats[2];
}
/TIMED OUT/ { timeout++; }
/ABORTED/ { aborted++; }
/\[E\]/ { errors++; }
/\[W\]/ { warnings++; }
END {
	print ""
	print "=============RESULTS=============\n"
	print "PASSED:", passed, "/", total,"\n";
	print "TIMED OUT:", timeout,"\n";
	print "ABORTED:", aborted,"\n";
	print "WITH ERRORS:", errors,"\n";
	print "WITH WARNINGS:", warnings,"\n";
}
