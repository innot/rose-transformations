#define BOOST_RESULT_OF_USE_DECLTYPE
#include "ReductionAnalysis.h"
#include "CodeAnalyser.h"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <utility>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/adaptors.hpp>
#include "sage3basic.h"
#include "expressionTreeEqual.h"
#include "VariableIdMapping.h"
#include "util.h"
#include "LoopSplitter.h"

namespace CodeAnalyses {

using namespace SageInterface;
using namespace LoopTransformations;
using namespace TfUtils;

ReductionAnalysis::ReductionAnalysis(SgForStatement* loop)
	: loop(loop), rd(getEnclosingProcedure(loop), false)
{
}

namespace {
/// First checks whether s contains more than one non-(assignment|declaration).
/// Then returns the most complex one (or nullptr if they are equally simple).
///
/// \todo We should be able to propagate array elements, despite assignments
/// to them can be reductions.
auto leave_the_only_nontrivial = [] (auto& s) -> SgStatement* {
	SgStatement* nontriv = nullptr;
	for (auto stmt : s) {
		SgInitializedName* var;
		SgExpression* initializer;
		if (isPureAssignment(stmt, &var, &initializer)) {
			// If statement is/has a modifier,
			// it can be a reduction;
			// If an array element is assigned, it can
			// also be a reduction;
			// Otherwise it's trivial.
			auto bo = isSgBinaryOp(isSgExprStatement(stmt)
							->get_expression());
			assert(bo);
			auto arrays = querySubTree<SgPntrArrRefExp>(
						bo->get_lhs_operand_i()).size();
			auto modifier = expr_contains_any_of_vars(initializer,
									var);
			SV t;
			t.push_back(stmt);
			if (!arrays && !modifier
				&& get_vars_modified_in_statements(t).empty())
				continue;
		} else if (isSgVariableDeclaration(stmt)) {
			continue;
		}
		if (nontriv)
			return nullptr;
		nontriv = stmt;
	}
	return nontriv;
};
} // namespace

/// Dependency checking for reduction detection.
///
/// A statement in loop body may define a reduction only if it
/// would make up a single new loop after loop distribution
/// (i.e. it doesn't depend on other statements).
///
/// We allow to predefine some variables in the same loop,
/// so there may actually be more than one statement, but
/// other ones should be assignments or definitions.
SV ReductionAnalysis::detect_reduction_possibilities(SgForStatement* loop)
{
	using namespace boost::adaptors;
	SV result;
	boost::copy(get_dependency_sets(loop)
			| transformed(leave_the_only_nontrivial)
			| filtered([] (auto stmt) { return stmt != nullptr; }),
		std::back_inserter(result));
	return result;
}

namespace {

bool supported_reduction_type(VariantT type)
{
	switch (type) {
	case V_SgPlusPlusOp:
	case V_SgPlusAssignOp:
	case V_SgAddOp:
	case V_SgMinusMinusOp:
	case V_SgMinusAssignOp:
	case V_SgSubtractOp:
	case V_SgMultAssignOp:
	case V_SgMultiplyOp:
	case V_SgDivAssignOp:
	case V_SgDivideOp:
	case V_SgXorAssignOp:
	case V_SgBitXorOp:
	case V_SgAndAssignOp:
	case V_SgBitAndOp:
	case V_SgIorAssignOp:
	case V_SgBitOrOp:
	case V_SgAndOp:
	case V_SgOrOp:
	case V_SgGreaterThanOp:
	case V_SgLessThanOp:
	case V_SgEqualityOp:
	case V_SgNotEqualOp:
	case V_SgGreaterOrEqualOp:
	case V_SgLessOrEqualOp:
		return true;
	default:
		return false;
	}
}

const char* red_type_to_string(VariantT type)
{
	switch (type) {
	case V_SgPlusPlusOp:
	case V_SgPlusAssignOp:
	case V_SgAddOp:
		return "SUM";
	case V_SgMinusMinusOp:
	case V_SgMinusAssignOp:
	case V_SgSubtractOp:
		return "DIFF";
	case V_SgMultAssignOp:
	case V_SgMultiplyOp:
		return "PRODUCT";
	case V_SgDivAssignOp:
	case V_SgDivideOp:
		return "RATIO";
	case V_SgXorAssignOp:
	case V_SgBitXorOp:
		return "XOR";
	case V_SgAndAssignOp:
	case V_SgBitAndOp:
		return "BINAND";
	case V_SgIorAssignOp:
	case V_SgBitOrOp:
		return "BINOR";
	case V_SgAndOp:
		return "AND";
	case V_SgOrOp:
		return "OR";
	case V_SgGreaterThanOp:
		return "MAX";
	case V_SgLessThanOp:
		return "MIN";
	case V_SgEqualityOp:
		return "EQV";
	case V_SgNotEqualOp:
		return "NEQV";
	case V_SgGreaterOrEqualOp:
		return "MAXLOC";
	case V_SgLessOrEqualOp:
		return "MINLOC";
	default:
		assert(!"Unsupported construct.");
		return "";
	}
}

class ReductionAnalysisResult {
	std::vector<Reduction> result;
public:
	void push_back(Reduction&& r)
	{
		result.push_back(std::move(r));
	}
	std::unique_ptr<SgPragmaDeclaration> to_pragma()
	{
		boost::remove_erase_if(result, [] (auto &r) {
			return !r.valid();
		});
		if (result.empty())
			return std::unique_ptr<SgPragmaDeclaration>();
		std::stringstream s;
		s << "PRG REDUCTION(";
		for (auto it = result.begin(); it != result.end(); it++) {
			if (it != result.begin())
				s << ", ";
			s << it->to_string();
		}
		s << ")";
		return std::unique_ptr<SgPragmaDeclaration>(
			SageBuilder::buildPragmaDeclaration(s.str()));
	}
};

template<bool modifier>
VariantT is_reduction_op(VariantT type);

// Accepts { +, *, -, /, &, ^, | }=.
template<>
VariantT is_reduction_op<true>(VariantT type)
{
	switch (type) {
	case V_SgPlusAssignOp:
	case V_SgMultAssignOp:
	case V_SgMinusAssignOp:
	case V_SgDivAssignOp:
	case V_SgAndAssignOp:
	case V_SgXorAssignOp:
	case V_SgIorAssignOp:
		return type;
	default:
		return V_SgTypeUnknown;
	}
}

// Accepts { +, *, -, /, &, ^, |, &&, || }.
template<>
VariantT is_reduction_op<false>(VariantT type)
{
	switch (type) {
	case V_SgAddOp:
	case V_SgMultiplyOp:
	case V_SgSubtractOp:
	case V_SgDivideOp:
	case V_SgBitAndOp:
	case V_SgBitXorOp:
	case V_SgBitOrOp:
	case V_SgAndOp:
	case V_SgOrOp:
		return type;
	default:
		return V_SgTypeUnknown;
	}
}

/// Given a comparison expression,
/// probably removes negation and rewrites the comparison so
/// that it would be >, then writes lhs and rhs to corresponding parameters.
bool normalize_condition(SgExpression* e, SgExpression*& lhs, SgExpression*& rhs)
{
	bool negation = false;
	if (auto no = isSgNotOp(e)) {
		e = no->get_operand();
		negation = true;
	}
	auto bo = isSgBinaryOp(e);
	if (!bo)
		return false;
	lhs = bo->get_lhs_operand_i();
	rhs = bo->get_rhs_operand_i();
	switch (bo->variantT()) {
	case V_SgGreaterThanOp:
	case V_SgGreaterOrEqualOp:
		break;
	case V_SgLessThanOp:
	case V_SgLessOrEqualOp:
		std::swap(rhs, lhs);
		break;
	default:
		return false;
	}
	if (negation)
		std::swap(rhs, lhs);
	return true;
}

} // namespace

bool Reduction::valid()
{
	return name && supported_reduction_type(type);
}

std::string Reduction::to_string()
{
	assert(valid());
	std::stringstream s;
	s << red_type_to_string(type);
	s << "(" << get_var_name(name);
	if (array) {
		s << ", " << get_var_name(array);
		s << ", " << array_length;
	}
	s << ")";
	return s.str();
}

/// Given a comparizon expression, determine whether max or min is seeked
/// and determine the reduction variable and the corresponding expression.
/// Return false on detection errors.
/// \todo may the accumulator be not a variable, but something else?
bool ReductionAnalysis::determine_xrd_vars(SgExpression* e, bool& max,
			SgInitializedName*& acc, SgExpression*& arr)
{
	SgExpression* lhs = nullptr;
	SgExpression* rhs = nullptr;
	if (!normalize_condition(e, lhs, rhs))
		return false;
	assert(lhs && rhs);

	if (isSgVarRefExp(rhs) && isSgVarRefExp(lhs)) {
		// if two vars are compared, try to get reaching definitions
		// to check whether one of them is assigned an
		// array element before
		rd.generate();
		auto enc_s = getEnclosingNode<SgStatement>(e);
		ES refs;
		refs.insert(lhs);
		refs.insert(rhs);
		reaching_defs d;
		rd.get_definitions(enc_s, refs, d);
		if (d.size() != 1)
			return false;
		auto e = *(d.begin());
		if (e.first == lhs)
			lhs = e.second.first;
		else if (e.first == rhs)
			rhs = e.second.first;
		else
			assert(false);
	}
	if (isSgVarRefExp(rhs)) {
		max = true;
		acc = get_var_initialized_name(rhs);
		arr = lhs;
	} else if (isSgVarRefExp(lhs)) {
		max = false;
		acc = get_var_initialized_name(lhs);
		arr = rhs;
	} else {
		return false;
	}
	return true;
}

/// Detects min/max reduction provided as an assignment of C ternary operator.
Reduction ReductionAnalysis::detect_cond_min_max(SgInitializedName* acc, SgConditionalExp* cond)
{
	Reduction result;
	auto c = cond->get_conditional_exp();
	auto t = cond->get_true_exp();
	auto f = cond->get_false_exp();
	auto gt = true;
	SgInitializedName* ac = nullptr;
	SgExpression* arr = nullptr;
	if (!determine_xrd_vars(c, gt, ac, arr))
		return result;
	if (acc != ac)
		return result;
	if (!gt)
		std::swap(t, f);

	bool max = false;
	if ((acc == get_var_initialized_name(f)) && expressionTreeEqual(t, arr))
		max = true;
	else if ((acc == get_var_initialized_name(t))
				&& expressionTreeEqual(f, arr))
		max = false;
	else
		return result;
	result.name = acc;
	result.type = (max ? V_SgGreaterThanOp : V_SgLessThanOp);
	return result;
}

/// Accepts `x = x op expr` (and `x = expr op x`)
///    * x is a regular variable,
///    * expr doesn't contain x,
///    * op is comething that is_reduction_op<false>(op) accepts.
///
/// \todo do variables get propagated here?
Reduction detect_binop_reduction(SgInitializedName* assignee,
				SgExpression* assigner)
{
	Reduction result;
	auto type = is_reduction_op<false>(assigner->variantT());
	if (type == V_SgTypeUnknown)
		return result;
	auto bo = isSgBinaryOp(assigner);
	auto lhs = bo->get_lhs_operand_i();
	auto rhs = bo->get_rhs_operand_i();
	auto lhs_name = get_var_initialized_name(lhs);
	auto rhs_name = get_var_initialized_name(rhs);
	if (lhs_name == assignee) {
		// we check lhs later
		lhs = rhs;
	} else if (rhs_name == assignee) {
		// we can't allow x = expr - x;
		if (type == V_SgSubtractOp)
			return result;
	} else {
		// not a reduction
		return result;
	}
	// check if expr contains x
	auto refs = querySubTree<SgVarRefExp>(lhs);
	if (boost::find_if(refs, [assignee] (auto r) {
				return get_var_initialized_name(r) == assignee;
			}) != refs.end())
		return result;
	result.name = assignee;
	result.type = type;
	return result;
}

/// Get reaching definition for e if it exists.
SgExpression* ReductionAnalysis::get_rd(SgExpression* e)
{
	rd.generate();
	auto enc_s = getEnclosingNode<SgStatement>(e);
	ES refs;
	refs.insert(e);
	reaching_defs d;
	rd.get_definitions(enc_s, refs, d);
	if (d.size() != 1)
		return nullptr;
	auto x = *(d.begin());
	return x.second.first;
}

/// Accepts `x[y] = a[y] == b[y]` (or `!=`)
///    * x is a regular variable,
///    * expr doesn't contain x,
///    * op is comething that is_reduction_op<false>(op) accepts.
Reduction ReductionAnalysis::detect_eqv_reduction(SgPntrArrRefExp* assignee,
							SgExpression* assigner)
{
	assert(assignee && assigner);
	Reduction result;
	bool eqv = true;
	if (auto n = isSgNotOp(assigner)) {
		eqv = false;
		assigner = n->get_operand();
	}
	if (auto v = isSgVarRefExp(assigner))
		assigner = skip_casting(get_rd(v));
	auto bo = isSgBinaryOp(assigner);
	if (!bo)
		return result;
	auto type = assigner->variantT();
	if (type == V_SgNotEqualOp)
		eqv = !eqv;
	else if (type != V_SgEqualityOp)
		return result;

	auto lhs = bo->get_lhs_operand_i();
	auto rhs = bo->get_rhs_operand_i();
	if (isSgVarRefExp(lhs))
		lhs = skip_casting(get_rd(lhs));
	if (isSgVarRefExp(rhs))
		rhs = skip_casting(get_rd(rhs));
	EV rs, ls, as;
	auto lsp = &ls;
	auto rsp = &rs;
	auto asp = &as;
	if (!isArrayReference(lhs, nullptr, &lsp)
		|| !isArrayReference(rhs, nullptr, &rsp))
		return result;
	assert(isArrayReference(assignee, nullptr, &asp));
	if ((ls.size() != rs.size()) || (rs.size() != as.size()))
		return result;
	for (unsigned i = 0; i < as.size(); i++)
		if (!equal_as_strings(ls[i], rs[i])
				|| !equal_as_strings(rs[i], as[i]))
			return result;
	result.name = get_var_initialized_name(assignee);
	result.type = (eqv ? V_SgEqualityOp : V_SgNotEqualOp);
	return result;
}

/// Detects reduction in assignments. Possible variants:
/// 1. x[y] = ...;
///    Dispatch to detect_eqv_reduction().
/// 1. x = ( ... ? ... : ...);
///    Dispatch to detect_cond_min_max().
/// 1. x = ...;
///    Dispatch to detect_binop_reduction().
Reduction ReductionAnalysis::detect_assignment_reduction(SgAssignOp* e)
{
	Reduction result;
	SgExpression* assignee = nullptr;
	SgExpression* assigner = nullptr;
	if (!isPureAssignment(e, &assignee, &assigner))
		return result;
	assigner = skip_casting(assigner);
	if (auto arr = isSgPntrArrRefExp(assignee))
		return detect_eqv_reduction(arr, assigner);
	auto var = get_var_initialized_name(assignee);
	if (!var)
		return result;
	if (auto ca = isSgConditionalExp(assigner))
		return detect_cond_min_max(var, ca);
	return detect_binop_reduction(var, assigner);
}

namespace {
/// If there's only one assignment to *acc* in *statements*,
/// return what's assigned, otherwise return nullptr.
/// If there is something else than assignments, also return nullptr.
/// If anything's found, set pos accordingly.
SgExpression* get_x_assignment(SgStatementPtrList& statements,
				SgInitializedName* acc, std::size_t &pos)
{
	SgExpression* result = nullptr;
	for (auto it = statements.begin(); it != statements.end(); it++)
		if (auto es = isSgExprStatement(*it)) {
			auto e = isSgAssignOp(es->get_expression());
			if (!e)
				// not an assignment, fail
				return nullptr;
			if (acc != get_var_initialized_name(e->get_lhs_operand_i()))
				continue;
			if (result)
				// found two assignments to acc
				return nullptr;
			result = e->get_rhs_operand_i();
			pos = std::distance(statements.begin(), it);
		} else {
			// not an expression, fail
			return nullptr;
		}
	return result;
}

// If v is an array, return its length.
// If it is a variable, return 1.
// \todo check other possible variants?
std::size_t get_array_size(SgInitializedName* v)
{
	SPRAY::VariableIdMapping vidm;
	auto type = v->get_type();
	if (auto at = isSgArrayType(type))
		return vidm.getArrayElementCount(at);
	else
		return 1;
}

// Create a temporary array, replace assignments to variables in *stmts*
// with assignments to that array, insert assignments to variables from that
// array after *loop*.
// \todo if several types are assigned... ?
SgInitializedName* create_tmp_array_for_loc(const SgStatementPtrList& stmts,
						int x_pos, SgForStatement* loop)
{
	using namespace SageBuilder;
	using namespace boost::adaptors;
	// declare array
	auto tmp_name = SgName(strip(generateUniqueVariableName(loop, "loc")));
	auto tmp_type = buildArrayType(buildLongDoubleType(),
					buildIntVal(stmts.size() + 1));
	auto tmp_arr_d = buildVariableDeclaration(tmp_name, tmp_type);
	insertStatementBefore(loop, tmp_arr_d);
	auto tmp_arr = getFirstInitializedName(tmp_arr_d);

	// store data in the array in the loop
	// copy data from the array after loop
	for (int i = stmts.size() - 1; i >= 0; i--) {
		if (i == x_pos)
			continue;
		auto bs = isSgAssignOp(isSgExprStatement(
					stmts[i])->get_expression());
		auto copyback = deepCopy(stmts[i]);
		assert(bs && copyback);
		auto cb_a = isSgAssignOp(isSgExprStatement(copyback
						)->get_expression());
		auto tmp_loc = buildPntrArrRefExp(buildVarRefExp(tmp_arr, loop),
							buildIntVal(i));
		deepDelete(bs->get_lhs_operand_i());
		bs->set_lhs_operand_i(deepCopy(tmp_loc));
		deepDelete(cb_a->get_rhs_operand_i());
		cb_a->set_rhs_operand_i(tmp_loc);
		insertStatementAfter(loop, copyback);
	}
	return tmp_arr;
}

// Handles xloc reduction where multiple variables are set at location.
//
bool handle_complex_xloc(const SgStatementPtrList& statements, std::size_t x_pos,
			SgForStatement* loop, Reduction& result)
{
	assert(statements.size() > 2);
	SgInitializedName* loc_arr = nullptr;
	for (unsigned i = 0; i != statements.size(); i++) {
		if (i == x_pos)
			continue;
		SgExpression* lhs = nullptr;
		assert(isPureAssignment(statements[i], &lhs));
		SgInitializedName* lhv = get_var_initialized_name(lhs);
		if (loc_arr && (loc_arr != lhv)) {
			// more than one array
			loc_arr = nullptr;
			break;
		} else if (!loc_arr) {
			loc_arr = lhv;
		}
	}
	if (loc_arr) {
		// one array is assigned
		result.array = loc_arr;
		auto as = get_array_size(loc_arr);
		result.array_length = (as ? as : statements.size() - 1);
		return true;
	}
	// several variables
	result.array_length = statements.size() - 1;
	result.array = create_tmp_array_for_loc(statements, x_pos, loop);
	return true;
}

} // namespace

/// Given parameters:
///     if (cond) {
///         statements[0];
///         statements[1];
///         ...
///     }
/// Detect minloc/maxloc reduction.
/// Also, if results are stored in a set of variables, create a temporary
/// array and store them there instead, moving results back later.
Reduction ReductionAnalysis::detect_xloc(SgStatement* cond,
						SgStatementPtrList& statements)
{
	Reduction result;
	auto c = stmt_to_expr(cond);
	if (!c)
		return result;

	auto max = true;
	SgInitializedName* acc = nullptr;
	SgExpression* arr = nullptr;
	if (!determine_xrd_vars(c, max, acc, arr))
		return result;

	// Get the extreme assignment
	std::size_t pos = 0;
	auto assigner = get_x_assignment(statements, acc, pos);
	if (!assigner)
		return result;
	if (!expressionTreeEqual(assigner, arr) &&
			!expressionTreeEqual(get_rd(assigner), arr))
		return result;

	// determine what is stored
	if (statements.size() == 2) {
		auto st = statements[1 - pos];
		SgExpression* lhs = nullptr;
		SgExpression* rhs = nullptr;
		assert(isPureAssignment(st, &lhs, &rhs));
		// do not allow things like a[i]
		if (querySubTree<SgVarRefExp>(lhs).size() != 1)
			return result;
		if (auto buf = get_var_initialized_name(lhs)) {
			result.array = buf;
			auto as = get_array_size(buf);
			result.array_length = (as ? as : 1);
		} else {
			// unknown buffer type
			return result;
		}
	} else {
		if (!handle_complex_xloc(statements, pos, loop, result))
			return result;
	}

	result.name = acc;
	result.type = (max ? V_SgGreaterOrEqualOp : V_SgLessOrEqualOp);
	return result;
}

namespace {
/// Detects reduction in compound assignments.
/// x op= expr;
/// * x is a regular variable,
/// * expr doesn't contain x,
/// * op is comething that is_reduction_op<true>(op) accepts.
Reduction detect_modifier_reduction(SgCompoundAssignOp* cao)
{
	Reduction result;
	auto var = isSgVarRefExp(cao->get_lhs_operand_i());
	auto name = get_var_initialized_name(var);
	if (!name)
		return result;
	auto type = is_reduction_op<true>(cao->variantT());
	if (type == V_SgTypeUnknown)
		return result;
	auto refs = querySubTree<SgVarRefExp>(cao->get_rhs_operand_i());
	if (boost::find_if(refs, [name] (auto r) {
				return get_var_initialized_name(r) == name;
			}) != refs.end())
		return result;
	result.name = name;
	result.type = type;
	return result;
}

template<typename AnalysisResult>
void insert_reduction_pragma(SgStatement* target, AnalysisResult info)
{
	auto pragma = info.to_pragma();
	if (pragma)
		insertStatementBefore(target, pragma.release());
}

} // namespace

/// Given parameters as in:
///     if (*cond*)
///         *s*;
/// Detect min/max reduction. If *lhs* is an array reference, then *rhs*
/// must be a variable and *s* must be an assignment *rhs* = *lhs*;. Then
/// it is a MAX reduction with *rhs* as a reduction variable. Otherwise it
/// is a MIN with *lhs* as a r. variable.
Reduction ReductionAnalysis::detect_min_max(SgStatement* cond, SgStatement* s)
{
	Reduction result;
	auto c = stmt_to_expr(cond);
	if (!c)
		return result;

	// Get info about the assignment
	auto e = stmt_to_expr(s);
	if (!e)
		return result;
	SgInitializedName* assignee = nullptr;
	SgExpression* assigner = nullptr;
	if (!isPureAssignment(e, &assignee, &assigner))
		return result;

	auto max = true;
	SgInitializedName* acc = nullptr;
	SgExpression* arr = nullptr;
	if (!determine_xrd_vars(c, max, acc, arr))
		return result;

	if (assignee != acc)
		return result;
	if (!expressionTreeEqual(assigner, arr))
		return result;
	result.name = acc;
	result.type = (max ? V_SgGreaterThanOp : V_SgLessThanOp);
	return result;
}

/// Detect min/max or minloc/manloc reduction defined as an if statement.
/// Check that nothing's modified in the conditional statement at this point.
Reduction ReductionAnalysis::detect_extreme_reduction(SgIfStmt* ifs)
{
	Reduction result;
	if (ifs->get_false_body())
		return result;
	auto cond = ifs->get_conditional();
	if (!cond)
		return result;

	NV mod;
	get_modified_variables(cond, mod);
	if (mod.size())
		return result;
	auto body = ifs->get_true_body();
	auto bb = isSgBasicBlock(body);
	if (!bb) {
		return detect_min_max(cond, body);
	} else {
		auto stmts = bb->get_statements();
		switch (stmts.size()) {
		case 1:
			return detect_min_max(cond, stmts[0]);
		default:
			return detect_xloc(cond, stmts);
		}
	}
	return result;
}

// Matches v++; etc.
Reduction detect_unary_reduction(SgUnaryOp* uo)
{
	Reduction result;
	auto type = uo->variantT();
	if ((type != V_SgPlusPlusOp) &&
			(type != V_SgMinusMinusOp))
		return result;
	auto var = isSgVarRefExp(uo->get_operand());
	auto name = get_var_initialized_name(var);
	if (name) {
		result.name = name;
		result.type = type;
	}
	return result;
}

/// Find out whether *s* specifies a reduction and if so, fill the struct.
Reduction ReductionAnalysis::detect_reduction(SgStatement* s)
{
	if (auto es = isSgExprStatement(s)) {
		auto e = es->get_expression();
		if (auto uo = isSgUnaryOp(e)) {
			return detect_unary_reduction(uo);
		} else if (auto ao = isSgAssignOp(e)) {
			return detect_assignment_reduction(ao);
		} else if (auto cao = isSgCompoundAssignOp(e)) {
			return detect_modifier_reduction(cao);
		}
	} else if (auto ifs = isSgIfStmt(s)) {
		return detect_extreme_reduction(ifs);
	}
	// no reduction found
	return Reduction();
}

void ReductionAnalysis::run()
{
	ReductionAnalysisResult r;
	for (auto s : detect_reduction_possibilities(loop)) {
		auto reduction = detect_reduction(s);
		if (reduction.valid())
			r.push_back(std::move(reduction));
	}
	insert_reduction_pragma(loop, r);
}

} // namespace CodeAnalyses
