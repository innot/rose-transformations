#pragma once

#include "sage3basic.h"
#include <set>
#include <vector>
#include <string>
#include "util.h"

namespace LoopTransformations {
/// A class that splits loop bodies.
class LoopSplitter {
	// variables
	SgForStatement *loop; // is used in exceptions thrown when checks fail
	const TfUtils::STRS &p;
	SgBasicBlock *block;
	TfUtils::CNS loop_indices;

	void check_split_possibility(const TfUtils::depv &deps);
	void check_if_all_stmts_are_used(
			const TfUtils::SV &statements,
			const TfUtils::CNSV &modified_vars,
			const TfUtils::depv &dependencies);
public:
	LoopSplitter(SgForStatement *loop,
			const TfUtils::STRS &split);
	void split_loop();
};

/// Calculates dependencies of a single variable.
TfUtils::depv get_dependant_variables(SgBasicBlock *block,
					const SgInitializedName *var);

/// Performs full loop body distribution.
void distribute_loop(SgForStatement* loop);

using deps = std::vector<TfUtils::SV>;
/// Splits all statements of the body of *loop* into independent sets.
deps get_dependency_sets(SgForStatement* loop);

} // namespace LoopTransformations
