#include "PragmaProcessor.h"

int main(int argc, char * argv[])
{
	PragmaProcessor t(argc, argv);
	t.process();
	return t.backend();
}

