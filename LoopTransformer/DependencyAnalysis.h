#pragma once
#include <memory>
#include "sage3basic.h"

namespace CodeAnalyses {

class DependencyAnalysis {
	class Impl;
	std::unique_ptr<Impl> impl;
public:
	DependencyAnalysis(SgForStatement* loop);
	~DependencyAnalysis();
	void run();
	void annotate_source();
	void annotate_ast();
};

} // namespace CodeAnalyses
