#pragma once
#include "util.h"

class SgForStatement;
class SgStatement;

/// The class that dispatches requests for analyses.
class CodeAnalyser {
	TfUtils::AnalysisParams p;

	/// The analysed block
	SgStatement* block;
public:
	CodeAnalyser(SgStatement* block,
		TfUtils::AnalysisParams&& params);
	~CodeAnalyser();
	void handle_block();
};
